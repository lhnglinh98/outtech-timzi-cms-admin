import axios from "axios";
// const cors = require("cors");
// app.use(cors({ origin: true }));
const CallApi = (endpoint, method, body, Authorization) => {
  return axios({
    method: method,
    url: `http://127.0.0.1:8000/api/${endpoint}`,
    data: body,
    headers: {
      Authorization: "Bearer " + Authorization,
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS, HEAD",
      "Access-Control-Allow-Headers":
        "Access-Control-*, Origin, X-Requested-With, Content-Type, Accept",
    },
  });
};
export const CallApi1 = (endpoint, method, body, Authorization) => {
  return axios({
    method: method,
    url: `${endpoint}`,
    data: body,
    headers: {
      Authorization: "Bearer " + Authorization,
      "Content-Type": "application/json",

      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Expose-Headers": "Content-Length, X-JSON",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS, HEAD",
      "Access-Control-Allow-Headers": "*",
    },
  });
};
export default CallApi;
