import React, { useState, useEffect } from "react";
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  // CImg,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import CallApi from "../Middleware/api";
const TheHeaderDropdownMssg = () => {
  const [list, setList] = useState([]);
  useEffect(() => {
    CallApi(`notify/list-notify`, "GET", null, localStorage.getItem("token"))
      .then((res) => {
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, []);
  const changeLocation = (link) => {
    window.location = link;
  };
  const CheckAll = () =>{
    changeLocation( `/#/thong-bao`);
  }
  const ConfirmNoti = (item) => {
    CallApi(
      `notify/confirm-view-notify?notify_id=${item.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        if (res.data.status === 1) {
          if (item.type === 1) {
            changeLocation(
              `/#/Quan-ly-cua-hang/chi-tiet-cua-hang/${item.object_id}`
            );
          }
          CallApi(
            `notify/list-notify`,
            "GET",
            null,
            localStorage.getItem("token")
          )
            .then((res) => {
              if (res.data.status === 1) {
                setList(res.data.data);
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              }
            })
            .catch((error) => {
              // swal("Vui lòng kiểm tra internet");
            });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  };
  
  return (
    <CDropdown inNav className="c-header-nav-item mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
          <CIcon name="cil-bell" />  {Number(list.count_notify) > 0 ? (
          <>
        
            <CBadge shape="pill" color="info">
              {list.count_notify}
            </CBadge>{" "}
          </>
        ) : null}
      </CDropdownToggle>
      <CDropdownMenu
        className="pt-0"
        placement="bottom-end"
        style={{ paddingBottom: "0px" }}
      >
        <CDropdownItem header tag="div" color="light">
          <strong>Bạn có {list.count_notify} thông báo mới</strong>
        </CDropdownItem>
        {list?.list_notify?.data?.map((item, index) => {
          return (
            <CDropdownItem
              href="#"
              key={index}
              onClick={() => ConfirmNoti(item)}
              className={item.is_view === 0 ? "BackGroundStyle" : ""}
            >
              <div className="message">
                {/* <div className="pt-3 mr-3 float-left">
                  <div className="c-avatar">
                    <CImg
                      src={"avatars/7.jpg"}
                      className="c-avatar-img"
                      alt="admin@bootstrapmaster.com"
                    />
                    <span className="c-avatar-status bg-success"></span>
                  </div>
                </div> */}
                {/* <div>
                  <small className="text-muted">{item.device_id}</small>
                  <small className="text-muted float-right mt-1">
                    {item.created_at}
                  </small>
                </div> */}
                {/* <div className="text-truncate font-weight-bold">
              <span className="fa fa-exclamation text-danger"></span> Important message
            </div> */}
             <div style={{whiteSpace: "break-spaces", maxWidth:'300px', minWidth:"250px"}}>
                  {item.content}
                </div>
                <div className="small text-muted text-truncate">
                  {item.created_at}
                </div>
              </div>
            </CDropdownItem>
          );
        })}

        <CDropdownItem href="#" className="text-center border-top" onClick={CheckAll}><strong style={{textAlign:"center", width:"80px", margin:"0px auto"}}>Xem tất cả</strong></CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

export default TheHeaderDropdownMssg;
