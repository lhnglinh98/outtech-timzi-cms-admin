import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  // CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from "@coreui/react";

// import CIcon from "@coreui/icons-react";
import Anh1 from "../assets/Untitled-2.png";
// sidebar nav config
import navigation from "./_nav";
import { Set_reponsive } from "../Redux/action/index";
import "./index.css";
const TheSidebar = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.sidebarShow);

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch(Set_reponsive(val))}
      style={{
        background: "#004780",
        color: "white",
        borderRight: "1px solid rgb(0 71 128)",
      }}
    >
      <CSidebarBrand
        className="d-md-down-none"
        to="/"
        style={{
          // padding: "10px 0px 10px 0px",
          background: "#004780",
          fontWeight: "bold",
        }}
      >
        <img
          src={Anh1}
          style={{ height: "100px", marginRight: "10px", marginBottom:"10px" }}
          alt="hinh-anh"
        />
        {/* TIN TỨC */}
        {/* <CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          height={35}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={35}
        /> */}
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={navigation}
          style={{ color: "white !important" }}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      {/* <CSidebarMinimizer className="c-d-md-down-none" disabled /> */}
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
