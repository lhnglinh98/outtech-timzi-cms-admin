export const router = [
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Doanh thu',
    icon: 'cil-cursor',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Cửa hàng',
        to: '/doanh-thu-cua-hang',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Shipper',
        to: '/doanh-thu-shipper',
      },
    ]
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Tài khoản người dùng',
    icon: 'cil-people',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Khách hàng',
        to: '/users',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Apps shop',
        to: '/tai-khoan-apps-shop',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Shipper',
        to: '/tai-khoan-shipper',
        
      },
    ]
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Tài khoản quản trị',
    icon: 'cil-user',
    _children: [
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Phân quyền',
      //   to: '/Quan-ly-phan-quyen',
        
      // },
      {
        _tag: 'CSidebarNavItem',
        name: 'Nhân viên',
        to: '/Quan-ly-nhan-vien-quan-tri',
        
      },
    ]
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Tài khoản chuyển nhượng',
    to: '/quan-ly-tai-khoan-chuyen-nhuong',
    icon: 'cil-user',

  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Quản trị website',
    icon: 'cil-drop',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Trang chủ',
        to: '/Quan-ly-trang-chu-website',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Link Youtube',
        to: '/Quan-ly-link-youtube',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Nội dung hướng tới',
        to: '/Quan-ly-noi-dung-huong-toi',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Tính năng',
        to: '/Quan-ly-tinh-nang-website',
        
      },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Sử dụng giải pháp',
      //   to: '/quan-ly-su-dung-giai-phap',
        
      // },
      {
        _tag: 'CSidebarNavItem',
        name: 'Câu hỏi',
        to: '/Quan-ly-cau-hoi-website',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Tài liệu hướng dẫn',
        to: '/Quan-ly-tai-lieu-huong-dan',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Đội ngũ lãnh đạo',
        to: '/Quan-ly-doi-ngu-lanh-dao-website',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Liên hệ',
        to: '/Quan-ly-dang-ki-lien-he-website',
        
      },
    ]
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Chính sách trên app',
    icon: 'cil-drop',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Hướng dẫn thanh toán',
        to: '/huong-dan-thanh-toan',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Chính sách bảo mật',
        to: '/chinh-sach-bao-mat',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Quy chế',
        to: '/quy-che',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Điều khoản sử dụng',
        to: '/dieu-khoan-su-dung',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'CS giải quyết tranh chấp',
        to: '/chinh-sach-giai-quyet-tranh-chap',
      },
    ]
  },
 
  // icon: 'cil-drop',
  {
    _tag: 'CSidebarNavItem',
    name: 'Cửa hàng',
    to: '/Quan-ly-cua-hang',
    icon: 'cil-home',
  },
    {
    _tag: 'CSidebarNavItem',
    name: 'Danh mục cửa hàng',
    to: '/quan-ly-danh-muc-cua-hang',
    icon: 'cil-puzzle',

  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Danh mục món ăn',
    icon: 'cil-list',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Danh mục món ăn',
        to: '/quan-ly-danh-muc-mon-an',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Kiểu món ăn',
        to: '/quan-ly-danh-muc-kieu-mon-an',
        
      },
    ]
  },
  
  {
    _tag: 'CSidebarNavItem',
    name: 'Voucher',
    to: '/quan-ly-voucher',
    icon: 'cil-moon',

  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Voucher đơn giao hộ',
    to: '/quan-ly-voucher-don-giao-ho',
    icon: 'cil-moon',

  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Phí ship',
    to: '/quan-ly-phi-ship',
    icon: 'cil-credit-card',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Trọng lượng hàng hóa',
    to: '/quan-ly-khoi-luong-hang-hoa',
    icon: 'cil-credit-card',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Đơn hàng',
    to: '/don-hang',
    icon: 'cil-credit-card',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Đơn hàng giao hộ',
    to: '/don-hang-giao-ho',
    icon: 'cil-credit-card',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Chương trình cửa hàng',
    to: '/chuong-trinh-cua-hang',
    icon: 'cil-star',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Chương trình TimZi',
    to: '/chuong-trinh-timzi',
    icon: 'cil-star',
  },
  
  {
    _tag: 'CSidebarNavItem',
    name: 'Banner',
    to: '/Banner',
    icon: 'cil-file',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Setting',
    to: '/chiet-khau',
    icon: 'cil-dollar',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Thông báo',
    to: '/thong-bao',
    icon: 'cil-bell',
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Quản trị mua hộ',
    icon: 'cil-credit-card',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Đơn hàng mua hộ',
        to: '/don-hang-mua-ho',
        
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Cấu hình chi phí dịch vụ',
        to: '/quan-ly-category-services',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Cấu hình chi phí khoảng cách',
        to: '/quan-ly-distances',
      },
    ]
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'TB CMS chuyển nhượng',
    to: '/thong-bao-cms-chuyen-nhuong',
    icon: 'cil-bell',
  },
  
]

export default router;