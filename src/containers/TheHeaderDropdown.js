import React, { useState, createRef } from "react";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
} from "@coreui/react";
import swal from "sweetalert";
import {set_profile, reloading} from "../Redux/action/index"
import Group3 from "../assets/Group 14847 1.png";
import { Modal, Row, Col, Button, Form } from "react-bootstrap";
import { useSelector , useDispatch} from "react-redux";
import CIcon from "@coreui/icons-react";
import Key from "../assets/key-4.png";
import CallApi from "../Middleware/api";
import Resizer from "react-image-file-resizer";
const TheHeaderDropdown = () => {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile);
  const fileInput3 = createRef();
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [user, setUser] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [birthday, setBirthday] = useState("");
  const [address, setAddress] = useState("");
  const [image, setImage] = useState("");
  const [hinhanh, setHinhanh] = useState("");
  const [current_password, setCurrent_password] = useState("");
  const [password, setPassword] = useState("");
  const [password_confirmation, setPassword_confirmation] = useState("");
  // const changeLocation = (link) => {
  //   window.location = link;
  // };
  const toggleModal = () => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setTitleModal("Cập nhật thông tin cá nhân");
    setName(profile.name);
    setBirthday(profile.birthday);
    setHinhanh(profile.avatar);
    setAddress(profile.address);
  setImage("")
    setUser(true);
    setEmail(profile.email);
  };
  const toggleModal1 = () => {
    setShowHide(!showHide);
    setSubmitModal("Đổi");
    setName("");
    setBirthday("");
    setPassword("");
    setImage("")
    setPassword_confirmation("");
    setCurrent_password("")
    setHinhanh("");
    setAddress("");
    setUser(false);
    setEmail("");
    setTitleModal("Đổi mật khẩu");
  };
  const CloseModal = () => {
    setShowHide(!showHide);
  };

  
  const logoutTo = () => {
    localStorage.clear();
    window.location.reload();
  };
  const UpdateAccount = () => {
    if (user) {
      dispatch(reloading(true));
      let file = new FormData()
      file.append("name", name)
      file.append("birthday", birthday)
      file.append("email", email)
      if(image!==""){
          file.append("avatar", image)
      }
    
      file.append("address", address)
      file.append("_method", 'put')
      CallApi(
        `admin/update-user`,
        "POST",
        file,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(false);
            swal("Thành công!", "Bạn đã cập nhật thành công!", "success")
            dispatch(set_profile(res.data.data));
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          swal("Có gì đó không ổn!!!");
        });
    } else {
      dispatch(reloading(true));
      CallApi(
        `admin/change-password`,
        "PUT",
        {
          current_password,
          password,
          password_confirmation,
        },
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            swal("Thành công!", "Bạn đã cập nhật thành công!", "success")
            setShowHide(false);
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          swal("Có gì đó không ổn!!!");
        });
    }
  };
  const OnchangeImage3 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);

    var fileInput = false;
    if (e.target.files[0]) {
      fileInput = true;
    }
    if (fileInput) {
      try {
        Resizer.imageFileResizer(
          e.target.files[0],
          300,
          300,
          "JPEG",
          100,
          0,
          (uri) => {
            fetch(uri)
              .then((res) => res.blob())
              .then((blob) => {
                const file1 = new File([blob], "File name", {
                  type: "image/png",
                });
                setHinhanh(file);
                setImage(file1);
              });
          },
          "base64",
          200,
          200
        );
      } catch (err) {
        console.log(err);
      }
    }
  };
  const UploadImage3 = () => fileInput3.current.click();

  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {user ? (
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Họ tên</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    defaultValue={name}
                    placeholder="Họ tên..."
                    onChange={(e) => setName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="text"
                    name="email"
                    defaultValue={email}
                    placeholder="Email..."
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Ngày sinh</Form.Label>
                  <Form.Control
                    type="date"
                    name="birthday"
                    defaultValue={birthday}
                    placeholder="Ngày sinh..."
                    onChange={(e) => setBirthday(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Địa chỉ</Form.Label>
                  <Form.Control
                    type="text"
                    name="address"
                    defaultValue={address}
                    placeholder="Địa chỉ..."
                    onChange={(e) => setAddress(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group>
                  <Form.Label>Hình ảnh</Form.Label>
                  <input
                    ref={fileInput3}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage3(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {hinhanh === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={hinhanh}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>
              </Col>
            </Row>) : (
              <Row>
                <Col md={12}>
                <Form.Group>
                  <Form.Label>Mật khẩu hiện tại</Form.Label>
                  <Form.Control
                    type="password"
                    name="current_password"
                    defaultValue={current_password}
                    placeholder="Mật khẩu hiện tại..."
                    onChange={(e) => setCurrent_password(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mật khẩu mới</Form.Label>
                  <Form.Control
                    type="password"
                    name="password"
                    defaultValue={password}
                    placeholder="Mật khẩu mới..."
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Xác nhận mật khẩu</Form.Label>
                  <Form.Control
                    type="password"
                    name="password_confirmation"
                    defaultValue={password_confirmation}
                    placeholder="Xác nhận mật khẩu..."
                    onChange={(e) => setPassword_confirmation(e.target.value)}
                  />
                </Form.Group>
                </Col>
              </Row>
            )}
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <div className="c-avatar">
            <CImg
              src={profile?.avatar}
              className="c-avatar-img"
              style={{borderRadius:"100%"}}
              alt="admin@bootstrapmaster.com"
            />
          </div>
          <span style={{ marginLeft: "10px" }}>{profile?.name}</span>
        </CDropdownToggle>
        <CDropdownMenu
          className="pt-0"
          placement="bottom-end"
          style={{ padding: "0px" }}
        >
          {/* <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Account</strong>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-bell" className="mfe-2" />
          Updates
          <CBadge color="info" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-envelope-open" className="mfe-2" />
          Messages
          <CBadge color="success" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-task" className="mfe-2" />
          Tasks
          <CBadge color="danger" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-comment-square" className="mfe-2" />
          Comments
          <CBadge color="warning" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem> */}
          <CDropdownItem header tag="div" color="light" className="text-center">
            <strong>Cài đặt</strong>
          </CDropdownItem>
          {/* <CDropdownItem onClick={() => toggleModal()}>
            <CIcon name="cil-user" className="mfe-2" />
            Cập nhật thông tin cá nhân
          </CDropdownItem>
          <CDropdownItem onClick={() => toggleModal1()}>
            <CIcon name="cil-settings" className="mfe-2" />
            Đổi mật khẩu
          </CDropdownItem> */}
          {/* <CDropdownItem>
          <CIcon name="cil-credit-card" className="mfe-2" />
          Payments
          <CBadge color="secondary" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem>
          <CIcon name="cil-file" className="mfe-2" />
          Projects
          <CBadge color="primary" className="mfs-auto">
            42
          </CBadge>
        </CDropdownItem> */}
          <CDropdownItem divider />
          <CDropdownItem onClick={() => toggleModal()}>
            <CIcon name="cil-user" className="mfe-2" />
            Thông tin cá nhân
          </CDropdownItem>
          <CDropdownItem onClick={() => toggleModal1()}>
            <img
              src={Key}
              alt="Key"
              style={{ width: "16px", height: "16px", marginRight: "10px" }}
            />
            Đổi mật khẩu
          </CDropdownItem>
          <CDropdownItem onClick={() => logoutTo()}>
            <CIcon name="cil-lock-locked" className="mfe-2" />
            Đăng xuất
          </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
    </>
  );
};

export default TheHeaderDropdown;
