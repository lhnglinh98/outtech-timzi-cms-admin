import React, { useEffect }  from "react";
import { TheContent, TheSidebar, TheFooter, TheHeader } from "./index";
import LoadingMask from "react-loadingmask";
import { useSelector,useDispatch } from "react-redux";

import {set_profile, reloading} from "../Redux/action/index"
import CallApi from "../Middleware/api"
const TheLayout = () => {
  const dispatch = useDispatch();
  const counter = useSelector((state) => state.loading);

  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      "admin/get-user-information",
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          
          dispatch(set_profile(res.data.data));
        } else if (
          res.data.message === "Token không hợp lệ" ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  return (
    <LoadingMask loading={counter} text={"loading..."}>
      <div className="c-app c-default-layout">
        <TheSidebar />
        <div className="c-wrapper">
          <TheHeader />
          <div className="c-body">
            <TheContent />
          </div>
          <TheFooter />
        </div>
      </div>
    </LoadingMask>
  );
};

export default TheLayout;
