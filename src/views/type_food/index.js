import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";

import Search from "@material-ui/icons/Search";
const Event = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [search, setSearch] = useState(false);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/type-food/list-type-food`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
      
  }, [dispatch]);

  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setName(item.name);
    setId(item.id);
    setTitleModal("Cập nhật kiểu món ăn");
  };
  const toggleModal3 = () => {
    setSearch(false);
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setName("");
    setId("");
    setTitleModal("Thêm kiểu món ăn");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    if (search){
      CallApi(
        `admin/type-food/list-type-food?name=${name}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setList(res.data.data);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
          CloseModal()
        })
        .catch((error) => {
          
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    }else if (id === "") {
      CallApi(
        `admin/type-food/create-type-food`,
        "POST",
        { name },
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(!showHide);
            swal("Thành công!", "Bạn đã thêm thành công!", "success");

            CallApi(
              `admin/type-food/list-type-food`,
              "GET",
              null,
              localStorage.getItem("token")
            )
              .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                  setList(res.data.data);
                } else if (
                  res.data.message === "Không tìm thấy tài khoản." ||
                  res.data.message === "Token đã hết hạn"
                ) {
                  localStorage.clear();
                  window.location.reload();
                }
              })
              .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");
              });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    } else {
      CallApi(
        `admin/type-food/update-type-food/${id}`,
        "PUT",
        { name },
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(!showHide);
            swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

            CallApi(
              `admin/type-food/list-type-food`,
              "GET",
              null,
              localStorage.getItem("token")
            )
              .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                  setList(res.data.data);
                } else if (
                  res.data.message === "Không tìm thấy tài khoản." ||
                  res.data.message === "Token đã hết hạn"
                ) {
                  localStorage.clear();
                  window.location.reload();
                }
              })
              .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");
              });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    }
  };
  const CloseModal = () => {
    setName("");
    setShowHide(!showHide);
  };

  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/type-food/delete-type-food/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              CallApi(
                `admin/type-food/list-type-food`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  const toggleModal = () => {
    setShowHide(!showHide);
    setId("");
    setTitleModal("Tìm kiếm");
    setSubmitModal("Tìm");
    setSearch(true);
  };
  
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên kiểu món ăn</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    defaultValue={name}
                    placeholder="Tên kiểu món ăn..."
                    onChange={(e) => setName(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách danh mục món ăn
          <div className="card-header-actions">
            <small className="text-muted">
              <Button
                onClick={() => toggleModal()}
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  backgroundImage: " linear-gradient(#3776a9, #004780)",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "7%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  ID
                </th>
                <th
                  style={{
                    width: "30%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên kiểu món ăn
                </th>
                <th
                  style={{
                    width: "20%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Ngày tạo
                </th>
                <th
                  style={{
                    width: "20%",
                    textAlign: "center",
                  }}
                >
                  Ngày cập nhật
                </th>

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                    // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{index+1}</td>
                      <td>{item.name}</td>

                      <td>
                        {item?.created_at?.slice(8, 10)}-
                        {item?.created_at?.slice(5, 7)}-
                        {item?.created_at?.slice(0, 4)}
                      </td>
                      <td>
                        {item?.updated_at?.slice(8, 10)}-
                        {item?.updated_at?.slice(5, 7)}-
                        {item?.updated_at?.slice(0, 4)}
                      </td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    </>
  );
};
export default Event;
