import React, { useState, useEffect, createRef } from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Group3 from "../../assets/Group 14847 1.png";
import Delete from "@material-ui/icons/Delete";
import Select from 'react-select';
const Event = () => {
  const fileInput1 = createRef();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [id, setId] = useState("");
  const [banner, setBanner] = useState("");
  const [banner_image, setBanner_image] = useState("");
  const [banner_store_id, setBanner_store_id] = useState({ value: "", label: "Chọn cửa hàng" });
  const [showHide, setShowHide] = useState(false);
  const [listStore, setListStore] = useState("");
  const [store_id_set, setStore_id] = useState({ value: "", label: "Chọn cửa hàng" });
  const [store_id, setStore_id_push] = useState("");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/banner/list-banner`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
      CallApi(
        `admin/store/list-store-with-push-notify`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            console.log(res.data.data);
            setListStore(res.data.data)
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  }, [dispatch]);


  const OnchangeImage1 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);

    setBanner_image(file);
    setBanner(e.target.files[0]);
  };
  const UploadImage1 = () => fileInput1.current.click();

  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setBanner("")
    setId(item.id)
    setBanner_image(item.image)
    setBanner_store_id({ value: item.store_id, label: item.store.name })
    setTitleModal("Cập nhật banner");
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setBanner("")
    setId("")
    setBanner_image("")
    setBanner_store_id({ value: "", label: "Chọn cửa hàng" })
    setTitleModal("Thêm banner");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    let file = new FormData();
    if(id===""){
    file.append("image",banner)
    file.append("store_id",store_id)

    CallApi(
      `admin/banner/create-banner`,
      "POST",
      file,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setShowHide(!showHide);
          swal("Thành công!", "Bạn đã thêm thành công!", "success");

          CallApi(
            `admin/banner/list-banner`,
            "GET",
            null,
            localStorage.getItem("token")
          )
            .then((res) => {
              dispatch(reloading(false));
              if (res.data.status === 1) {
                setList(res.data.data);
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              }
            })
            .catch((error) => {
              dispatch(reloading(true));
              // swal("Vui lòng kiểm tra internet");
            });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
        else{
            swal(res.data.message)
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
    }
    else{
        
        if(banner !==""){
             file.append("image",banner)
        }
        if(store_id !==""){
             file.append("store_id",store_id)
        }
       file.append("_method","put")
       
    
        CallApi(
          `admin/banner/update-banner/${id}`,
          "POST",
          file,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");
    
              CallApi(
                `admin/banner/list-banner`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            }
        else{
            swal(res.data.message)
        }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
    }
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/banner/delete-banner/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              CallApi(
                `admin/banner/list-banner`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                
                <Form.Group>
                  <Form.Label>Banner</Form.Label>
                  <input
                    ref={fileInput1}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage1(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {banner_image === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage1()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={banner_image}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage1()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>
                <Form.Group>
                <Form.Label>Cửa hàng</Form.Label>
                <Select 
                  value={banner_store_id}
                  onChange={(e) =>{
                    setStore_id(e)
                    setBanner_store_id(e)
                    setStore_id_push(e.value)
                  
                  } } 
                  options={listStore.map((item, index) => {
                    return (
                      { value: item.id, label: item.name }
                    );
                  })} 
                 
                  placeholder="Chọn cửa hàng"
                  />
              </Form.Group>
               
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
            
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách banner
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
              <th
                  style={{
                    width: "6%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  STT
                </th>
               
                

                <th
                  style={{
                    width: "50%",
                    textAlign: "center",
                  }}
                >
                  Banner
                </th>
                <th
                  style={{
                    width: "14%",
                    textAlign: "center",
                  }}
                >
                  Cửa hàng
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                 Ngày thêm
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Ngày cập nhật
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>
                        {index + 1}
                      </td>
                     
                      <td>
                        <img
                          src={item.image}
                          alt="imageBanner"
                          style={{
                            width: "85%",
                            maxWidth: "85%",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                      </td>
                      <td>
                        {item.store ? item.store.name : null}
                      </td>
                      <td>
                        {item?.created_at?.slice(8, 10)}-
                        {item?.created_at?.slice(5, 7)}-
                        {item?.created_at?.slice(0, 4)}
                      </td>
                      <td>
                        {item?.updated_at?.slice(8, 10)}-
                        {item?.updated_at?.slice(5, 7)}-
                        {item?.updated_at?.slice(0, 4)}
                      </td>
                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                           onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
         
        </div>
      </div>
    </>
  );
};
export default Event;
