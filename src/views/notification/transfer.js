import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import { CPagination } from "@coreui/react";
import Select from 'react-select'
const options = [
  { value: "Value 1", label: "Value 1" },
  { value: "Value 2", label: "Value 2" }
];
const Event = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [page, setPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [type, setType] = useState("1");
  const [showHide, setShowHide] = useState(false);
  const [content, setContent] = useState("");
  const [listStore, setListStore] = useState("")
  const [store_id_set, setStore_id] = useState({ value: "", label: "Chọn cửa hàng" })
  const [store_id, setStore_id_push] = useState("")
  
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/list-notify-transfer-with-admin?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data?.data?.data);
          setPage(res.data.data.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

      dispatch(reloading(true));
      CallApi(
        `admin/store/list-store-with-push-notify`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setListStore(res.data.data)
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  }, [dispatch, currentPage]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  // const options = [
  //   // listStore.map((item, index) => {
  //   //   return (
  //   //     { value: item.id, label: item.name }
  //   //   );
  //   // })
  //   { value: 'chocolate', label: 'Chocolate' },
  //   { value: 'strawberry', label: 'Strawberry' },
  //   { value: 'vanilla', label: 'Vanilla' }
  // ]
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Đẩy");
    setContent("");
    setType("1");
    setStore_id("")
    setTitleModal("Đẩy thông báo");
  };
  // const handleFirstLevelChange = function (event) {
  //   console.log(1)
  //   // setStore_id(event.target.value)
  // }
  const ConfirmItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xác nhận?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/confirm-notify-transfer/${item.id}`,
          "PUT",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            console.log(res);
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xác nhận thành công!", "success");
              setList([]);
              CallApi(
                `admin/list-notify-transfer-with-admin?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data?.data?.data);
                    setPage(res.data.data.last_page);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  const CancelItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn hủy?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/cancel-notify-transfer/${item.id}`,
          "PUT",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            console.log(res);
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã hủy thành công!", "success");
              setList([]);
              CallApi(
                `admin/list-notify-transfer-with-admin?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data?.data?.data);
                    setPage(res.data.data.last_page);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      <div className="card">
        <div className="card-header">
          Danh sách thông báo
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "13%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Người gửi
                </th>
                <th
                  style={{
                    width: "13%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  QL khu vực
                </th>
                <th
                  style={{
                    width: "18%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Nội dung
                </th>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Đối tượng
                </th>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Cửa hàng
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Ngày tạo
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Trạng thái
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                    Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                    //   onClick={() => ConfirmNoti(item)}
                    >
                      <td>{item?.transfer.name}</td>
                      <td>{item?.transfer.manage_province?.name} {item?.transfer.manage_district? " - " : ""} {item?.transfer.manage_district?.name}</td>
                      <td>{item?.content}</td>
                      <td>
                          {item?.type === 1 ? "App khách hàng" : ""}
                          {item?.type === 2 ? "App Shipper" : ""}
                          {item?.type === 3 ? "App Shop" : ""}
                        </td>
                        <td>{item?.store?.name}</td>
                        <td>{item?.created_at?.slice(11, 16)} {item?.created_at?.slice(8, 10)}-{item?.created_at?.slice(5, 7)}-{item?.created_at?.slice(0, 4)}</td>
                        <td>
                          {item?.status === 0 ? "Chưa xác nhận" : ""}
                          {item?.status === 1 ? "Đã xác nhận" : ""}
                          {item?.status === 2 ? "Đã hủy" : ""}
                        </td>
                        <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          {
                            item?.status === 0
                            ?
                            <Button
                            onClick={() => ConfirmItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              background:
                                "#20d414",
                              margin: "5px",
                              padding: "6px 10px",
                              cursor: "pointer",
                            }}
                          >
                            Xác nhận
                          </Button>
                           
                          :null
                          }
                          {
                            item?.status === 0
                            ?
                            <Button
                            onClick={() => CancelItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              background:
                              "red",
                              margin: "5px",
                              padding: "6px 10px",
                              cursor: "pointer",
                            }}
                          >
                            Hủy
                          </Button>
                           
                          :null
                          }
                        </span>
                        </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
          <br></br>
        </div>
      </div>
    </>
  );
};
export default Event;
