import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import { CPagination } from "@coreui/react";
import Select from 'react-select'
const options = [
  { value: "Value 1", label: "Value 1" },
  { value: "Value 2", label: "Value 2" }
];
const Event = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [page, setPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [type, setType] = useState("1");
  const [showHide, setShowHide] = useState(false);
  const [content, setContent] = useState("");
  const [listStore, setListStore] = useState("")
  const [store_id_set, setStore_id] = useState({ value: "", label: "Chọn cửa hàng" })
  const [store_id, setStore_id_push] = useState("")
  
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `notify/list-notify?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data?.data.list_notify?.data);
          setPage(res.data.data.list_notify.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

      dispatch(reloading(true));
      CallApi(
        `admin/store/list-store-with-push-notify`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setListStore(res.data.data)
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  }, [dispatch, currentPage]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  // const options = [
  //   // listStore.map((item, index) => {
  //   //   return (
  //   //     { value: item.id, label: item.name }
  //   //   );
  //   // })
  //   { value: 'chocolate', label: 'Chocolate' },
  //   { value: 'strawberry', label: 'Strawberry' },
  //   { value: 'vanilla', label: 'Vanilla' }
  // ]
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Đẩy");
    setContent("");
    setType("1");
    setStore_id("")
    setTitleModal("Đẩy thông báo");
  };
  // const handleFirstLevelChange = function (event) {
  //   console.log(1)
  //   // setStore_id(event.target.value)
  // }
  const UpdateAccount = () => {
    dispatch(reloading(true));
    if(store_id===""){
      CallApi(
      `admin/push-all-user `,
      "POST",
      {
        
        content,
        type,
      },
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));

        setShowHide(!showHide);

        swal(res.data.message);
        CallApi(
          `notify/list-notify?page=${currentPage}`,
          "GET",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setList(res.data?.data.list_notify?.data);
              setPage(res.data.data.list_notify.last_page);
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });

        swal(res.data.message);
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
    }
    else{
      CallApi(
        `admin/push-all-user `,
        "POST",
        {
          store_id,
          content,
          type,
        },
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
  
          setShowHide(!showHide);
  
          swal(res.data.message);
          CallApi(
            `notify/list-notify?page=${currentPage}`,
            "GET",
            null,
            localStorage.getItem("token")
          )
            .then((res) => {
              dispatch(reloading(false));
              if (res.data.status === 1) {
                setList(res.data?.data.list_notify?.data);
                setPage(res.data.data.list_notify.last_page);
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              }
            })
            .catch((error) => {
              dispatch(reloading(true));
              // swal("Vui lòng kiểm tra internet");
            });
  
          swal(res.data.message);
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    }
    
  };
  const changeLocation = (link) => {
    window.location = link;
  };
  const ConfirmNoti = (item) => {
    CallApi(
      `notify/confirm-view-notify?notify_id=${item.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        if (res.data.status === 1) {
          if (item.type === 1) {
            changeLocation(
              `/#/Quan-ly-cua-hang/chi-tiet-cua-hang/${item.object_id}`
            );
          }
          CallApi(
          `notify/list-notify?page=${currentPage}`,
          "GET",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setList(res.data?.data.list_notify?.data);
              setPage(res.data.data.list_notify.last_page);
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Nội dung</Form.Label>
                  <Form.Control
                    type="text"
                    name="money"
                    value={content}
                    placeholder="Nội dung..."
                    onChange={(e) => setContent(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                <Form.Label>Cửa hàng</Form.Label>
                {/* <Form.Control
                  as="select"
                  name="store_id"
                  value={store_id}
                  onChange={(e) => setStore_id(e.target.value)}
                >
                  <option value="">Chọn cửa hàng</option>
                  {listStore.map((item, index) => {
                    return (
                      <option value={item.id} key={index}>
                        {item.name}
                      </option>
                    );
                  })}
                </Form.Control> */}
                
                <Select 
                  value={store_id_set}
                  onChange={(e) =>{
                    setStore_id(e)
                    setStore_id_push(e.value)
                  
                  } } 
                  options={listStore.map((item, index) => {
                    return (
                      { value: item.id, label: item.name }
                    );
                  })} 
                 
                  placeholder="Chọn cửa hàng"
                  />
              </Form.Group>
                <Form.Group>
                  <Form.Label>Chọn lọai</Form.Label>
                  <Form.Control
                    as="select"
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                  >
                    <option value="1">app khách</option>
                    <option value="2">Driver</option>
                    <option value="3">Shop</option>
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách thông báo
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "5px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "14px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "5px 15px",
                  cursor: "pointer",
                }}
              >
                Đẩy thông báo
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "50%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Nội dung
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Ngày tạo
                </th>
                <th
                  style={{
                    width: "12%",
                    textAlign: "center",
                  }}
                >
                  Trạng thái
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      onClick={() => ConfirmNoti(item)}
                    >
                      <td>{item?.content}</td>
                      <td>{item?.created_at}</td>
                      <td>{item?.is_view === 1 ? "Đã xem" : "Chưa xem"}</td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
          <br></br>
        </div>
      </div>
    </>
  );
};
export default Event;
