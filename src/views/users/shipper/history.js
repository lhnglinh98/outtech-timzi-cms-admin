import React, { useState, useEffect } from "react";
import { CCard, CCardBody, CCol, CRow } from "@coreui/react";
// import { Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import CallApi from "../../../Middleware/api";
// import { useHistory } from "react-router-dom";
import { reloading } from "../../../Redux/action/index";
const History = ({ match }) => {
    // const history = useHistory();
    const dispatch = useDispatch();
    const [list, setList] = useState([]);
    // const changeLocation = (link) => {
    //   window.location = link;
    // };
    useEffect(() => {
        dispatch(reloading(true));
        CallApi(
            `admin/account/list-history-money-shipper/${match.params.id}`,
            "GET",
            null,
            localStorage.getItem("token")
        )
            .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                    setList(res.data.data.data);
                } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                ) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    //   changeLocation(`/#/users`);
                }
            })
            .catch((error) => {
                // swal("Vui lòng kiểm tra internet");
            });
    }, [dispatch, match.params.id]);

    return (
        <CRow >
            <CCol lg={10} className="mx-auto">
                <CCard>
                    {/* <CCardHeader>Thông tin sinh viên</CCardHeader> */}
                    <CCardBody>
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr className="font-weight-bold">
                                    <td>Nội dung</td>
                                    <td>Số tiền</td>
                                    <td>Loại</td>
                                    <td>Ngày cập nhật</td>
                                </tr>
                            </thead>
                            {list.length > 0 ? (
                                <tbody>
                                    {list.map((item) => {
                                        return (
                                            <tr
                                                key={item.id}>
                                                <td>{item.content}</td>
                                                <td>{Number(item.money)
                                                    .toString()
                                                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}</td>
                                                <td>{item.type === 1 ? "Cộng tiền" : "Trừ tiền"}</td>
                                                <td>{item?.created_at?.slice(8, 10)}-
                                                    {item?.created_at?.slice(5, 7)}-
                                                    {item?.created_at?.slice(0, 4)}</td>
                                            </tr>
                                        );
                                    })}
                                </tbody>) : (
                                <tbody>
                                    <tr>
                                        <td
                                            colSpan="10"
                                            style={{
                                                textAlign: "center",
                                                padding: "10px",
                                            }}
                                        >
                                            Không có dữ liệu
                                        </td>
                                    </tr>
                                </tbody>
                            )}
                        </table>
                    </CCardBody>
                </CCard>
            </CCol>

        </CRow>
    );
};

export default History;
