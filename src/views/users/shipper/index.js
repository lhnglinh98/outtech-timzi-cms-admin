import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import CallApi from "../../../Middleware/api";
import swal from "sweetalert";
import { Table, Button, Modal, Form } from "react-bootstrap";
import Add from "@material-ui/icons/AddCircle";
import Edit from "@material-ui/icons/Edit";
import { CPagination } from "@coreui/react";
import Visibility from "@material-ui/icons/Visibility";
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import reloading from "../../../Redux/action/index";
import Delete from "@material-ui/icons/Delete";
import ToggerOn from "@material-ui/icons/ToggleOn";
import ToggerOff from "@material-ui/icons/ToggleOff";
import Search from "@material-ui/icons/Search";
import { useHistory } from "react-router-dom";
import CachedIcon from '@material-ui/icons/Cached';
const Colors = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [showHide, setShowHide] = useState(false);
  const [search, setSearch] = useState(false);
  const [phone, setPhone] = useState("");
  const [status, setStatus] = useState("");
  const [name, setName] = useState("");
  const [role_id, setRole_id] = useState("4");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password_confirmation, setPassword_confirmation] = useState("");
  const [page, setPage] = useState(0);
  const [id, setId] = useState("");
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [total,setTotal] = useState("");
  const [listProvice, setListProvice] = useState([]);
  const [province_id, setProvince_id] = useState("");
  const [district_id, setDistrict_id] = useState("");
  const [listDistrict, setListDistrict] = useState([]);
  // const counter = useSelector((state) => state.loading);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/account/list-shipper?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          
          setList(res.data.data.user.data);
          setPage(res.data.data.user.last_page);
          setTotal(res.data.data.user_count);
        } else if (
          res.data.message === "Token không hợp lệ" ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
      CallApi(`list-province`, "GET", null, localStorage.getItem("token"))
      .then((res) => {
        if (res.data.status === 1) {
          setListProvice(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });

    CallApi(
      `list-district?province_id=${province_id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        // console.log(province_id);
        if (res.data.status === 1) {
          setListDistrict(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, currentPage, province_id]);
  // const changeLocation = (link) => {
  //   window.location = link;
  // };
  const toggleModal1 = () => {
    setShowHide(!showHide);
    setId("");
    setSearch(false);
    setTitleModal("Thêm tài khoản");
    setSubmitModal("Thêm");
    setName("");
    setPhone("");
    setEmail("");
    setRole_id("4");
    setPassword("");
    setPassword_confirmation("");
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setId(item.id);
    setSearch(false);
    setTitleModal("Cập nhật tài khoản");
    setSubmitModal("Cập nhật");
    setName(item.name);
    setPhone(item.phone);
    setEmail(item.email);
    setRole_id(item.role_id);
    setPassword("");
    setPassword_confirmation("");
  };
  const toggleModal = () => {
    setShowHide(!showHide);
    setId("");
    setTitleModal("Tìm kiếm");
    setSubmitModal("Tìm");
    setSearch(true);
  };

  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const OnchangeInputTextsatus = (e) => {
    setStatus(e.target.value);
  };
  const OnchangeInputText = (e) => {
    setName(e.target.value);
  };
  const OnchangeInputTextsdt = (e) => {
    setPhone(e.target.value);
  };
  const OnchangeInputTextemail = (e) => {
    setEmail(e.target.value);
  };
  const OnchangeInputTextrole_id = (e) => {
    setRole_id(e.target.value);
  };
  const OnchangeInputTextpassword_confirmation = (e) => {
    setPassword_confirmation(e.target.value);
  };
  const OnchangeInputTextpassword = (e) => {
    setPassword(e.target.value);
  };
  const reSetPassword = (item) => {
    swal({
      title: "Bạn chắc chắn muốn reset mật khẩu?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/account/reset-password/${item.id}`,
          "PUT",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã reset mật khẩu thành công!", "success");
              setList([]);
              setTotal(0);
              CallApi(
                `admin/account/list-shipper?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  if (res.data.status === 1) {
                    setList(res.data.data.user.data);
                    setPage(res.data.data.user.last_page);
                    setTotal(res.data.data.user_count);
                  } else if (
                    res.data.message === "Token không hợp lệ" ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  swal("Có gì đó không ổn !!!");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/account/delete-account/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              setTotal(0);
              CallApi(
                `admin/account/list-shipper?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  if (res.data.status === 1) {
                    setList(res.data.data.user.data);
                    setPage(res.data.data.user.last_page);
                    setTotal(res.data.data.user_count);
                  } else if (
                    res.data.message === "Token không hợp lệ" ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  swal("Có gì đó không ổn !!!");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };

  const lockAccount = (item) => {
    dispatch(reloading(true));
    CallApi(
      `admin/account/lock-account/${item.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    ).then((res) => {
      dispatch(reloading(false));
      swal(res.data.message);
      CallApi(
        search
          ? `admin/account/list-shipper?role_id=${role_id}&name=${name}&phone=${phone}&status=${status}`
          : `admin/account/list-shipper?page=${currentPage}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));

          if (res.data.status === 1) {
            setList(res.data.data.user.data);
            setPage(res.data.data.user.last_page);
            setTotal(res.data.data.user_count);
          } else if (
            res.data.message === "Token không hợp lệ" ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          // swal("Vui lòng kiểm tra internet");
        });
    });
  };
  const UnlockAccount = (item) => {
    dispatch(reloading(true));
    CallApi(
      `admin/account/unlock-account/${item.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    ).then((res) => {
      dispatch(reloading(false));
      swal(res.data.message);
      CallApi(
        search
          ? `admin/account/list-shipper?role_id=${role_id}&name=${name}&phone=${phone}&status=${status}`
          : `admin/account/list-shipper?page=${currentPage}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));

          if (res.data.status === 1) {
            setList(res.data.data.user.data);
            setPage(res.data.data.user.last_page);
            setTotal(res.data.data.user_count);
          } else if (
            res.data.message === "Token không hợp lệ" ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          // swal("Vui lòng kiểm tra internet");
        });
    });
  };

  const NapMoney = (item) => {
    dispatch(reloading(true));
    if (search) {
      CallApi(
        `admin/account/list-shipper?role_id=${role_id}&name=${name}&phone=${phone}&status=${status}&province_id=${province_id}&district_id=${district_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          setShowHide(!showHide);
          if (res.data.status === 1) {
            setList(res.data.data.user.data);
            setPage(res.data.data.user.last_page);
            setTotal(res.data.data.user_count);
          } else if (
            res.data.message === "Token không hợp lệ" ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          // swal("Vui lòng kiểm tra internet");
        });
    } else {
      if (id === "") {
        CallApi(
          `admin/account/create-account`,
          "POST",
          {
            name: name,
            phone: phone,
            email: email,
            role_id: role_id,
            password: password,
            password_confirmation: password_confirmation,
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            // dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã thêm thành công!", "success");
              CallApi(
                `admin/account/list-shipper?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));

                  if (res.data.status === 1) {
                    setList(res.data.data.user.data);
                    setPage(res.data.data.user.last_page);
                    setTotal(res.data.data.user_count);
                  } else if (
                    res.data.message === "Token không hợp lệ" ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Token không hợp lệ" ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        CallApi(
          `admin/account/update-account/${id}`,
          "POST",
          {
            name: name,
            phone: phone,
            email: email,
            role_id: role_id,
            password: password,
            password_confirmation: password_confirmation,
            _method: "put",
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            // dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");
              CallApi(
                `admin/account/list-shipper?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));

                  if (res.data.status === 1) {
                    setList(res.data.data.user.data);
                    setPage(res.data.data.user.last_page);
                    setTotal(res.data.data.user_count);
                  } else if (
                    res.data.message === "Token không hợp lệ" ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Token không hợp lệ" ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            // swal("Vui lòng kiểm tra internet");
          });
      }
    }
  };
  return (
    <div className="card">
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group>
              <Form.Label>Tên shipper</Form.Label>
              <Form.Control
                type="text"
                name="name"
                defaultValue={name}
                placeholder="Tên shipper..."
                onChange={(e) => OnchangeInputText(e)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Số điện thoại</Form.Label>
              <Form.Control
                type="text"
                name="phone"
                defaultValue={phone}
                placeholder="Số điện thoại..."
                onChange={(e) => OnchangeInputTextsdt(e)}
              />
            </Form.Group>
            <Form.Group>
                  <Form.Label>Tỉnh / thành phố</Form.Label>
                  <Form.Control
                    as="select"
                    name="province_id"
                    value={province_id}
                    onChange={(e) => setProvince_id(e.target.value)}
                  >
                    <option value=""> Chọn tỉnh / thành phố</option>
                    {listProvice.map((item, index) => {
                      return (
                        <option key={index} value={item.id}>
                          {item.name}
                        </option>
                      );
                    })}
                  </Form.Control>
                </Form.Group>

                <Form.Group>
                  <Form.Label>Quận / huyện</Form.Label>
                  <Form.Control
                    as="select"
                    name="district_id"
                    value={district_id}
                    onChange={(e) => setDistrict_id(e.target.value)}
                  >
                    <option value=""> Chọn Quận / huyện</option>
                    {listDistrict.map((item, index) => {
                      return (
                        <option key={index} value={item.id}>
                          {item.name}
                        </option>
                      );
                    })}
                  </Form.Control>
                </Form.Group>
            {!search ? (
              <>
                {id !== "" ? (<><Form.Group>
                      <Form.Label>Chức vụ</Form.Label>
                      <Form.Control
                        as="select"
                        name="role_id"
                        value={role_id}
                        disabled
                        onChange={(e) => OnchangeInputTextrole_id(e)}
                      >
                        <option value=""> Chọn chức vụ</option>
                        <option value="1">Admin</option>
                        <option value="2">Chủ chuỗi cửa hàng</option>
                        <option value="3">Nhân viên</option>
                        <option value="4">Shipper</option>
                        <option value="5">Khách hàng</option>
                        <option value="6">Chủ cửa hàng</option>
                      </Form.Control>
                    </Form.Group>
                  <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="text"
                      name="email"
                      defaultValue={email}
                      placeholder="Email..."
                      onChange={(e) => OnchangeInputTextemail(e)}
                    />
                  </Form.Group>
                  </>
                ) : (
                  <>
                    {" "}
                    <Form.Group>
                      <Form.Label>Chức vụ</Form.Label>
                      <Form.Control
                        as="select"
                        name="role_id"
                        value={role_id}
                        disabled
                        onChange={(e) => OnchangeInputTextrole_id(e)}
                      >
                        <option value=""> Chọn chức vụ</option>
                        <option value="1">Admin</option>
                        <option value="2">Chủ chuỗi cửa hàng</option>
                        <option value="3">Nhân viên</option>
                        <option value="4">Shipper</option>
                        <option value="5">Khách hàng</option>
                        <option value="6">Chủ cửa hàng</option>
                      </Form.Control>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Mật khẩu</Form.Label>
                      <Form.Control
                        type="password"
                        name="password"
                        defaultValue={password}
                        placeholder="Mật khẩu..."
                        onChange={(e) => OnchangeInputTextpassword(e)}
                      />
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Xác nhận mật khẩu</Form.Label>
                      <Form.Control
                        type="password"
                        name="password_confirmation"
                        defaultValue={password_confirmation}
                        placeholder="Xác nhận mật khẩu..."
                        onChange={(e) =>
                          OnchangeInputTextpassword_confirmation(e)
                        }
                      />
                    </Form.Group>
                  </>
                )}
              </>
            ) : (
              <Form.Group>
                <Form.Label>Trạng thái</Form.Label>
                <Form.Control
                  as="select"
                  name="status"
                  value={status}
                  onChange={(e) => OnchangeInputTextsatus(e)}
                >
                  <option value=""> Chọn Trạng thái</option>
                  <option value="0">Chưa kích hoạt</option>
                  <option value="1">Đã kích hoạt</option>
                  <option value="2">Đã khóa</option>
                </Form.Control>
              </Form.Group>
            )}
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => NapMoney()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}

      <div className="card-header">
        Danh sách tài khoản shipper
        <div className="card-header-actions">
          <small className="text-muted">
            <Button
              onClick={() => toggleModal()}
              className="label theme-bg text-white f-12"
              style={{
                borderRadius: "15px",
                backgroundImage: " linear-gradient(#3776a9, #004780)",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                fontSize: "12px",
                padding: "3px 10px",
                cursor: "pointer",
              }}
            >
              <Search style={{ fontSize: "20px" }} />
            </Button>
          </small>
          <small className="text-muted" style={{ marginLeft: "10px" }}>
            <Button
              onClick={() => toggleModal1()}
              className="label theme-bg text-white f-12"
              style={{
                borderRadius: "15px",
                backgroundImage: " linear-gradient(#3776a9, #004780)",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                fontSize: "12px",
                padding: "3px 10px",
                cursor: "pointer",
              }}
            >
              <Add style={{ fontSize: "20px" }} />
            </Button>
          </small>
        </div>
      </div>
      <div className="card-body">
        <Table bordered hover responsive id="TableRespon1">
          <thead>
            <tr>
              <th
                style={{
                  width: "10%",
                  wordBreak: "break-word",
                  textAlign: "center",
                }}
              >
                Avatar
              </th>
              <th
                style={{
                  width: "11%",
                  wordBreak: "break-word",
                  textAlign: "center",
                }}
              >
                Mã tài khoản
              </th>
              <th
                style={{
                  width: "20%",
                  textAlign: "center",
                }}
              >
                Tên tài khoản
              </th>

              <th
                style={{
                  width: "15%",
                  textAlign: "center",
                }}
              >
                Số điện thoại
              </th>

              <th
                style={{
                  width: "15%",
                  textAlign: "center",
                }}
              >
                Trạng thái
              </th>
              <th
                style={{
                  width: "15%",
                  textAlign: "center",
                }}
              >
                Chức vụ
              </th>

              <th
                style={{
                  width: "13%",
                  textAlign: "center",
                }}
              >
                Tùy chọn
              </th>
            </tr>
          </thead>
          {list.length > 0 ? (
            <tbody>
              {list.map((item, index) => {
                return (
                  <tr
                    key={index}
                    style={{
                      textAlign: "center",
                      cursor: "pointer",
                    }}
                  >
                    <td>
                      <img
                        src={item.avatar}
                        alt="linkAvatar"
                        style={{
                          width: "60px",
                          textAlign: "center",
                          borderRadius: "100%",
                        }}
                      />
                    </td>
                    <td>{item.code}</td>
                    <td>
                      <p>{item.name}</p>
                    </td>
                    <td>{item.phone}</td>

                    <td>
                      {item.status === 2 ? (
                        <ToggerOn
                          onClick={() => UnlockAccount(item)}
                          style={{
                            border: "0px",
                            color: "gray",
                            fontWeight: "bold",
                            fontSize: "40px",
                          }}
                        />
                      ) : null}
                      {item.status === 1 ? (
                        <ToggerOff
                          onClick={() => lockAccount(item)}
                          style={{
                            border: "0px",
                            color: "#1cbc49",
                            fontWeight: "bold",
                            fontSize: "40px",
                          }}
                        />
                      ) : null}
                      {item.status === 0 ? "Chưa kích hoạt" : null}
                    </td>
                    <td>
                      {item.role_id === 1 ? "Admin" : null}
                      {item.role_id === 2 ? "Chủ chuỗi cửa hàng " : null}
                      {item.role_id === 3 ? "Nhân viên" : null}
                      {item.role_id === 4 ? "Shipper" : null}
                      {item.role_id === 5 ? "Khách hàng " : null}
                      {item.role_id === 6 ? "Chủ cửa hàng" : null}
                    </td>
                    <td>
                      <span
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          // flexWrap: "wrap",
                        }}
                      >
                        <Button
                          onClick={() => toggleModal2(item)}
                          className="label theme-bg text-white f-12"
                          style={{
                            borderRadius: "15px",
                            border: "0px",
                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                            float: "right",
                            fontSize: "12px",
                            backgroundImage:
                              " linear-gradient(#3776a9, #004780)",
                            margin: "5px",
                            padding: "3px 5px",
                            cursor: "pointer",
                          }}
                        >
                          <Edit style={{ fontSize: "20px" }} />
                        </Button>
                        <Button
                          onClick={() => reSetPassword(item)}
                          className="label theme-bg text-white f-12"
                          style={{
                            borderRadius: "15px",
                            border: "0px",
                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                            float: "right",
                            fontSize: "12px",
                            backgroundImage:
                              " linear-gradient(#3776a9, #004780)",
                            margin: "5px",
                            padding: "3px 5px",
                            cursor: "pointer",
                          }}
                        >
                          <CachedIcon style={{ fontSize: "20px" }} />
                        </Button>
                        <Button
                          onClick={() => DeleteItem(item)}
                          className="label theme-bg text-white f-12"
                          style={{
                            borderRadius: "15px",
                            border: "0px",
                            background: "red",
                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                            float: "right",
                            fontSize: "10px",
                            padding: "3px 5px",
                            margin: "5px",
                            cursor: "pointer",
                          }}
                        >
                          <Delete style={{ fontSize: "20px" }} />
                        </Button>
                        <Button
                          onClick={() =>
                            history.push(`/tai-khoan-apps-shop/${item.id}`)
                          }
                          className="label theme-bg text-white f-12"
                          style={{
                            borderRadius: "15px",
                            border: "0px",
                            marginRight: "0px",
                            backgroundImage:
                              " linear-gradient(#3776a9, #004780)",
                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                            float: "right",
                            fontSize: "10px",
                            padding: "3px 5px",
                            margin: "5px",
                            cursor: "pointer",
                          }}
                        >
                          <Visibility style={{ fontSize: "20px" }} />
                        </Button>
                        <Button
                          onClick={() =>
                            history.push(`/lich-su-dong-tien/${item.id}`)
                          }
                          className="label theme-bg text-white f-12"
                          style={{
                            borderRadius: "15px",
                            border: "0px",
                            marginRight: "0px",
                            backgroundImage:
                              " linear-gradient(#3776a9, #004780)",
                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                            float: "right",
                            fontSize: "10px",
                            padding: "3px 5px",
                            margin: "5px",
                            cursor: "pointer",
                          }}
                        >
                          <AccountBalanceIcon style={{ fontSize: "20px" }} />
                        </Button>
                      </span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          ) : (
            <tbody>
              <tr>
                <td
                  colSpan="7"
                  style={{
                    textAlign: "center",
                    padding: "10px",
                  }}
                >
                  Không có dữ liệu
                </td>
              </tr>
            </tbody>
          )}
        </Table>
        <div className="row">
          <div className="col-md-6"><p>Tổng số bản ghi: {total}</p></div>
          <div className="col-md-6">
            <CPagination
              style={{float: "right"}}
              align="center"
              addListClass="some-class"
              activePage={currentPage}
              pages={page}
              onActivePageChange={setCurrentPage}
            />
          </div>
        </div>
        <br></br>
      </div>
    </div>
  );
};

export default Colors;
