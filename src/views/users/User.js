import React, { useState, useEffect } from "react";
import { CCard, CCardBody, CCol, CRow } from "@coreui/react";
// import { Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import CallApi from "../../Middleware/api";
// import { useHistory } from "react-router-dom";
import { reloading } from "../../Redux/action/index";
const DetailEvent = ({ match }) => {
  // const history = useHistory();
  const dispatch = useDispatch();
  const [list, setList] = useState("");
  // const changeLocation = (link) => {
  //   window.location = link;
  // };
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/account/account-detail/${match.params.id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          //   changeLocation(`/#/users`);
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, match.params.id]);

  return (
    <CRow>
      <CCol lg={6}>
        <CCard>
          {/* <CCardHeader>Thông tin sinh viên</CCardHeader> */}
          <CCardBody>
            <table className="table table-striped table-hover">
              <tbody>
                <tr>
                  <td>Hình ảnh</td>
                  <td>
                    <img
                      src={list.avatar}
                      alt="linkAvatar"
                      style={{
                        width: "60%",
                        maxWidth: "250px",
                        textAlign: "center",
                        // borderRadius: "100%",
                      }}
                    />
                  </td>
                </tr>
                <tr>
                  <td>Họ tên</td>
                  <td>
                    <strong>{list.name}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Mã tài khoản</td>
                  <td>
                    <strong>{list.code}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>{list.email}</td>
                </tr>
                <tr>
                  <td>Số điện thoại</td>
                  <td>
                    <strong>{list.phone}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Ngày sinh</td>
                  <td>
                    <strong>{list.birthday}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Giới tính</td>
                  <td>{list?.gender === 1 ? "Nam" : "Nữ"}</td>
                </tr>
                <tr>
                  <td>Địa chỉ</td>
                  <td>{list?.address}</td>
                </tr>
                <tr>
                  <td>Nghề nghiệp</td>
                  <td>{list?.career}</td>
                </tr>
               
              </tbody>
            </table>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol lg={6}>
        <CCard>
          {/* <CCardHeader>Thông tin sinh viên</CCardHeader> */}
          <CCardBody>
            <table className="table table-striped table-hover">
              <tbody>
                <tr>
                  <td>Chức vụ </td>
                  <td>
                    {list?.role?.name}
                  </td>
                </tr>
                <tr>
                  <td>Bằng lái xe</td>
                  <td>
                    {list.driver_license_image ? (
                      <img
                        src={list.driver_license_image}
                        alt="linkAvatar"
                        style={{
                          width: "60%",
                          maxWidth: "250px",
                          textAlign: "center",
                          // borderRadius: "100%",
                        }}
                      />
                    ) : null}
                  </td>
                </tr>
                <tr>
                  <td>Đăng ký xe</td>
                  <td>
                    {list.vehicle_registration_image ? (
                      <img
                        src={list.vehicle_registration_image}
                        alt="linkAvatar"
                        style={{
                          width: "60%",
                          maxWidth: "250px",
                          textAlign: "center",
                          // borderRadius: "100%",
                        }}
                      />
                    ) : null}
                  </td>
                </tr>
                <tr>
                  <td>CMT/CCCD mặt trước</td>
                  <td>
                    {list.image_cmnd_front ? (
                      <img
                        src={list.image_cmnd_front}
                        alt="linkAvatar"
                        style={{
                          width: "60%",
                          maxWidth: "250px",
                          textAlign: "center",
                          // borderRadius: "100%",
                        }}
                      />
                    ) : null}
                  </td>
                </tr>
                <tr>
                  <td>CMT/CCCD mặt sau</td>
                  <td>
                    {list.image_cmnd_back ? (
                      <img
                        src={list.image_cmnd_back}
                        alt="linkAvatar"
                        style={{
                          width: "60%",
                          maxWidth: "250px",
                          textAlign: "center",
                          // borderRadius: "100%",
                        }}
                      />
                    ) : null}
                  </td>
                </tr>
              </tbody>
            </table>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default DetailEvent;
