import React, { useState } from "react";
// import { Link } from 'react-router-dom';
import swal from "sweetalert";
import anh1 from "../../../assets/Untitled-2.png";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import {useDispatch } from "react-redux";
import CIcon from "@coreui/icons-react";
import CallApi from "../../../Middleware/api";
import { useForm } from "react-hook-form";
import {reloading} from "../../../Redux/action/index"
const Login = () => {
  const dispatch = useDispatch();
  const { handleSubmit } = useForm();
  const [phone, setPhone] = useState("");
  const [otp, setOtp] = useState("");
  const [checkOtp, setCheckOtp] = useState(false);
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("")
  const LoginFC = () => {
    dispatch(reloading(true));
    if (!checkOtp) {
      CallApi(
        "admin/login-admin",
        "POST",
        {
          phone,
          password,
          device_id: "389afe0d-332d-4727-84b9-b30d7f2eb0f8",
        },
        null
      ).then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setCheckOtp(true);
          setMessage(res.data.message)
        } else {
          swal(res.data.message);
        }
      });
    } else {
      CallApi(
        "admin/check-otp-verify",
        "PUT",
        {
          otp,
        },
        null
      ).then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          localStorage.setItem("token", res.data.data.token);
          changeLocation("/");
        } else {
          swal(res.data.message);
        }
      });
    }
  };
  const changeLocation = (link) => {
    window.location = link;
  };
  const OnchangeInputTexttaikhoan = (e) => {
    setPhone(e.target.value);
  };
  const OnchangeInputTextpassword = (e) => {
    setPassword(e.target.value);
  };
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleSubmit(LoginFC)}>
                    <h1>Đăng nhập</h1>
                    <p className="text-muted">
                     {checkOtp ? message : "Đăng nhập bằng tài khoản của bạn"} 
                    </p>
                    {!checkOtp ? <>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        placeholder="Số điện thoại"
                        name="phone"
                        value={phone}
                        onChange={(e) => OnchangeInputTexttaikhoan(e)}
                        autoComplete="username"
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        name="password"
                        value={password}
                        onChange={(e) => OnchangeInputTextpassword(e)}
                        placeholder="Mật khẩu"
                        autoComplete="current-password"
                      />
                    </CInputGroup>
                    </>
                    : <CInputGroup className="mb-4">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      type="text"
                      name="otp"
                      value={otp}
                      onChange={(e)=>setOtp(e.target.value)}
                      placeholder="OTP"
                      autoComplete="current-otp"
                    />
                  </CInputGroup> }
                    <CRow>
                      <CCol xs="12">
                        <CButton
                          color="primary"
                          type="submit"
                          className="px-4"
                          
                        >
                        Đăng nhập
                        </CButton>
                      </CCol>
                      {/* <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Forgot password?</CButton>
                      </CCol> */}
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard
                className="text-white bg-primary py-5 d-md-down-none"
                style={{ width: "44%" }}
              >
                <CCardBody className="text-center">
                  <img src={anh1} alt="" style={{ width: "200px" }} />
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
