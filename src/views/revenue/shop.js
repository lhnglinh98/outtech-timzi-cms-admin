import React, { useState, useEffect } from "react";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
// import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
// import { useHistory } from "react-router-dom";
import {
    CPagination
  } from '@coreui/react'
const Event = () => {
    // const history = useHistory();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [name, setName] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [showHide, setShowHide] = useState(false);
  const [page, setPage] = useState(1);
  const [store_id, setStore_id] = useState("")
  const [total, setTotal] = useState(1);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/revenue/list-revenue-store?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data.list_revenue.data);
          setTotal(res.data.data.total_money)
          setPage(res.data.data.list_revenue.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch,currentPage]);
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Tìm");
    setName("");
    setStore_id("");
    setTitleModal("Tìm kiếm");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    CallApi(
        `admin/revenue/list-revenue-store?page=${currentPage}&month=${name ? `${name?.slice(0, 4)}-${name?.slice(5, 7)}` :""}&store_name=${store_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
              setShowHide(false)
              setList(res.data.data.list_revenue.data);
              setTotal(res.data.data.total_money)
              setPage(res.data.data.list_revenue.last_page);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  };
  const CloseModal = () => {
    setShowHide(!showHide);
  };

  
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên cửa hàng</Form.Label>
                  <Form.Control
                    type="text"
                    name="store_id"
                    defaultValue={store_id}
                    placeholder="Tên cửa hàng..."
                    onChange={(e) => setStore_id(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Tháng</Form.Label>
                  <Form.Control
                    type="date"
                    name="name"
                    defaultValue={name}
                    placeholder="Tháng..."
                    onChange={(e) => setName(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách doanh thu cửa hàng
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
            <h5>Tổng tiền :     {Number(total)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}</h5>
                          <br/>
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "7%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  ID
                </th>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên cửa hàng
                </th>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                   Doanh thu
                </th>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                   Phí chiết khấu
                </th>
               

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                 Trạng thái
                </th> 
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                 Tháng
                </th> 
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Số ngày nộp muộn
                </th>
                {/* <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                 Xem
                </th> */}
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{item.id}</td>
                      <td>{item.store?.name}</td>

                      <td>
                      {Number(item?.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>
                      {Number(item?.fee_money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>
                      {item.status === 0 ? "Chưa đến hạn" : null}
                      {item.status === 1 ? "Đã trả" : null}
                      {item.status === 2 ? "Đến hạn" : null}
                      {item.status === 3 ? "Quá hạn" : null}
                      </td>
                      <td>
                        {item?.month?.slice(5, 7)}-
                        {item?.month?.slice(0, 4)}
                      </td>
                      <td>
                      {item.late_day}
                      </td>
                      {/* <td>
                      <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                           onClick={() => history.push(`/doanh-thu-cua-hang/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Visibility style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td> */}
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
           
          </Table> <br/>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );
};
export default Event;
