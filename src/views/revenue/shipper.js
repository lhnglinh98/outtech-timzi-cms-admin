import React, { useState, useEffect } from "react";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
// import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";

// import { useHistory } from "react-router-dom";
import {
    CPagination
  } from '@coreui/react'
const Event = () => {
    // const history = useHistory();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [name, setName] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [showHide, setShowHide] = useState(false);
  const [page, setPage] = useState(1);
//   const [store_id, setStore_id] = useState("")
  const [total, setTotal] = useState(1);
  const [shipper_name, setShipName] = useState("");
  const [from_date, setFromDate] = useState("");
  const [to_date, setToDate] = useState("");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/revenue/list-revenue-shipper?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data.list_revenue.data);
          setTotal(res.data.data.total_money)
          setPage(res.data.data.list_revenue.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
      
  }, [dispatch,currentPage]);
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Tìm");
    setName("");
    setTitleModal("Tìm kiếm");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    CallApi(
        `admin/revenue/list-revenue-shipper?page=${currentPage}&shipper_name=${shipper_name}&from_date=${from_date}&to_date=${to_date}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
              setShowHide(false)
              setList(res.data.data.list_revenue.data);
              setTotal(res.data.data.total_money)
              setPage(res.data.data.list_revenue.last_page);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  };
  const CloseModal = () => {
    setShowHide(!showHide);
  };

  
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên shipper</Form.Label>
                  <Form.Control
                    type="text"
                    name="shipper_name"
                    placeholder="Tên shipper"
                    defaultValue={shipper_name}
                    onChange={(e) => setShipName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                <Row>
                    <Col md={6}>
                    <Form.Group>
                      <Form.Label>Từ ngày</Form.Label>
                      <Form.Control
                        type="date"
                        name="from_date"
                        placeholder="từ ngày"
                        defaultValue={from_date}
                        onChange={(e) => setFromDate(e.target.value)}
                      />
                    </Form.Group>
                  </Col>
                    <Col md={6}>
                      <Form.Group>
                        <Form.Label>Đến ngày</Form.Label>
                        <Form.Control
                          type="date"
                          name="to_date"
                          placeholder="Đến ngày"
                          defaultValue={to_date}
                          onChange={(e) => setToDate(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                </Row>
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách doanh thu shipper
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
            <h5>Tổng tiền :     {Number(total)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}</h5>
                          <br/>
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "7%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  ID
                </th>
                <th
                  style={{
                    width: "20%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên shipper
                </th>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                   Doanh thu
                </th>
                <th
                  style={{
                    width: "30%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                 Nội dung
                </th>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                 Ngày
                </th>
               

              
                {/* <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Số ngày nộp muộn
                </th> */}
                {/* <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                 Xem
                </th> */}
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{item.id}</td>
                      <td>{item.shipper?.name}</td>

                      <td>
                      {Number(item?.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                    
                      <td>
                        {item?.content}
                      </td>
                      <td>
                        {item?.created_at?.slice(8, 10)}-
                        {item?.created_at?.slice(5, 7)}-
                        {item?.created_at?.slice(0, 4)}
                        {/* {item?.created_at} */}
                      </td>
                      {/* <td>
                      {item.late_day}
                      </td> */}
                      {/* <td>
                      <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                           onClick={() => history.push(`/doanh-thu-cua-hang/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Visibility style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td> */}
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
           
          </Table> <br/>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );
};
export default Event;
