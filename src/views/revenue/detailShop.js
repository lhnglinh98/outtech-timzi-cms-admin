import React, { useState, useEffect } from "react";
import { CCard, CCardBody, CCol, CRow, CCardHeader } from "@coreui/react";
import { Button, Modal, Row, Col, Table } from "react-bootstrap";
import { useDispatch } from "react-redux";
import CallApi from "../../Middleware/api";
import { reloading } from "../../Redux/action/index";
import "../Shop/index.css";
const DetailEvent = ({ match }) => {
  const dispatch = useDispatch();
  const [titleModal, setTitleModal] = useState("");
  const [listCombo, setListCombo] = useState([]);
  const [list, setList] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [combo, setCombo] = useState(false);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/revenue/revenue-store-detail/${match.params.id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          //   changeLocation(`/#/users`);
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });

      CallApi(
        `store/store-detail-with-book-table/${match.params.id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setList(res.data.data);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          } else {
            //   changeLocation(`/#/users`);
          }
        })
        .catch((error) => {
          // swal("Vui lòng kiểm tra internet");
        });
  }, [dispatch, match.params.id]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal3 = (item) => {
    setShowHide(!showHide);
    setListCombo(item?.food);
    setCombo(true);
    setTitleModal("Danh sách món ăn trong combo");
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setListCombo(item?.food);
    setCombo(false);
    setTitleModal("Danh sách món ăn");
  };
  return (
    <CRow>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Table bordered hover responsive id="TableRespon1" style={{    minWidth: "800px"}}>
                  <thead>
                    <tr>
                      <th style={{ textAlign: "center" }}>Hình ảnh</th>
                      <th style={{ textAlign: "center" }}>Tên món</th>
                      <th style={{ textAlign: "center" }}>Giá</th>
                      {!combo ?<>
                      <td style={{ textAlign: "center" }}>Giá sau giảm</td>
                      <td style={{ textAlign: "center" }}>Giá áp dụng với chương trình</td>
                       <td style={{ textAlign: "center" }}>Topping</td>
                       
                        </>: null}
                    </tr>
                  </thead>
                  <tbody>
                    {listCombo?.map((item, index) => {
                      return (
                        <tr key={index}>
                          <td style={{ textAlign: "center" }}>
                          
                            <img
                              src={list.store?.image}
                              alt="linkAvatar"
                              style={{
                                width: "60%",
                                maxWidth: "60px",
                                textAlign: "center",
                                // borderRadius: "100%",
                              }}
                            />
                          </td>
                          <td style={{ textAlign: "center" }}>{item.name}</td>
                          <td style={{ textAlign: "center" }}>
                            {Number(item?.price)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                          </td>
                          {!combo ? <>
                            <td style={{ textAlign: "center" }}>{Number(item?.price_discount)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}</td> 
                            <td style={{ textAlign: "center" }}>{Number(item?.price_discount_with_program)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}</td> 
                          <td >{item.category_topping_food.map((item1,index1)=>{
                            return(
                              <span key={index1}>- {item1.name} <br/></span>
                            )
                          })}</td>
                          
                          </> : null}
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => CloseModal()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              Đóng
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <CCol lg={12}>
        <CCard>
          <CCardHeader>Thông tin cửa hàng</CCardHeader>
          <CCardBody>
            <table className="table table-striped table-hover">
              <tbody>
                <tr>
                  <td>Hình ảnh</td>
                  <td>
                    <img
                      src={list.store?.image}
                      alt="linkAvatar"
                      style={{
                        width: "60%",
                        maxWidth: "250px",
                        textAlign: "center",
                        // borderRadius: "100%",
                      }}
                    />
                  </td>
                </tr>
                <tr>
                  <td>Tên cửa hàng</td>
                  <td>
                    <strong>{list.store?.name}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Địa chỉ</td>
                  <td>
                    <strong>{list.store?.address}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Hotline</td>
                  <td>{list.store?.hotline}</td>
                </tr>
                <tr>
                  <td>Giá trung bình</td>
                  <td>
                    {Number(list.store?.average_price)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                  </td>
                </tr>
                <tr>
                  <td>Giờ mở cửa</td>
                  <td>
                    <strong>{list.store?.open_hours}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Giờ đóng cửa</td>
                  <td>{list?.store?.close_hours}</td>
                </tr>

                <tr>
                  <td>Tọa độ</td>
                  <td>
                    <strong>{list.store?.latitude}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Kinh độ</td>
                  <td>{list?.store?.longtidue}</td>
                </tr>
                <tr>
                  <td>Điểm đánh giá</td>
                  <td>{list?.store?.star}</td>
                </tr>
                <tr>
                  <td>Danh mục cửa hàng</td>
                  <td>
                    {list?.store?.category.map((item, index) => {
                      return <span key={index}>{item.name} , </span>;
                    })}
                  </td>
                </tr>
                <tr>
                  <td>Danh mục kinh doanh</td>
                  <td>
                    {list?.store?.category_store_detail.map((item, index) => {
                      return <span key={index}>{item.category_name} , </span>;
                    })}
                  </td>
                </tr>
              </tbody>
            </table>
          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12}>
        <CCard>
          <CCardHeader>Danh mục món ăn</CCardHeader>
          <CCardBody>
            <Table bordered hover responsive id="TableRespon1">
              <thead>
                <tr>
                  <th
                    style={{
                      maxWidth: "6%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                   ID
                  </th>
                  <th
                    style={{
                      width: "63%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                   Tên danh mục
                  </th>
                  <th
                    style={{
                      width: "30%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                   Danh sách món ăn
                  </th>
                  
                  
                </tr>
              </thead>
              {list?.category_food?.length > 0 ? (
                <tbody>
                  {list?.category_food?.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>{item.id}</td>
                        <td style={{ textAlign: "center" }}>
                        {item.name}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "5px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "center",
                              fontSize: "14px",
                              padding: "5px 15px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            Danh sách món ăn
                          </Button>
                        </td>
                       
                       
                      </tr>
                    );
                  })}
                </tbody>
              ) : (
                <tbody>
                  <tr>
                    <td
                      colSpan="9"
                      style={{
                        textAlign: "center",
                        padding: "10px",
                      }}
                    >
                      Không có dữ liệu
                    </td>
                  </tr>
                </tbody>
              )}
            </Table>
          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12}>
        <CCard>
          <CCardHeader>Combo đồ ăn</CCardHeader>
          <CCardBody>
            <Table bordered hover responsive id="TableRespon1">
              <thead>
                <tr>
                  <th
                    style={{
                      width: "10%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Hình ảnh
                  </th>
                  <th
                    style={{
                      width: "16%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Tên combo
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Nội dung
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Giá tiền
                  </th>
                  <th
                    style={{
                      width: "8%",
                      textAlign: "center",
                    }}
                  >
                    Số lượng
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Ngày mở
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Ngày đóng
                  </th>
                  <th
                    style={{
                      width: "17%",
                      textAlign: "center",
                    }}
                  >
                    Danh sách món ăn
                  </th>
                </tr>
              </thead>
              {list?.store?.combo_food.length > 0 ? (
                <tbody>
                  {list?.store?.combo_food.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>
                          {" "}
                          <img
                            src={list.store?.image}
                            alt="linkAvatar"
                            style={{
                              width: "60%",
                              maxWidth: "60px",
                              textAlign: "center",
                              // borderRadius: "100%",
                            }}
                          />
                        </td>
                        <td style={{ textAlign: "center" }}>{item.name}</td>
                        <td style={{ textAlign: "center" }}>{item.content}</td>
                        <td style={{ textAlign: "center" }}>
                          {Number(item?.price)
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                        </td>
                        <td style={{ textAlign: "center" }}>{item.quantity}</td>
                        <td style={{ textAlign: "center" }}>
                        {item?.time_open?.slice(8, 10)}-
                            {item?.time_open?.slice(5, 7)}-
                            {item?.time_open?.slice(0, 4)}
                          {/* {item.time_open} */}
                        </td>
                        <td style={{ textAlign: "center" }}>
                        {item?.time_close?.slice(8, 10)}-
                            {item?.time_close?.slice(5, 7)}-
                            {item?.time_close?.slice(0, 4)}
                          {/* {item.time_close} */}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          <Button
                            onClick={() => toggleModal3(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "5px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "center",
                              fontSize: "14px",
                              padding: "5px 15px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            Danh sách món ăn
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              ) : (
                <tbody>
                  <tr>
                    <td
                      colSpan="9"
                      style={{
                        textAlign: "center",
                        padding: "10px",
                      }}
                    >
                      Không có dữ liệu
                    </td>
                  </tr>
                </tbody>
              )}
            </Table>
          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12}>
        <CCard>
          <CCardHeader>Chương trình ưu đãi</CCardHeader>
          <CCardBody>
            <Table bordered hover responsive id="TableRespon1">
              <thead>
                <tr>
                  <th
                    style={{
                      width: "10%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Hình ảnh
                  </th>
                  <th
                    style={{
                      width: "16%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Tên chương trình
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Nội dung
                  </th>
                  <th
                    style={{
                      width: "8%",
                      textAlign: "center",
                    }}
                  >
                    Ưu đãi
                  </th>
                  <th
                    style={{
                      width: "12%",
                      textAlign: "center",
                    }}
                  >
                    Tên công khai
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Ngày mở
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Ngày đóng
                  </th>
                </tr>
              </thead>
              {list?.store?.program_store.length > 0 ? (
                <tbody>
                  {list?.store?.program_store.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>
                          {" "}
                          <img
                            src={list.store?.image}
                            alt="linkAvatar"
                            style={{
                              width: "60%",
                              maxWidth: "60px",
                              textAlign: "center",
                              // borderRadius: "100%",
                            }}
                          />
                        </td>
                        <td style={{ textAlign: "center" }}>{item.name}</td>
                        <td style={{ textAlign: "center" }}>{item.content}</td>
                        <td style={{ textAlign: "center" }}>
                          {item?.percent}%
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {item.name_public}
                        </td>
                        <td style={{ textAlign: "center" }}>
                        {item?.time_open?.slice(8, 10)}-
                            {item?.time_open?.slice(5, 7)}-
                            {item?.time_open?.slice(0, 4)}
                          {/* {item.time_open} */}
                        </td>
                        <td style={{ textAlign: "center" }}>
                        {item?.time_close?.slice(8, 10)}-
                            {item?.time_close?.slice(5, 7)}-
                            {item?.time_close?.slice(0, 4)}
                          {/* {item.time_close} */}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              ) : (
                <tbody>
                  <tr>
                    <td
                      colSpan="9"
                      style={{
                        textAlign: "center",
                        padding: "10px",
                      }}
                    >
                      Không có dữ liệu
                    </td>
                  </tr>
                </tbody>
              )}
            </Table>
          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12}>
        <CCard>
          <CCardHeader>Danh sách nhân viên</CCardHeader>
          <CCardBody>
            <Table bordered hover responsive id="TableRespon1">
              <thead>
                <tr>
                  <th
                    style={{
                      width: "10%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Mã nhân viên
                  </th>
                  <th
                    style={{
                      width: "10%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Hình ảnh
                  </th>
                  <th
                    style={{
                      width: "16%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Tên nhân viên
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Số điện thoại
                  </th>
                  <th
                    style={{
                      width: "8%",
                      textAlign: "center",
                    }}
                  >
                    Ngày sinh
                  </th>
                  <th
                    style={{
                      width: "12%",
                      textAlign: "center",
                    }}
                  >
                    Giới tính
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Email
                  </th>
                </tr>
              </thead>
              {list?.store?.staff.length > 0 ? (
                <tbody>
                  {list?.store?.staff.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>{item.code}</td>
                        <td style={{ textAlign: "center" }}>
                          {" "}
                          <img
                            src={list.store?.image}
                            alt="linkAvatar"
                            style={{
                              width: "60%",
                              maxWidth: "60px",
                              textAlign: "center",
                              // borderRadius: "100%",
                            }}
                          />
                        </td>
                        <td style={{ textAlign: "center" }}>{item.name}</td>
                        <td style={{ textAlign: "center" }}>{item.phone}</td>
                        <td style={{ textAlign: "center" }}>
                          {item?.birthday}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {item.gender === 1 ? "Nam" : "Nữ"}
                        </td>
                        <td style={{ textAlign: "center" }}>{item.email}</td>
                       
                      </tr>
                    );
                  })}
                </tbody>
              ) : (
                <tbody>
                  <tr>
                    <td
                      colSpan="9"
                      style={{
                        textAlign: "center",
                        padding: "10px",
                      }}
                    >
                      Không có dữ liệu
                    </td>
                  </tr>
                </tbody>
              )}
            </Table>
          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12}>
        <CCard>
          <CCardHeader>Bàn của cửa hàng</CCardHeader>
          <CCardBody>
            <Table bordered hover responsive id="TableRespon1">
              <thead>
                <tr>
                  <th
                    style={{
                      width: "10%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Mã bàn
                  </th>
                  <th
                    style={{
                      width: "10%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Số tầng
                  </th>
                  <th
                    style={{
                      width: "16%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                   Số người tối đa
                  </th>
                  <th
                    style={{
                      width: "10%",
                      textAlign: "center",
                    }}
                  >
                    Số người tối thiểu
                  </th>
                  <th
                    style={{
                      width: "8%",
                      textAlign: "center",
                    }}
                  >
                    Số bàn
                  </th>
                  <th
                    style={{
                      width: "8%",
                      textAlign: "center",
                    }}
                  >
                    Trạng thái
                  </th>
                  
                </tr>
              </thead>
              {list?.store?.table_store.length > 0 ? (
                <tbody>
                  {list?.store?.table_store.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>{item.code}</td>
                        <td style={{ textAlign: "center" }}>
                        {item.number_floor}
                        </td>
                        <td style={{ textAlign: "center" }}>{item.number_people_max}</td>
                        <td style={{ textAlign: "center" }}>{item.number_people_min}</td>
                        <td style={{ textAlign: "center" }}>
                          {item?.number_table}
                        </td>
                        <td style={{ textAlign: "center" }}>
                          {item?.status===1 ? "Trống" : "Đã đặt"}
                        </td>
                       
                      </tr>
                    );
                  })}
                </tbody>
              ) : (
                <tbody>
                  <tr>
                    <td
                      colSpan="9"
                      style={{
                        textAlign: "center",
                        padding: "10px",
                      }}
                    >
                      Không có dữ liệu
                    </td>
                  </tr>
                </tbody>
              )}
            </Table>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default DetailEvent;
