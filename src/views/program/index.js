import React, { useState, useEffect,createRef } from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Group3 from "../../assets/Group 14847 1.png";
import Delete from "@material-ui/icons/Delete";
const Event = () => {
    const fileInput3 = createRef();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [title, setTitle] = useState("");
  const [id, setId] = useState("");
  const [image, setImage] = useState("");
  const [hinhanh, setHinhanh] = useState("");
  const [description, setDescription] = useState("");

  const [shop, setShop] = useState([]);
  const [time_open, setTime_open] = useState("");
  const [time_close, setTime_close] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [money, setMoney] = useState("");
  const [store_id, setStore_id] = useState("");
  const [type_percent_or_money, setType_percent_or_money] = useState("1");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/program/list-program`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
    dispatch(reloading(true));
    CallApi(
      `admin/category-food/list-category-food`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setShop(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const OnchangeImage3 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);
    setHinhanh(file);
    setImage(e.target.files[0]);
  };
  const UploadImage3 = () => fileInput3.current.click();
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setTitle(item.name);
    setDescription(item.content);
    setType_percent_or_money(item.type_percent_or_money)
    setStore_id(item.category_food_id);
    setId(item.id);
    setImage("")
    setHinhanh(item.image)
    setMoney(item.type_percent_or_money===2 ? item.money : item.percent);
    setTime_close(item.time_close);
    setTime_open(item.time_open);
    setTitleModal("Cập nhật chương trình Timzi");
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setTitle("");
    setDescription("");
  
    setStore_id("");
    setImage("")
    setHinhanh("")
    setType_percent_or_money("1");
    setId("");
    setMoney("");
    setTime_close("");
    setTime_open("");
    setTitleModal("Thêm chương trình Timzi");
  };

  const UpdateAccount = () => {
    dispatch(reloading(true));
    let fileMoney = new FormData()
    fileMoney.append("name", title)
    fileMoney.append("category_food_id", store_id)
    fileMoney.append("money", money?.toString().replace(/,/gi, ""))
    fileMoney.append("percent",0)
    fileMoney.append("content", description)
    fileMoney.append("time_open", time_open)
    fileMoney.append("time_close", time_close)
    fileMoney.append("type_percent_or_money", type_percent_or_money)

    let filePercent = new FormData()
    filePercent.append("name", title)
    filePercent.append("category_food_id", store_id)
    filePercent.append("percent", money)
    filePercent.append("money",0)
    filePercent.append("content", description)
    filePercent.append("time_open", time_open)
    filePercent.append("time_close", time_close)
    filePercent.append("type_percent_or_money", type_percent_or_money)
   
    if (id === "") {
        filePercent.append("image", image)
        fileMoney.append("image", image)
        CallApi(
          `admin/program/create-program`,
          "POST",
          Number(type_percent_or_money)===2 ? fileMoney:filePercent,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã thêm thành công!", "success");

              CallApi(
                `admin/program/list-program`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      
    } else {
        filePercent.append("_method", "put")
        fileMoney.append("_method", "put")
        if (image !== "") {
          filePercent.append("image", image);
          fileMoney.append("image", image);
        }
        CallApi(
            `admin/program/update-program/${id}`,
            "POST",
            Number(type_percent_or_money)===2 ? fileMoney:filePercent,
            localStorage.getItem("token")
          )
            .then((res) => {
              dispatch(reloading(false));
              if (res.data.status === 1) {
                setShowHide(!showHide);
                swal("Thành công!", "Bạn đã cập nhật thành công!", "success");
  
                CallApi(
                  `admin/program/list-program`,
                  "GET",
                  null,
                  localStorage.getItem("token")
                )
                  .then((res) => {
                    dispatch(reloading(false));
                    if (res.data.status === 1) {
                      setList(res.data.data);
                    } else if (
                      res.data.message === "Không tìm thấy tài khoản." ||
                      res.data.message === "Token đã hết hạn"
                    ) {
                      localStorage.clear();
                      window.location.reload();
                    }
                  })
                  .catch((error) => {
                    dispatch(reloading(true));
                    // swal("Vui lòng kiểm tra internet");
                  });
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              } else {
                swal(res.data.message);
              }
            })
            .catch((error) => {
              dispatch(reloading(true));
              // swal("Vui lòng kiểm tra internet");
            });
    }
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/program/delete-program/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              CallApi(
                `admin/program/list-program`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên chương trình</Form.Label>
                  <Form.Control
                    type="text"
                    name="title"
                    value={title}
                    placeholder="Tên chương trình..."
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group>
                  <Form.Label>Bắt đầu</Form.Label>
                  <Form.Control
                    type="date"
                    name="time_open"
                    value={time_open}
                    onChange={(e) => setTime_open(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group>
                  <Form.Label>Kết thúc</Form.Label>
                  <Form.Control
                    type="date"
                    name="time_close"
                    value={time_close}
                    onChange={(e) => setTime_close(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Kiểu áp dụng</Form.Label>
                  <Form.Control
                    as="select"
                    name="type_percent_or_money"
                    value={type_percent_or_money}
                    onChange={(e) => setType_percent_or_money(e.target.value)}
                  >
                    <option value="2">Tiền</option>
                    <option value="1">Phần trăm</option>
                  </Form.Control>
                </Form.Group>

                <Form.Group>
                  <Form.Label>Chọn danh mục cửa hàng</Form.Label>
                  <Form.Control
                    as="select"
                    value={store_id}
                    onChange={(e) => setStore_id(e.target.value)}
                  >
                    <option value="">Chọn danh mục cửa hàng</option>
                    {shop.map((item, index) => {
                      return (
                        <option value={item.id} key={index}>
                          {item.name}
                        </option>
                      );
                    })}
                  </Form.Control>
                </Form.Group>
               
                {Number(type_percent_or_money) === 2 ? (
                  <Form.Group>
                    <Form.Label>Giá tiền</Form.Label>
                    <Form.Control
                      type="text"
                      name="money"
                      value={
                        Number(money.toString().replace(/,/gi, ""))
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          ? Number(money.toString().replace(/,/gi, ""))
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          : 0
                      }
                      placeholder="Giá tiền..."
                      onChange={(e) => setMoney(e.target.value)}
                    />
                  </Form.Group>
                ) : (
                  <Form.Group>
                    <Form.Label>Phần trăm (%)</Form.Label>
                    <Form.Control
                      type="text"
                      name="money"
                      value={money}
                      placeholder="Phần trăm..."
                      onChange={(e) => setMoney(e.target.value)}
                    />
                  </Form.Group>
                )}  
                 <Form.Group>
                  <Form.Label>Nội dung</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows="4"
                    name="description"
                    value={description}
                    placeholder="Nội dung..."
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Hình ảnh</Form.Label>
                  <input
                    ref={fileInput3}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage3(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {hinhanh === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={hinhanh}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>
                </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách chương trình Timzi
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
               
                <th
                  style={{
                    width: "17%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên chương trình
                </th>
                <th
                  style={{
                    width: "22%",
                    textAlign: "center",
                  }}
                >
                  Nội dung
                </th>

                <th
                  style={{
                    width: "12%",
                    textAlign: "center",
                  }}
                >
                  Số tiền
                </th>
                <th
                  style={{
                    width: "12%",
                    textAlign: "center",
                  }}
                >
                  Phần trăm
                </th>
                
                <th
                  style={{
                    width: "13%",
                    textAlign: "center",
                  }}
                >
                  Bắt đầu
                </th>
                <th
                  style={{
                    width: "13%",
                    textAlign: "center",
                  }}
                >
                  Kết thúc
                </th>
                <th
                  style={{
                    width: "13%",
                    textAlign: "center",
                  }}
                >
                  Trạng thái
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                     
                      <td>{item.name}</td>
                      <td>{item.content}</td>

                      <td>
                        {" "}
                        {Number(item?.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>{item.percent} %</td>
                     
                      <td>
                        {item?.time_open?.slice(8, 10)}-
                        {item?.time_open?.slice(5, 7)}-
                        {item?.time_open?.slice(0, 4)}
                      </td>
                      <td>
                        {item?.time_close?.slice(8, 10)}-
                        {item?.time_close?.slice(5, 7)}-
                        {item?.time_close?.slice(0, 4)}
                      </td>
                      <td>{item.status===0 ? "Chưa mở" :null}
                      {item.status===1 ? "Đang mở" :null}
                      {item.status===2 ? "Đã đóng" :null}
                       </td>
                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    </>
  );
};
export default Event;
