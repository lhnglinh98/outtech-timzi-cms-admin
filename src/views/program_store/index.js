import React, { useState, useEffect } from "react";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
// import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
import Select from 'react-select';
// import { useHistory } from "react-router-dom";
import {
    CPagination
  } from '@coreui/react'
const Event = () => {
    // const history = useHistory();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [name, setName] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [showHide, setShowHide] = useState(false);
  const [page, setPage] = useState(1);
//   const [store_id, setStore_id] = useState("")
  const [total, setTotal] = useState(1);
  const [shipper_name, setShipName] = useState("");
  const [from_date, setFromDate] = useState("");
  const [to_date, setToDate] = useState("");
  const [count_quantity, setCountQuantity] = useState("");
  const [listStore, setListStore] = useState("")
  const [store_id_set, setStore_id] = useState({ value: "", label: "Chọn cửa hàng" })
  const [store_id, setStore_id_push] = useState("")
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/program/list-program-store?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data.list.data);
          setTotal(res.data.data.sum_quantity_bought)
          setCountQuantity(res.data.data.count_program)
          setPage(res.data.data.list_revenue.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(false));
        // swal("Vui lòng kiểm tra internet");
      });
      dispatch(reloading(true));
      CallApi(
        `admin/store/list-store-with-push-notify`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setListStore(res.data.data)
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  }, [dispatch,currentPage]);
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Tìm");
    setName("");
    setTitleModal("Tìm kiếm");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    CallApi(
        `admin/program/list-program-store?page=${currentPage}&store_id=${store_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
              setShowHide(false)
              setList(res.data.data.list.data);
              setTotal(res.data.data.sum_quantity_bought)
              setCountQuantity(res.data.data.count_program)
              setPage(res.data.data.list_revenue.last_page);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(false));
          // swal("Vui lòng kiểm tra internet");
        });
  };
  const CloseModal = () => {
    setShowHide(!showHide);
  };

  
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên shipper</Form.Label>
                  <Select 
                  value={store_id_set}
                  onChange={(e) =>{
                    setStore_id(e)
                    setStore_id_push(e.value)
                  
                  } } 
                  options={listStore.map((item, index) => {
                    return (
                      { value: item.id, label: item.name }
                    );
                  })} 
                 
                  placeholder="Chọn cửa hàng"
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách chương trình cửa hàng
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
            <h5>Tổng số chương trình :     {Number(count_quantity)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " chương trình"}</h5>
                          <br/>
            <h5>Tổng số lượng món đã bán :     {Number(total)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " món"}</h5>
                          <br/>
          <Table bordered hover responsive id="TableRespon1">
            <thead>
            <tr>
               
               <th
                 style={{
                   width: "12%",
                   wordBreak: "break-word",
                   textAlign: "center",
                 }}
               >
                 Tên chương trình
               </th>
               <th
                 style={{
                   width: "12%",
                   wordBreak: "break-word",
                   textAlign: "center",
                 }}
               >
                 Tên cửa hàng
               </th>
               <th
                 style={{
                   width: "15%",
                   textAlign: "center",
                 }}
               >
                 Nội dung
               </th>

               <th
                 style={{
                   width: "10%",
                   textAlign: "center",
                 }}
               >
                 Số tiền
               </th>
               <th
                 style={{
                   width: "10%",
                   textAlign: "center",
                 }}
               >
                 Phần trăm
               </th>
               
               <th
                 style={{
                   width: "10%",
                   textAlign: "center",
                 }}
               >
                 Bắt đầu
               </th>
               <th
                 style={{
                   width: "10%",
                   textAlign: "center",
                 }}
               >
                 Kết thúc
               </th>
               <th
                 style={{
                   width: "10%",
                   textAlign: "center",
                 }}
               >
                 Trạng thái
               </th>
               <th
                 style={{
                   width: "7%",
                   textAlign: "center",
                 }}
               >
                 Đã bán
               </th>
               <th
                 style={{
                   width: "7%",
                   textAlign: "center",
                 }}
               >
                 Còn lại
               </th>
             </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                     
                      <td>{item.name}</td>
                      <td>{item.store.name}</td>
                      <td>{item.content}</td>

                      <td>
                        {" "}
                        {Number(item?.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>{item.percent} %</td>
                     
                      <td>
                        {item?.time_open?.slice(8, 10)}-
                        {item?.time_open?.slice(5, 7)}-
                        {item?.time_open?.slice(0, 4)}
                      </td>
                      <td>
                        {item?.time_close?.slice(8, 10)}-
                        {item?.time_close?.slice(5, 7)}-
                        {item?.time_close?.slice(0, 4)}
                      </td>
                      <td>{item.status===0 && item.is_active === 1? "Chưa mở" :null}
                      {item.status===1 && item.is_active === 1? "Đang mở" :null}
                      {item.status===2 || item.is_active === 0? "Đã đóng" :null}
                       </td>
                       <td>{item.quantity_bought}</td>
                       <td>{item.quantity}</td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
           
          </Table> <br/>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );
};
export default Event;
