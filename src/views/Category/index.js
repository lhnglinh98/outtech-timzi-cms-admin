import React, { useState, useEffect, createRef } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Group3 from "../../assets/Group 14847 1.png";
import Delete from "@material-ui/icons/Delete";
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import ViewListIcon from '@material-ui/icons/ViewList';
import Tooltip from "@material-ui/core/Tooltip";
const Event = () => {
  const fileInput3 = createRef();
  const fileInput2 = createRef();
  const fileInput1 = createRef();
  const history = useHistory();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const [image, setImage] = useState("");
  const [hinhanh, setHinhanh] = useState("");
  const [banner, setBanner] = useState("");
  const [banner_image, setBanner_image] = useState("");
  const [background, setBackground] = useState("");
  const [background_image, setBackground_image] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [eatStore, setEatStore] = useState(true);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/category/list-category`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const OnchangeImage3 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);
    setHinhanh(file);
    setImage(e.target.files[0]);
  };
  const UploadImage3 = () => fileInput3.current.click();

  const OnchangeImage1 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);
    setBanner_image(file);
    setBanner(e.target.files[0]);
  };
  const UploadImage1 = () => fileInput1.current.click();

  const OnchangeImage2 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);
    setBackground_image(file);
    setBackground(e.target.files[0]);
  };
  const UploadImage2 = () => fileInput2.current.click();
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setName(item.name);
    setBanner("")
    setId(item.id)
    setEatStore(item.is_eat_store)
    setBanner_image(item.banner)
    setImage("")
    setHinhanh(item.image)
    setBackground_image(item.background)
    setBackground("")
    setTitleModal("Cập nhật danh mục");
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setName("");
    setBanner("")
    setId("")
    setBanner_image("")
    setImage("")
    setHinhanh("")
    setBackground_image("")
    setBackground("")
    setTitleModal("Thêm danh mục");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    let file = new FormData();
    file.append("name", name)
    file.append("is_eat_store", eatStore)
    if (id === "") {
      file.append("image", image)
      file.append("banner", banner)
      file.append("background", background)
      CallApi(
        `admin/category/create-category`,
        "POST",
        file,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(!showHide);
            swal("Thành công!", "Bạn đã thêm thành công!", "success");

            CallApi(
              `admin/category/list-category`,
              "GET",
              null,
              localStorage.getItem("token")
            )
              .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                  setList(res.data.data);
                } else if (
                  res.data.message === "Không tìm thấy tài khoản." ||
                  res.data.message === "Token đã hết hạn"
                ) {
                  localStorage.clear();
                  window.location.reload();
                }
              })
              .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");
              });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
          else {
            swal(res.data.message)
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    }
    else {
      if (image !== "") {
        file.append("image", image)
      }
      if (banner !== "") {
        file.append("banner", banner)
      }
      if (background !== "") {
        file.append("background", background)
      }
      file.append("_method", "put")


      CallApi(
        `admin/category/update-category/${id}`,
        "POST",
        file,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(!showHide);
            swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

            CallApi(
              `admin/category/list-category`,
              "GET",
              null,
              localStorage.getItem("token")
            )
              .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                  setList(res.data.data);
                } else if (
                  res.data.message === "Không tìm thấy tài khoản." ||
                  res.data.message === "Token đã hết hạn"
                ) {
                  localStorage.clear();
                  window.location.reload();
                }
              })
              .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");
              });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
          else {
            swal(res.data.message)
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    }
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/category/delete-category/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              CallApi(
                `admin/category/list-category`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên danh mục</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    defaultValue={name}
                    placeholder="Tên danh mục..."
                    onChange={(e) => setName(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Ăn tại cửa hàng</Form.Label>
                  <Col md={12} className="row">
                    <Col md={6}><input type="radio" value="1" checked={eatStore == 1 ? true : false} onChange={(e) => setEatStore(e.target.value)} name="eatStore" style={{ marginRight: "10px" }} />Cho phép</Col>
                    <Col md={6}><input type="radio" value="0" checked={eatStore == 0 ? true : false} onChange={(e) => setEatStore(e.target.value)} name="eatStore" style={{ marginRight: "10px" }} />Không</Col>
                  </Col>


                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Hình ảnh</Form.Label>
                  <input
                    ref={fileInput3}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage3(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {hinhanh === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={hinhanh}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Banner</Form.Label>
                  <input
                    ref={fileInput1}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage1(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {banner_image === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage1()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={banner_image}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage1()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>

                <Form.Group>
                  <Form.Label>Ảnh bìa</Form.Label>
                  <input
                    ref={fileInput2}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage2(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {background_image === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage2()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={background_image}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage2()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách danh mục cửa hàng
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Hình ảnh
                </th>
                <th
                  style={{
                    width: "20%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên danh mục
                </th>
                <th
                  style={{
                    width: "20%",
                    textAlign: "center",
                  }}
                >
                  Ảnh bìa
                </th>

                <th
                  style={{
                    width: "35",
                    textAlign: "center",
                  }}
                >
                  Banner
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                    // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>
                        <img
                          src={item.image}
                          alt="linkAvatar"
                          style={{
                            width: "80%",
                            maxWidth: "70%",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                      </td>
                      <td>{item.name}</td>
                      <td>
                        <img
                          src={item.background}
                          alt="linkAvatar"
                          style={{
                            width: "60%",
                            maxWidth: "70%",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                      </td>
                      <td>
                        <img
                          src={item.banner}
                          alt="linkAvatar"
                          style={{
                            width: "85%",
                            maxWidth: "85%",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                      </td>
                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          <Button
                            onClick={() => history.push(`/quan-ly-danh-muc-cua-hang/quan-ly-mon-an-theo-danh-muc/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Tooltip title="Danh sách món ăn gợi ý" placement="top">
                              <ViewListIcon style={{ fontSize: "20px" }} />
                            </Tooltip>
                          </Button>
                          <Button
                            onClick={() => history.push(`/quan-ly-danh-muc-cua-hang/menu-co-dinh/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          > 
                            <Tooltip title="Danh sách menu cố định" placement="top">
                              <LibraryAddIcon style={{ fontSize: "20px" }} />
                            </Tooltip>
                          </Button>
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>

                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>

        </div>
      </div>
    </>
  );
};
export default Event;
