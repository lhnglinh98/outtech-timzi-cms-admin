import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
const Event = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [id, setId] = useState("");
  const [from_weight, setFrom_weight] = useState("");
  const [to_weight, setTo_weight] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [money, setMoney] = useState("");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/product-weight/list-product-weight`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setId(item.id);
    setMoney(item.money);
    setTo_weight(item.to_weight);
    setFrom_weight(item.from_weight);
    setTitleModal("Cập nhật phí ship");
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setId("");
    setMoney("");
    setTo_weight("");
    setFrom_weight("");
    setTitleModal("Thêm phí ship");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    if (id === "") {
      if (Number(from_weight) < Number(to_weight)) {
          
        CallApi(
          `admin/product-weight/create-product-weight`,
          "POST",
          {
            from_weight,
            to_weight,
            money: money?.toString().replace(/,/gi, ""),
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã thêm thành công!", "success");

              CallApi(
                `admin/product-weight/list-product-weight`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Phạm vi từ phải nhỏ hơn phạm vi đến !");

        dispatch(reloading(false));
      }
    } else {
      if (Number(from_weight) < Number(to_weight)) {
        CallApi(
          `admin/product-weight/update-product-weight/${id}`,
          "PUT",
          {
            from_weight,
            to_weight,
            money: money?.toString().replace(/,/gi, ""),
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

              CallApi(
                `admin/product-weight/list-product-weight`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Phạm vi từ phải nhỏ hơn phạm vi đến !");
        dispatch(reloading(false));
      }
    }
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/product-weight/delete-product-weight/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              CallApi(
                `admin/product-weight/list-product-weight`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tiền cộng thêm</Form.Label>
                  <Form.Control
                    type="text"
                    name="money"
                    value={
                      Number(money.toString().replace(/,/gi, ""))
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        ? Number(money.toString().replace(/,/gi, ""))
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        : 0
                    }
                    placeholder="Tiền cộng thêm..."
                    onChange={(e) => setMoney(e.target.value)}
                  />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group>
                  <Form.Label>Khối lượng từ (kg)</Form.Label>
                  <Form.Control
                    type="text"
                    name="from_weight"
                    value={from_weight}
                    placeholder="Khối lượng từ (kg)"
                    onChange={(e) => setFrom_weight(e.target.value)}
                  />
                </Form.Group>
              </Col>
                <Col md={6}>
                  <Form.Group>
                    <Form.Label>Khối lượng đến (kg)</Form.Label>
                    <Form.Control
                      type="text"
                      name="to_weight"
                      value={to_weight}
                      placeholder="Khối lượng đến (kg)..."
                      onChange={(e) => setTo_weight(e.target.value)}
                    />
                  </Form.Group>
                </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách khối lượng hàng hóa
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tiền cộng thêm
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Khối lượng từ (kg)
                </th>
                <th
                  style={{
                    width: "12%",
                    textAlign: "center",
                  }}
                >
                  Khối lượng đến (kg)
                </th>

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>
                        {" "}
                        {Number(item?.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>

                      <td>{item?.from_weight} kg</td>
                      <td>{item?.to_weight} kg</td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    </>
  );
};
export default Event;
