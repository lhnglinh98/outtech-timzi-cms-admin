import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { CPagination } from "@coreui/react";

import swal from "sweetalert";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../../Redux/action/index";


import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
const OderBuyMe = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [page, setPage] = useState(1);
  const [customer_name, setCustomerName] = useState("");
  const [code, setCode] = useState("");
  const [from_date, setFromDate] = useState("");
  const [to_date, setToDate] = useState("");
  const [dataOderBuyMe, setDataOderBuyMe] = useState([]);
  const [coutnOder, setCoutnOder] = useState(0);
  const [coutnMoney, setCoutnMoney] = useState(0);
  const [dataStatus, setDataStatus] = useState(0);
  const listRole = [
    { 'status_id': 1, 'data': 'Chờ xác nhận' },
    { 'status_id': 2, 'data': 'Shipper đã nhận' },
    { 'status_id': 3, 'data': 'Đã hoàn thành' },
    { 'status_id': 4, 'data': 'Tất cả shipper đều bận' }
  ];
  useEffect(() => {
    dispatch(reloading(true));
    getDataOderBuyMe();
  }, [dispatch, currentPage]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Tìm");

    setTitleModal("Tìm kiếm");
  };
  const getDataOderBuyMe = () => {
    dispatch(reloading(true));

    CallApi(
      `oderbuyme/list-oder-buy-me?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data) {
          setCoutnOder(res.data.data.countOder);
          setCoutnMoney(res.data.data.coutMoney);
          setPage(res.data.data.data.last_page);
          setDataOderBuyMe(res.data.data.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

  };
  const UpdateOder = (id_oder) => {
    dispatch(reloading(true));
    let file = {
      status: 0
    }
    console.log('dataStatus: ', file)
    CallApi(
      `oderbuyme/update-oder-buy-me/${id_oder}`,
      "PUT",
      file,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          swal("Đã từ chối đơn hàng");
          getDataOderBuyMe();
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");

      });

  };
  const OnchangeInputStatus = (e) => {
    console.log("e: ", e.target.value)
    setDataStatus(e.target.value);
  };
  const SearchOderBuyMe = () => {
    dispatch(reloading(true));
    CallApi(
      `oderbuyme/list-oder-buy-me?page=${currentPage}&customer_name=${customer_name}&code=${code}&status=${dataStatus}&from_date=${from_date}&to_date=${to_date}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setPage(res.data.data.data.last_page);
          setDataOderBuyMe(res.data.data.data.data);
          CloseModal();
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  };
  // };
  // const DeleteItem = (item) => {
  //   swal({
  //     title: "Bạn chắc chắn muốn hủy&",
  //     icon: "warning",
  //     buttons: true,
  //     dangerMode: true,
  //   }).then((willDelete) => {
  //     if (willDelete) {
  //       dispatch(reloading(true));
  //       CallApi(
  //         `admin/order-food/cancel-order-user-product/${item.id}`,
  //         "PUT",
  //         null,
  //         localStorage.getItem("token")
  //       )
  //         .then((res) => {
  //           console.log(res);
  //           dispatch(reloading(false));
  //           if (res.data.status === 1) {
  //             swal("Thành công!", "Bạn đã hủy thành công!", "success");
  //             setList([]);
  //             CallApi(
  //               `admin/order-food/list-order-user-product?page=${currentPage}`,
  //               "GET",
  //               null,
  //               localStorage.getItem("token")
  //             )
  //               .then((res) => {
  //                 dispatch(reloading(false));
  //                 if (res.data.status === 1) {
  //                   setList(res.data.data.data.data);
  //                   setCount(res.data.data.count);
  //                   setTotalMoneySender(res.data.data.total_money_sender);
  //                   setTotalMoneyShipper(res.data.data.total_money_shipper);
  //                   setPage(res.data.data.data.last_page);

  //                 } else if (
  //                   res.data.message === "Không tìm thấy tài khoản." ||
  //                   res.data.message === "Token đã hết hạn"
  //                 ) {
  //                   localStorage.clear();
  //                   window.location.reload();
  //                 }
  //               })
  //               .catch((error) => {
  //                 dispatch(reloading(true));
  //                 // swal("Vui lòng kiểm tra internet");
  //               });
  //           } else {
  //             swal(res.data.message);
  //           }
  //         })
  //         .catch((error) => {
  //           this.setState({ isLoading: false });
  //         });
  //     }
  //   });
  // };
  return (
    <>
      {/* Phần tạo mới đơn mua hộ */}
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên người gửi</Form.Label>
                  <Form.Control
                    type="text"
                    name="customer_name"
                    placeholder="Tên người gửi"
                    defaultValue={customer_name}
                    onChange={(e) => setCustomerName(e.target.value)}
                  />
                  <Form.Label>Mã đơn hàng</Form.Label>
                  <Form.Control
                    type="text"
                    name="Code"
                    placeholder="Mã đơn hàng"
                    defaultValue={code}
                    onChange={(e) => setCode(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Bộ lọc trạng thái</Form.Label>
                  <Form.Control
                    as="select"
                    name="role_id"
                    value={dataStatus != 0 ? dataStatus : 'Vui lòng chọn'}
                    onChange={(e) => OnchangeInputStatus(e)}
                  >
                    <option value=""> Xử lý trạng thái đơn</option>
                    {listRole.map((item, index) => {
                      return (
                        <option key={index} value={item.status_id}>
                          {item.data}
                        </option>
                      );
                    })}
                  </Form.Control>
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group>
                  <Form.Label>Từ ngày</Form.Label>
                  <Form.Control
                    type="date"
                    name="from_date"
                    placeholder="từ ngày"
                    defaultValue={from_date}
                    onChange={(e) => setFromDate(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group>
                  <Form.Label>Đến ngày</Form.Label>
                  <Form.Control
                    type="date"
                    name="to_date"
                    placeholder="Đến ngày"
                    defaultValue={to_date}
                    onChange={(e) => setToDate(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => SearchOderBuyMe()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      {/* End Tạo mới đơn mua hộ */}

      {/* Danh sách đơn hàng */}
      <div className="card">
        <div className="card-header">
          Danh sách đơn hàng mua hộ
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>

          </div>
        </div>
        <div className="card-header">
          Tổng số đơn mua hộ: {coutnOder}, Tổng tiền sản phẩm người gửi: {Number(coutnMoney)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
        </div>

        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: 100,
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Mã Đơn
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tên KH
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  SĐT KH
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Ghi chú
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Phí khoảng cách
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Phí dịch vụ
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tổng tiền thu
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Ngày tạo
                </th>
                <th
                  style={{
                    width: 200,
                    textAlign: "center",
                  }}
                >
                  Tác vụ
                </th>
              </tr>
            </thead>
            {dataOderBuyMe.length > 0 ? (
              <tbody>
                {dataOderBuyMe.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                    // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{item?.code}</td>
                      <td>{item?.name} </td>
                      <td>{item?.phone} </td>
                      <td style={{
                        width: "25%",
                        textAlign: "center",
                      }}>{item?.all_note}</td>
                      <td> {Number(item?.price_discount)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td> {Number(item?.price_service)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td> {Number(item?.total_money)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td style={{
                        width: 400,
                        textAlign: "center",
                      }}>
                        {item?.created_at?.slice(11, 16)} {item?.created_at?.slice(8, 10)}-{item?.created_at?.slice(5, 7)}-{item?.created_at?.slice(0, 4)}
                      </td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          {
                            item.status != 0
                              ?
                              <Button
                                onClick={() => UpdateOder(item.id)}
                                className="label theme-bg text-white f-12"
                                style={{
                                  width:90,
                                  borderRadius: "15px",
                                  border: "0px",
                                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                                  float: "right",
                                  fontSize: "12px",
                                  backgroundImage:
                                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                                  margin: "5px",
                                  padding: "3px 5px",
                                  cursor: "pointer",
                                }}
                              >
                                Huỷ nhận đơn
                              </Button>
                              :
                              <Button
                                className="label theme-bg text-white f-12"
                                style={{
                                  width:90,
                                  borderRadius: "15px",
                                  border: "0px",
                                  boxShadow: "red",
                                  float: "right",
                                  fontSize: "12px",
                                  background:'red',
                                  margin: "5px",
                                  padding: "3px 5px",
                                  cursor: "pointer",
                                }}
                              >
                                Đã huỷ đơn
                              </Button>
                          }
                          {/* <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button> */}
                          <Button
                            onClick={() => history.push(`/chi-tiet-don-hang-mua-ho/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Visibility style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );

}
export default OderBuyMe;
