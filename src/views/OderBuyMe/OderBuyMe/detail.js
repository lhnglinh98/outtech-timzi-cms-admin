import React, { useState, useEffect } from "react";
import { CCard, CCardBody, CCol, CRow, CCardHeader } from "@coreui/react";
import { Button, Modal, Row, Col, Table, Form } from "react-bootstrap";
import { useDispatch } from "react-redux";
import CallApi from "../../../Middleware/api";
import { reloading } from "../../../Redux/action/index";
import Check from "@material-ui/icons/Check";
import Delete from "@material-ui/icons/Delete";
import swal from "sweetalert";
import QRCode from "qrcode.react";
import "./index.css";
import PDFFile from "../../../lib/components/PdfComponents/PDFFile";
import { PDFDownloadLink } from "@react-pdf/renderer"
import { List } from "@material-ui/core";
const DetailOderBuyMe = ({ match }) => {
    const dispatch = useDispatch();
    const [list, setList] = useState("");
    const [submitModal, setSubmitModal] = useState("");
    const [showHide, setShowHide] = useState(false);
    const [titleModal, setTitleModal] = useState("Sửa thông tin");
    const [money_surcharge, setMoney_surcharge] = useState(0);
    const [note_surcharge, setNote_surcharge] = useState(" ");
    const [dataStatus, setDataStatus] = useState(0);
    const listRole = [
        { 'status_id': 1, 'data': 'Chờ xác nhận' },
        { 'status_id': 2, 'data': 'Shipper đã nhận' },
        { 'status_id': 3, 'data': 'Đã hoàn thành' },
        { 'status_id': 4, 'data': 'Tất cả shipper đều bận' }
    ];
    useEffect(() => {
        dispatch(reloading(true));
        GetDetailOder();
    }, [dispatch, match.params.id]);
    const GetDetailOder = () => {
        CallApi(
            `oderbuyme/detail-oder-buy-me/${match.params.id}`,
            "GET",
            null,
            localStorage.getItem("token")
        )
            .then((res) => {
                dispatch(reloading(false));
                if (res.data) {
                    setList(res.data.data);
                    setMoney_surcharge(res.data.data.money_surcharge);
                    setNote_surcharge(res.data.data.note_surcharge);
                } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                ) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    //   changeLocation(`/#/users`);
                }
            })
            .catch((error) => {
                // swal("Vui lòng kiểm tra internet");
            });
    }
    const CloseModal = () => {
        setShowHide(!showHide);
    };
    const toggleModal3 = () => {
        setShowHide(!showHide);
        setSubmitModal("Duyệt");
        setTitleModal("Thay đổi");
    };
    const UpdateOder = () => {
        dispatch(reloading(true));
        let file = dataStatus != '' && dataStatus != 0
            ?
            {
                money_surcharge: Number(money_surcharge),
                note_surcharge: note_surcharge,
                status: Number(dataStatus)
            }
            :
            {
                money_surcharge: Number(money_surcharge),
                note_surcharge: note_surcharge
            }
        console.log('dataStatus: ', file)
        CallApi(
            `oderbuyme/update-oder-buy-me/${match.params.id}`,
            "PUT",
            file,
            localStorage.getItem("token")
        )
            .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                    setShowHide(!showHide);
                    GetDetailOder();
                    swal("Cập nhật thành công");
                } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                ) {
                    localStorage.clear();
                    window.location.reload();
                }
            })
            .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");

            });

    };
    const OnchangeInputStatus = (e) => {
        console.log("e: ", e.target.value)
        setDataStatus(e.target.value);
    };
    return (

        <CRow>
            {showHide ? (
                <Modal show={showHide} onHide={() => CloseModal()}>
                    <Modal.Header closeButton onClick={() => CloseModal()}>
                        <Modal.Title>{titleModal}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col md={12}>
                                <Form.Group>
                                    <Form.Label>Tiền phụ thu</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="money_surcharge"
                                        placeholder="Tiền phụ thu"
                                        defaultValue={list?.money_surcharge}
                                        onChange={(e) => setMoney_surcharge(e.target.value)}
                                    />
                                    <Form.Label>Ghi chú - giải thích</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="shipper_name"
                                        placeholder="Ghi chú - giải thích"
                                        defaultValue={list?.note_surcharge}
                                        onChange={(e) => setNote_surcharge(e.target.value)}
                                    />
                                    <Form.Group>
                                        <Form.Label>Xử lý đơn</Form.Label>
                                        <Form.Control
                                            as="select"
                                            name="role_id"
                                            value={dataStatus != 0 ? dataStatus : 'Vui lòng chọn'}
                                            onChange={(e) => OnchangeInputStatus(e)}
                                        >
                                            <option value=""> Xử lý trạng thái đơn</option>
                                            {listRole.map((item, index) => {
                                                return (
                                                    <option key={index} value={item.status_id}>
                                                        {item.data}
                                                    </option>
                                                );
                                            })}
                                        </Form.Control>
                                    </Form.Group>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Modal.Body>

                    <Modal.Footer style={{ flexWrap: "nowrap" }}>
                        <Button
                            onClick={() => UpdateOder()}
                            className="unread label theme-bg2 text-white f-12 float-right"
                            style={{
                                borderRadius: "15px",
                                border: "0px",
                                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                                float: "right",
                                fontWeight: "bold",
                                fontSize: "14px",
                                padding: "5px 10px",
                                cursor: "pointer",
                            }}
                        >
                            {submitModal}
                        </Button>
                    </Modal.Footer>
                </Modal>
            ) : null}
            <CCol lg={12}>
                <CCard>
                    <CCardHeader>Thông tin tin đơn hàng- mã đơn hàng mua hộ: {list.code} </CCardHeader>
                    <CCardBody>
                        <table className="table table-striped table-hover">
                            <tbody>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Tên người gửi</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Số điện thoại người gửi</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.phone}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Địa chỉ người gửi</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list?.address}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Lựa chọn khoảng cách</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.distance?.title}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Phí khoảng cách</strong></td>
                                    <td style={{ width: '80%' }}>
                                        <strong>{Number(list.price_discount)
                                            .toString()
                                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                                        </strong>

                                    </td>

                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Lựa chọn dịch vụ</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.categoryService?.title}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Phí dịch vụ</strong></td>
                                    <td style={{ width: '80%' }}>
                                        <strong>{Number(list.price_service)
                                            .toString()
                                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Tiền phụ thu</strong></td>
                                    <td style={{ width: '80%' }}>
                                        <strong>{Number(list.money_surcharge)
                                            .toString()
                                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Giải thích,note chi phí phụ thu</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.note_surcharge}
                                    </td>

                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Ghi chú đi chợ hộ</strong></td>
                                    <td style={{ width: '80%' }}>
                                        <Form.Control
                                            as="textarea"
                                            rows="4"
                                            name="description"
                                            value={list?.all_note}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Thời gian tạo</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.created_at?.slice(11, 16)} {list.created_at?.slice(8, 10)}-{list.created_at?.slice(5, 7)}-{list.created_at?.slice(0, 4)}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Thời gian cập nhật cuối cùng</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.updated_at?.slice(11, 16)} {list.updated_at?.slice(8, 10)}-{list.updated_at?.slice(5, 7)}-{list.updated_at?.slice(0, 4)}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Lựa chọn thanh toán của khách</strong></td>
                                    <td style={{ width: '80%' }}>   
                                        {list.option_pay === 1 ? "Thanh toán hộ" : null}
                                        {list.option_pay === 2 ? "Gửi tiền" : null}
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Tổng tiền</strong></td>
                                    <td style={{ width: '80%' }}>
                                        <strong>
                                            {Number(list.total_money)
                                                .toString()
                                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ width: '20%' }}><strong>Trạng thái đơn hàng</strong></td>
                                    <td style={{ width: '80%' }}>
                                        {list.status === 0 ? "Đã huỷ đơn" : null}      
                                        {list.status === 1 ? "Chờ xử lý" : null}
                                        {list.status === 2 ? "Shipper đã nhận" : null}
                                        {list.status === 3 ? "Tất cả shipper đã bận" : null}
                                        {list.status === 4 ? "Đã hoàn thành" : null}
                                    </td>
                                </tr>
                                <tr>
                                    {/* <td>
                                        <PDFDownloadLink document={<PDFFile  />} fileName="Tải xuống">
                                            {({ loading }) =>
                                                loading ? (
                                                    <Button
                                                        className="label theme-bg text-white f-12"
                                                        style={{
                                                            borderRadius: "15px",
                                                            border: "0px",
                                                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                                                            fontSize: "12px",
                                                            backgroundImage:
                                                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                                                            padding: "3px 10px",
                                                            cursor: "pointer",
                                                        }}
                                                    >
                                                        Loading document...
                                                    </Button>
                                                )
                                                    :
                                                    <Button
                                                        className="label theme-bg text-white f-12"
                                                        style={{
                                                            borderRadius: "15px",
                                                            border: "0px",
                                                            boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                                                            fontSize: "12px",
                                                            backgroundImage:
                                                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                                                            padding: "3px 10px",
                                                            cursor: "pointer",
                                                        }}
                                                    >
                                                        Tải xuống
                                                    </Button>
                                            }
                                        </PDFDownloadLink>
                                    </td> */}
                                    <td>
                                        <Button
                                            onClick={() => toggleModal3()}
                                            className="label theme-bg text-white f-12"
                                            style={{
                                                borderRadius: "15px",
                                                border: "0px",
                                                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                                                fontSize: "12px",
                                                backgroundImage:
                                                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                                                padding: "3px 10px",
                                                cursor: "pointer",
                                            }}
                                        >
                                            Thay đổi
                                        </Button>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>
    );
};

export default DetailOderBuyMe;
