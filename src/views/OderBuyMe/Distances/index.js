import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { CPagination } from "@coreui/react";

import swal from "sweetalert";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../../Redux/action/index";


import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
import { DataUsageRounded } from "@material-ui/icons";
const Distances = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [showHide1, setShowHide1] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [page, setPage] = useState(1);
  const [title, setTitle] = useState("");
  const [priceDiscount, setPriceDiscount] = useState(0);
  const [dataDistances, setDataDistances] = useState([]);
  const [dataStatus, setDataStatus] = useState(0);
  const [idDiscount, setIdPriceDiscount] = useState(0);

  useEffect(() => {
    dispatch(reloading(true));
    getDataOderBuyMe();
  }, [dispatch, currentPage]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const CloseModal1 = () => {
    setShowHide1(!showHide1);
  };
  const toggleModal3 = () => {
    setShowHide1(!showHide1);
  };
  const getDataOderBuyMe = () => {
    dispatch(reloading(true));

    CallApi(
      `distances/list-distances?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data) {
          setPage(res.data.data.last_page);
          setDataDistances(res.data.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

  };
  const Push = (item) => {
    setShowHide(!showHide);
    setIdPriceDiscount(item.id);
    setDataStatus(item.status);
    setTitle(item.title);
    setPriceDiscount(item.price_discount)
  }
  const UpdateDistances = (id_distances, statusNow) => {
    dispatch(reloading(true));
    let file = {
      status: statusNow
    }
    console.log('dataStatus: ', file)
    CallApi(
      `distances/update-distances/${id_distances}`,
      "PUT",
      file,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          swal("Đã thay đổi");
          getDataOderBuyMe();
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");

      });

  };
  const Update = () => {
    dispatch(reloading(true));
    let file = {
      price_discount: Number(priceDiscount),
      title: title,
      status: Number(dataStatus)
    }
    console.log('dataStatus: ', file)
    CallApi(
      `distances/update-distances/${idDiscount}`,
      "PUT",
      file,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          swal("Đã thay đổi");
          getDataOderBuyMe();
          setShowHide(!showHide);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");

      });

  };
  const Create = () => {
    dispatch(reloading(true));
    let file = {
      price_discount: Number(priceDiscount),
      title: title,
    }
    console.log('dataStatus: ', file)
    CallApi(
      `distances/create-distances`,
      "POST",
      file,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          swal("Thêm thành công");
          getDataOderBuyMe();
          setShowHide1(!showHide1);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");

      });

  };
  const OnchangeInputStatus = (e) => {
    console.log("e: ", e.target.value)
    setDataStatus(e.target.value);
  };

  return (
    <>
      {/* Phần tạo mới đơn mua hộ */}
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Note khoảng cách</Form.Label>
                  <Form.Control
                    type="text"
                    name="title"
                    placeholder="Note khoảng cách"
                    defaultValue={title}
                    onChange={(e) => setTitle(e.target.value)}
                  />
                  <Form.Label>Giá trị khoảng cách</Form.Label>
                  <Form.Control
                    type="text"
                    name="price_discount"
                    placeholder="Giá trị khoảng cách"
                    defaultValue={priceDiscount}
                    onChange={(e) => setPriceDiscount(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => Update()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              Lưu
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      {/* End Tạo mới đơn mua hộ */}
      {showHide1 ? (
        <Modal show={showHide1} onHide={() => CloseModal1()}>
          <Modal.Header closeButton onClick={() => CloseModal1()}>
            <Modal.Title>Tạo mới</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Note khoảng cách</Form.Label>
                  <Form.Control
                    type="text"
                    name="title"
                    placeholder="Note khoảng cách"
                    defaultValue={""}
                    onChange={(e) => setTitle(e.target.value)}
                  />
                  <Form.Label>Giá trị khoảng cách</Form.Label>
                  <Form.Control
                    type="text"
                    name="price_discount"
                    placeholder="Giá trị khoảng cách"
                    defaultValue={0}
                    onChange={(e) => setPriceDiscount(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => Create()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              Lưu
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      {/* Danh sách đơn hàng */}
      <div className="card">
        <div className="card-header">
          Danh sách cấu hình chi phí khoảng cách
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                Tạo mới
              </Button>
            </small>
          </div>
        </div>

        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: 100,
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tiêu đề note
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Phí khoảng cách
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Ngày tạo
                </th>
                <th
                  style={{
                    width: 200,
                    textAlign: "center",
                  }}
                >
                  Tác vụ
                </th>
              </tr>
            </thead>
            {dataDistances.length > 0 ? (
              <tbody>
                {dataDistances.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                    // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{item?.title}</td>
                      <td> {Number(item?.price_discount)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td style={{
                        width: 400,
                        textAlign: "center",
                      }}>
                        {item?.created_at?.slice(11, 16)} {item?.created_at?.slice(8, 10)}-{item?.created_at?.slice(5, 7)}-{item?.created_at?.slice(0, 4)}
                      </td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          {
                            item.status != 0
                              ?
                              <Button
                                onClick={() => UpdateDistances(item.id, 0)}
                                className="label theme-bg text-white f-12"
                                style={{
                                  width: 90,
                                  borderRadius: "15px",
                                  border: "0px",
                                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                                  float: "right",
                                  fontSize: "12px",
                                  backgroundImage:
                                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                                  margin: "5px",
                                  padding: "3px 5px",
                                  cursor: "pointer",
                                }}
                              >
                                Hiện
                              </Button>
                              :
                              <Button
                                onClick={() => UpdateDistances(item.id, 1)}
                                className="label theme-bg text-white f-12"
                                style={{
                                  width: 90,
                                  borderRadius: "15px",
                                  border: "0px",
                                  boxShadow: "red",
                                  float: "right",
                                  fontSize: "12px",
                                  background: 'red',
                                  margin: "5px",
                                  padding: "3px 5px",
                                  cursor: "pointer",
                                }}
                              >
                                Ẩn
                              </Button>
                          }
                          <Button
                            onClick={() => Push(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              width: 90,
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "red",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            Sửa
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );

}
export default Distances;
