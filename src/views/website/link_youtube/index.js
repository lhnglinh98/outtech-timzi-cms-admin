import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Row, Col, Button, Form } from "react-bootstrap";
import CallApi from "../../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../../Redux/action/index";
const Event = () => {
  const dispatch = useDispatch();

  const [link_app_customer, setLinkAppCustomer] = useState("");
  const [id, setId] = useState("");
  const [link_app_shop, setLinkAppShop] = useState("");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/website/link-youtube`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setLinkAppShop(res.data.data.link_app_shop);
          setLinkAppCustomer(res.data.data.link_app_customer);
          setId(res.data.data.id);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const UpdateAccount = () => {
    dispatch(reloading(true));

    CallApi(
      `admin/website/update-link-youtube/${id}`,
      "PUT",
      { link_app_customer, link_app_shop },
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          dispatch(reloading(true));
          swal("Thành công!", "Bạn đã cập thành công!", "success")
          CallApi(
            `admin/website/link-youtube`,
            "GET",
            null,
            localStorage.getItem("token")
          )
            .then((res) => {
              dispatch(reloading(false));
              if (res.data.status === 1) {
                setLinkAppShop(res.data.data.link_app_shop);
                setLinkAppCustomer(res.data.data.link_app_customer);
                setId(res.data.data.id);
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              }
            })
            .catch((error) => {
              dispatch(reloading(true));
              // swal("Vui lòng kiểm tra internet");
            });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          swal(res.data.message);
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  };

  return (
    <>
      <div className="card">
        <div className="card-header">Quản lý link youtube</div>
        <div className="card-body">
          <Row>
            <Col md={1}></Col>
            <Col md={10}>
              <Form.Group>
                <Form.Label>Link app khách hàng</Form.Label>
                <Form.Control
                  type="text"
                  name="link_app_customer"
                  defaultValue={link_app_customer}
                  placeholder="Link app khách hàng..."
                  onChange={(e) => setLinkAppCustomer(e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Link app cửa hàng</Form.Label>
                <Form.Control
                  type="text"
                  name="link_app_shop"
                  defaultValue={link_app_shop}
                  placeholder="Link app cửa hàng..."
                  onChange={(e) => setLinkAppShop(e.target.value)}
                />
              </Form.Group>
            </Col>
            <Col md={1}></Col>
            <Col md={12}>
              <div
                style={{
                  textAlign: "center",
                  marginBottom: "50px",
                  marginTop: "50px",
                }}
              >
                <Button
                  variant="primary"
                  className="ButtonSave"
                  style={{
                    background: "rgb(0 71 128)",
                    borderColor: "rgb(0 71 128)",
                  }}
                  onClick={() => UpdateAccount()}
                >
                  Cập nhật
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
export default Event;
