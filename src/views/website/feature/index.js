import React, { useState, useEffect, createRef } from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Group3 from "../../../assets/Group 14847 1.png";
import Delete from "@material-ui/icons/Delete";
import Resizer from "react-image-file-resizer";
const Event = () => {
  const fileInput3 = createRef();
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [title, setTitle] = useState("");
  const [id, setId] = useState("");
  const [image, setImage] = useState("");
  const [content, setContent] = useState("");
  const [hinhanh, setHinhanh] = useState("");
  const [showHide, setShowHide] = useState(false);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/website/list-feature-website`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const OnchangeImage3 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);
    setHinhanh(file);
    if (Number(e.target.files[0].size / 1024) > 2000) {
      var fileInput = false;
      if (e.target.files[0]) {
        fileInput = true;
      }
      if (fileInput) {
        try {
          Resizer.imageFileResizer(
            e.target.files[0],
            500,
            500,
            "JPEG",
            100,
            0,
            (uri) => {
              fetch(uri)
                .then((res) => res.blob())
                .then((blob) => {
                  const file1 = new File([blob], "File name", {
                    type: "image/png",
                  });
                  setImage(file1);
                });
            },
            "base64",
            200,
            200
          );
        } catch (err) {
          console.log(err);
        }
      }
    } else {
      setImage(e.target.files[0]);
    }

   
  };
  const UploadImage3 = () => fileInput3.current.click();

  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setTitle(item.title);
    setContent(item.content);
    setId(item.id);
    setImage("");
    setHinhanh(item.image);
    setTitleModal("Cập nhật tính năng");
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setTitle("");

    setId("");
    setContent("");
    setImage("");
    setHinhanh("");
    setTitleModal("Thêm tính năng");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    let file = new FormData();
    file.append("title", title);
    file.append("content", content);
    if (id === "") {
      file.append("image", image);

      CallApi(
        `admin/website/create-feature-website`,
        "POST",
        file,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(!showHide);
            swal("Thành công!", "Bạn đã thêm thành công!", "success");

            CallApi(
              `admin/website/list-feature-website`,
              "GET",
              null,
              localStorage.getItem("token")
            )
              .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                  setList(res.data.data);
                } else if (
                  res.data.message === "Không tìm thấy tài khoản." ||
                  res.data.message === "Token đã hết hạn"
                ) {
                  localStorage.clear();
                  window.location.reload();
                }
              })
              .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");
              });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    } else {
      if (image !== "") {
        file.append("image", image);
      }

      file.append("_method", "put");

      CallApi(
        `admin/website/update-feature-website/${id}`,
        "POST",
        file,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShowHide(!showHide);
            swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

            CallApi(
              `admin/website/list-feature-website`,
              "GET",
              null,
              localStorage.getItem("token")
            )
              .then((res) => {
                dispatch(reloading(false));
                if (res.data.status === 1) {
                  setList(res.data.data);
                } else if (
                  res.data.message === "Không tìm thấy tài khoản." ||
                  res.data.message === "Token đã hết hạn"
                ) {
                  localStorage.clear();
                  window.location.reload();
                }
              })
              .catch((error) => {
                dispatch(reloading(true));
                // swal("Vui lòng kiểm tra internet");
              });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
    }
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/website/delete-feature-website/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              setList([]);
              CallApi(
                `admin/website/list-feature-website`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tiêu đề</Form.Label>
                  <Form.Control
                    type="text"
                    name="title"
                    defaultValue={title}
                    placeholder="Tiêu đề..."
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Nội dung</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows="6"
                    name="content"
                    value={content}
                    placeholder="Nội dung..."
                    onChange={(e) => setContent(e.target.value)}
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group>
                  <Form.Label>Hình ảnh</Form.Label>
                  <input
                    ref={fileInput3}
                    type="file"
                    name="image"
                    accept="image/*"
                    capture
                    multiple
                    onChange={(e) => OnchangeImage3(e)}
                    style={{ display: "none" }}
                  />
                  <div
                    style={{
                      background: "white",
                      width: "100%",
                      margin: "10px 0px",
                      textAlign: "center",
                    }}
                  >
                    {hinhanh === "" ? (
                      <img
                        src={Group3}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                          height: "auto",
                          maxHeight: "180px",
                        }}
                      />
                    ) : (
                      <img
                        src={hinhanh}
                        alt="imageas"
                        id="upfile3"
                        onClick={() => UploadImage3()}
                        style={{
                          cursor: "pointer",
                          maxWidth: "100%",
                        }}
                      />
                    )}
                  </div>
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách tính năng website
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "6%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  STT
                </th>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Hình ảnh
                </th>
                <th
                  style={{
                    width: "20%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên tính năng
                </th>
                <th
                  style={{
                    width: "50%",
                    textAlign: "center",
                  }}
                >
                  Nội dung
                </th>

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{index + 1}</td>
                      <td>
                        <img
                          src={item.image}
                          alt="linkAvatar"
                          style={{
                            width: "80%",
                            maxWidth: "70%",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                      </td>
                      <td>{item.title}</td>
                      <td>{item.content}</td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    </>
  );
};
export default Event;
