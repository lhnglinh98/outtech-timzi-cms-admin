import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Row, Col, Button, Form } from "react-bootstrap";
import CallApi from "../../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../../Redux/action/index";
const Event = () => {
  const dispatch = useDispatch();

  const [content_app_customer, setContentAppCustomer] = useState("");
  const [content_app_shop, setContentAppShop] = useState("");
  const [content_app_ship, setContentAppShip] = useState("");
  const [id, setId] = useState("");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/website/content-toward`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setContentAppCustomer(res.data.data.content_app_customer);
          setContentAppShop(res.data.data.content_app_shop);
          setContentAppShip(res.data.data.content_app_ship);
          setId(res.data.data.id);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const UpdateAccount = () => {
    dispatch(reloading(true));

    CallApi(
      `admin/website/update-content-toward/${id}`,
      "PUT",
      { content_app_customer, content_app_shop, content_app_ship },
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          dispatch(reloading(true));
          swal("Thành công!", "Bạn đã cập thành công!", "success")
          CallApi(
            `admin/website/content-toward`,
            "GET",
            null,
            localStorage.getItem("token")
          )
            .then((res) => {
              dispatch(reloading(false));
              if (res.data.status === 1) {
                setContentAppCustomer(res.data.data.content_app_customer);
                setContentAppShop(res.data.data.content_app_shop);
                setContentAppShip(res.data.data.content_app_ship);
                setId(res.data.data.id);
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              }
            })
            .catch((error) => {
              dispatch(reloading(true));
              // swal("Vui lòng kiểm tra internet");
            });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          swal(res.data.message);
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  };

  return (
    <>
      <div className="card">
        <div className="card-header">Quản lý nội dung hướng tới</div>
        <div className="card-body">
          <Row>
            <Col md={1}></Col>
            <Col md={10}>
              <Form.Group>
                <Form.Label>Người dùng</Form.Label>
                <Form.Control
                  as="textarea"
                  rows="6"
                  value={content_app_customer}
                  name="content_app_customer"
                  placeholder="Người dùng..."
                  onChange={(e) => setContentAppCustomer(e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Cửa hàng</Form.Label>
                <Form.Control
                  as="textarea"
                  rows="6"
                  value={content_app_shop}
                  name="content_app_shop"
                  placeholder="Cửa hàng..."
                  onChange={(e) => setContentAppShop(e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Người giao hàng</Form.Label>
                <Form.Control
                  as="textarea"
                  rows="6"
                  value={content_app_ship}
                  name="content_app_ship"
                  placeholder="Người giao hàng..."
                  onChange={(e) => setContentAppShip(e.target.value)}
                />
              </Form.Group>
            </Col>
            <Col md={1}></Col>
            <Col md={12}>
              <div
                style={{
                  textAlign: "center",
                  marginBottom: "50px",
                  marginTop: "50px",
                }}
              >
                <Button
                  variant="primary"
                  className="ButtonSave"
                  style={{
                    background: "rgb(0 71 128)",
                    borderColor: "rgb(0 71 128)",
                  }}
                  onClick={() => UpdateAccount()}
                >
                  Cập nhật
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
export default Event;
