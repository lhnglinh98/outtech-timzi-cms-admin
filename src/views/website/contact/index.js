import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import CallApi from "../../../Middleware/api";
import { Table } from "react-bootstrap";
import { CPagination } from "@coreui/react";
import reloading from "../../../Redux/action/index";
const Colors = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [page, setPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  // const counter = useSelector((state) => state.loading);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/website/list-contact-website?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));

        if (res.data.status === 1) {
          setList(res.data.data.data);
          setPage(res.data.data.last_page);
        } else if (
          res.data.message === "Token không hợp lệ" ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, currentPage]);
  return (
    <div className="card">
      <div className="card-header">Danh sách đăng ký liên hệ</div>
      <div className="card-body">
        <Table bordered hover responsive id="TableRespon1">
          <thead>
            <tr>
              <th
                style={{
                  width: "6%",
                  wordBreak: "break-word",
                  textAlign: "center",
                }}
              >
                STT
              </th>
              <th
                style={{
                  width: "11%",
                  wordBreak: "break-word",
                  textAlign: "center",
                }}
              >
                Họ tên
              </th>
              <th
                style={{
                  width: "20%",
                  textAlign: "center",
                }}
              >
                Email
              </th>

              <th
                style={{
                  width: "15%",
                  textAlign: "center",
                }}
              >
                Tin nhắn
              </th>

              <th
                style={{
                  width: "13%",
                  textAlign: "center",
                }}
              >
                Thời gian tạo
              </th>
            </tr>
          </thead>
          {list.length > 0 ? (
            <tbody>
              {list.map((item, index) => {
                return (
                  <tr
                    key={index}
                    style={{
                      textAlign: "center",
                      cursor: "pointer",
                    }}
                  >
                    <td>{index + 1}</td>
                    <td>
                      <p>{item.name}</p>
                    </td>
                    <td>{item.email}</td>
                    <td>{item.message}</td>
                    <td>
                      {item?.created_at?.slice(8, 10)}-
                      {item?.created_at?.slice(5, 7)}-
                      {item?.created_at?.slice(0, 4)}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          ) : (
            <tbody>
              <tr>
                <td
                  colSpan="7"
                  style={{
                    textAlign: "center",
                    padding: "10px",
                  }}
                >
                  Không có dữ liệu
                </td>
              </tr>
            </tbody>
          )}
        </Table>
        <CPagination
          align="center"
          addListClass="some-class"
          activePage={currentPage}
          pages={page}
          onActivePageChange={setCurrentPage}
        />
        <br></br>
      </div>
    </div>
  );
};

export default Colors;
