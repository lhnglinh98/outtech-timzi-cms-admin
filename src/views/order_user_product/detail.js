import React, { useState, useEffect } from "react";
import { CCard, CCardBody, CCol, CRow, CCardHeader } from "@coreui/react";
import { Button, Modal, Row, Col, Table } from "react-bootstrap";
import { useDispatch } from "react-redux";
import CallApi from "../../Middleware/api";
import { reloading } from "../../Redux/action/index";
import Check from "@material-ui/icons/Check";
import Delete from "@material-ui/icons/Delete";
import swal from "sweetalert";
import QRCode from "qrcode.react";
import "./index.css";
const DetailEvent = ({ match }) => {
  const dispatch = useDispatch();
  const [titleModal, setTitleModal] = useState("");
  const [listCombo, setListCombo] = useState([]);
  const [list, setList] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [combo, setCombo] = useState(false);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/order-food/order-user-product-detail/${match.params.id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
        console.log(res.data.data)

          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          //   changeLocation(`/#/users`);
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });

    CallApi(
      `store/store-detail-with-book-table/${match.params.id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          //   changeLocation(`/#/users`);
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, match.params.id]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const changeLocation = (link) => {
    window.location = link;
  };
  const toggleModal3 = (item) => {
    setShowHide(!showHide);
    setListCombo(item?.food);
    setCombo(true);
    setTitleModal("Danh sách món ăn trong combo");
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setListCombo(item?.food);
    setCombo(false);
    setTitleModal("Danh sách món ăn");
  };
  const CheckOk = () => {
    dispatch(reloading(true));
    CallApi(
      `admin/store/confirm-store/${list.store.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          swal("Thành công!", "Bạn đã duyệt thành công!", "success");
          changeLocation(`/#/Quan-ly-cua-hang`);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  };
  const DeleteItem = () => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/store/cancel-store/${list.store.id}`,
          "PUT",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              changeLocation(`/#/Quan-ly-cua-hang`);
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {});
      }
    });
  };
  return (
    <CRow>
      <CCol lg={12}>
        <CCard>
          <CCardHeader>Thông tin tin đơn hàng- mã đơn hàng giao hộ: {list.code} </CCardHeader>
          <CCardBody>
            <table className="table table-striped table-hover">
              <tbody>
                <tr>
                  <td>Tên người gửi</td>
                  <td>
                   {list.sender_name}
                  </td>
                </tr>
                <tr>
                  <td>Số điện thoại người gửi</td>
                  <td>
                    <strong>{list.sender_phone}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Địa chỉ người gửi</td>
                  <td>
                    <strong>{list.sender_address}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Tên người nhận</td>
                  <td>
                   {list.receiver_name}
                  </td>
                </tr>
                <tr>
                  <td>Số điện thoại người nhận</td>
                  <td>
                    <strong>{list.receiver_phone}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Địa chỉ người nhận</td>
                  <td>
                    <strong>{list.receiver_address}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Thời gian đặt hàng</td>
                  <td>
                    {list.created_at?.slice(11, 16)} {list.created_at?.slice(8, 10)}-{list.created_at?.slice(5, 7)}-{list.created_at?.slice(0, 4)}
                    </td>
                </tr>
                <tr>
                  <td>Thời gian cập nhật cuối cùng</td>
                  <td>
                    {list.updated_at?.slice(11, 16)} {list.updated_at?.slice(8, 10)}-{list.updated_at?.slice(5, 7)}-{list.updated_at?.slice(0, 4)}
                    </td>
                </tr>
                <tr>
                  <td>Phí vận chuyển</td>
                  <td>
                    <strong>
                    {Number(list.fee_ship)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>
                <tr>
                  <td>Phí khối lượng hàng hóa</td>
                  <td>
                    <strong>
                    {Number(list.money_with_product_weight)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>
                <tr>
                  <td>Khối lượng</td>
                  <td>
                    {list.from_weight} kg - {list.to_weight} kg
                  </td>
                </tr>
                <tr>
                  <td>Thu hộ</td>
                  <td>
                    <strong>
                      {Number(list.money)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>
                <tr>
                  <td>Tổng giá trị đơn hàng</td>
                  <td>
                  <strong>
                      {Number(list.total_money)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>
                <tr>
                  <td>Trạng thái đơn hàng</td>
                  <td>
                        {list.status === 0 ? "Chờ shipper xác nhận" : null}
                        {list.status === 1 ? "Shipper đã nhận đơn" : null}
                        {list.status === 2 ? "Shipper đã đến vị trí người gửi" : null}
                        {list.status === 3 ? "Shipper đã lấy hàng" : null}
                        {list.status === 4 ? "Shipper đã đến vị trí người nhận" : null}
                        {list.status === 5 ? "Đơn giao thành công" : null}
                        {list.status === 6 ? "Người gửi hủy" : null}
                        {list.status === 7 ? "Shipper hủy" : null}
                        {list.status === 8 ? "Người nhận hủy" : null}
                        {list.status === 9 ? "Không có shipper nhận đơn" : null}
                        {list.status === 10 && list.staff_timzi === null ? "Tìm Zì hủy" : null}
                        {list.status === 10 && list.staff_timzi != null ? list.staff_timzi.name + " hủy" : null}
                  </td>
                </tr>
                <tr>
                  <td>Thông tin shipper</td>
                  <td>
                    {list.shipper?.name } - {list.shipper?.phone }
                    
                  </td>
                </tr>
                <tr>
                  <td>Tên sản phẩm</td>
                  <td>
                    {list.product_name}
                  </td>
                </tr>
                <tr>
                  <td>Ảnh sản phẩm</td>
                  <td>
                    {list.image_product?.map((img) => {
                          return (
                            <img
                              src={img}
                              alt="linkAvatar"
                              style={{
                                width: "60px",
                                maxWidth: "70%",
                                marginRight: "15px",
                                textAlign: "center",
                                // borderRadius: "100%",
                              }}
                            />
                          );
                    })}
                  </td>
                </tr>
                <tr>
                  <td>Ghi chú</td>
                  <td>
                    {list.note}
                  </td>
                </tr>
                <tr>
                  <td>Ảnh shipper xác nhận nhận hàng</td>
                  <td>
                    {list.image_send_product?.map((img) => {
                          return (
                            <img
                              src={img}
                              alt="linkAvatar"
                              style={{
                                width: "60px",
                                maxWidth: "70%",
                                marginRight: "15px",
                                textAlign: "center",
                                // borderRadius: "100%",
                              }}
                            />
                          );
                    })}
                  </td>
                </tr>
                <tr>
                  <td>Ảnh shipper giao hàng thành công</td>
                  <td>
                    {list.image_receive_product?.map((img) => {
                          return (
                            <img
                              src={img}
                              alt="linkAvatar"
                              style={{
                                width: "60px",
                                maxWidth: "70%",
                                marginRight: "15px",
                                textAlign: "center",
                                // borderRadius: "100%",
                              }}
                            />
                          );
                    })}
                  </td>
                </tr>
                <tr></tr>
              </tbody>
            </table>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default DetailEvent;
