import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { CPagination } from "@coreui/react";

import swal from "sweetalert";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";


import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
const Event = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [id, setId] = useState("");
  const [from_range, setFrom_range] = useState("");
  const [to_range, setTo_range] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [money, setMoney] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [page, setPage] = useState(1);
  const [customer_name, setCustomerName] = useState("");
  const [shop_name, setShopName] = useState("");
  const [shipper_name, setShipName] = useState("");
  const [from_date, setFromDate] = useState("");
  const [to_date, setToDate] = useState("");
  const [count, setCount] = useState("");
  const [total_money_sender, setTotalMoneySender] = useState("");
  const [total_money_shipper, setTotalMoneyShipper] = useState("");
  // const [customer_name, setCustomerName] = useState("");

 
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/order-food/list-order-user-product?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));

        // console.log(res.data.data.data)
        if (res.data.status === 1) {
          setList(res.data.data.data.data);
          setCount(res.data.data.count);
          setTotalMoneySender(res.data.data.total_money_sender);
          setTotalMoneyShipper(res.data.data.total_money_shipper);
          setPage(res.data.data.data.last_page);
          console.log(res.data.data.data.last_page);

        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, currentPage]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Tìm");

    setTitleModal("Tìm kiếm");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
          
    CallApi(
      `admin/order-food/list-order-user-product?page=${currentPage}&customer_name=${customer_name}&shop_name=${shop_name}&shipper_name=${shipper_name}&from_date=${from_date}&to_date=${to_date}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data.data.data);
          setCount(res.data.data.count);
          setTotalMoneySender(res.data.data.total_money_sender);
          setTotalMoneyShipper(res.data.data.total_money_shipper);
          setPage(res.data.data.data.last_page);
          console.log(res.data.data.data.data);
          console.log(1);

        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
    
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn hủy&",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/order-food/cancel-order-user-product/${item.id}`,
          "PUT",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            console.log(res);
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã hủy thành công!", "success");
              setList([]);
              CallApi(
                `admin/order-food/list-order-user-product?page=${currentPage}`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data.data.data);
                    setCount(res.data.data.count);
                    setTotalMoneySender(res.data.data.total_money_sender);
                    setTotalMoneyShipper(res.data.data.total_money_shipper);
                    setPage(res.data.data.data.last_page);

                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên người gửi</Form.Label>
                  <Form.Control
                    type="text"
                    name="customer_name"
                    placeholder="Tên khác hàng"
                    defaultValue={customer_name}
                    onChange={(e) => setCustomerName(e.target.value)}
                  />
                  <Form.Label>Tên shipper</Form.Label>
                  <Form.Control
                    type="text"
                    name="shipper_name"
                    placeholder="Tên shipper"
                    defaultValue={shipper_name}
                    onChange={(e) => setShipName(e.target.value)}
                  />
                </Form.Group>
              </Col>

              <Col md={6}>
                <Form.Group>
                  <Form.Label>Từ ngày</Form.Label>
                  <Form.Control
                    type="date"
                    name="from_date"
                    placeholder="từ ngày"
                    defaultValue={from_date}
                    onChange={(e) => setFromDate(e.target.value)}
                  />
                </Form.Group>
              </Col>
                <Col md={6}>
                  <Form.Group>
                    <Form.Label>Đến ngày</Form.Label>
                    <Form.Control
                      type="date"
                      name="to_date"
                      placeholder="Đến ngày"
                      defaultValue={to_date}
                      onChange={(e) => setToDate(e.target.value)}
                    />
                  </Form.Group>
                </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách đơn hàng
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>
           
          </div>
        </div>
        <div className="card-header">
          Tổng số đơn: {count}, Tổng tiền sản phẩm người gửi: {Number(total_money_sender)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}, Tổng tiền shipper nhận: {Number(total_money_shipper)
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
        </div>

        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên người gửi
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tên người nhận
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tên shipper
                </th>

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                 Tổng tiền
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Phí ship
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                 Phí khối lượng
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Trạng thái
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                 Ngày đặt
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                 Tác vụ
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{item?.sender_name}</td>
                      <td>{item?.receiver_name} </td>
                      <td>{ item?.shipper != null ? item?.shipper.name :"Đơn chưa nhận"} </td>
                      <td> {Number(item?.total_money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td> {Number(item?.fee_ship)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td> {Number(item?.money_with_product_weight)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>
                        {item.status === 0 ? "Chờ shipper xác nhận" : null}
                        {item.status === 1 ? "Shipper đã nhận đơn" : null}
                        {item.status === 2 ? "Shipper đã đến vị trí người gửi" : null}
                        {item.status === 3 ? "Shipper đã lấy hàng" : null}
                        {item.status === 4 ? "Shipper đã đến vị trí người nhận" : null}
                        {item.status === 5 ? "Đơn giao thành công" : null}
                        {item.status === 6 ? "Người gửi hủy" : null}
                        {item.status === 7 ? "Shipper hủy" : null}
                        {item.status === 8 ? "Người nhận hủy" : null}
                        {item.status === 9 ? "Không có shipper nhận đơn" : null}
                        {item.status === 10 && item.staff_timzi === null ? "Tìm Zì hủy" : null}
                        {item.status === 10 && item.staff_timzi != null ? item.staff_timzi.name + " hủy" : null}
                      </td>
                      <td>
                        {item?.created_at?.slice(11, 16)} {item?.created_at?.slice(8, 10)}-{item?.created_at?.slice(5, 7)}-{item?.created_at?.slice(0, 4)}
                      </td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          {
                            item.status !=5 && item.status !=6 && item.status !=7 &&item.status !=8 &&item.status != 9 &&item.status != 10
                            ?
                            <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            Hủy
                          </Button>
                          
                          :null
                          }
                          {/* <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button> */}
                          <Button
                            onClick={() => history.push(`/chi-tiet-don-hang-giao-ho/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Visibility style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );
};
export default Event;
