import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Row, Col, Button, Form } from "react-bootstrap";
import CallApi from "../../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../../Redux/action/index";
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState, convertFromHTML } from 'draft-js';
import draftToHtml from "draftjs-to-html";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
const Event = () => {
  const dispatch = useDispatch();
  // var editorState = EditorState.createEmpty();
  const [title, setTitle] = useState("");
  const [id, setId] = useState("");
  const [editorState, setEditorState] = useState(EditorState.createEmpty())
  const onEditorStateChange = (editorState) => {
    setEditorState(editorState)
  }
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/policy-app/dispute-resolution-policy`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setEditorState(EditorState.createWithContent(
            ContentState.createFromBlockArray(
              convertFromHTML(res.data.data.content)
            )
          ))
          setId(res.data.data.id);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch]);
  const UpdateAccount = () => {
    dispatch(reloading(true));
    const currentContent = editorState.getCurrentContent();
    const contentRaw = convertToRaw(currentContent);
    const value = currentContent.hasText() ? draftToHtml(contentRaw) : "";
    let file = new FormData();
    file.append(
      "content",
      value
    );
    file.append("_method", "put")
    CallApi(
      `admin/policy-app/update-dispute-resolution-policy/${id}`,
      "POST",
      file,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          dispatch(reloading(true));
          swal("Thành công!", "Bạn đã cập thành công!", "success")
          CallApi(
            `admin/policy-app/dispute-resolution-policy`,
            "GET",
            null,
            localStorage.getItem("token")
          )
            .then((res) => {
              dispatch(reloading(false));
              if (res.data.status === 1) {
                setEditorState(EditorState.createWithContent(
                  ContentState.createFromBlockArray(
                    convertFromHTML(res.data.data.content)
                  )
                ))
                setId(res.data.data.id);
              } else if (
                res.data.message === "Không tìm thấy tài khoản." ||
                res.data.message === "Token đã hết hạn"
              ) {
                localStorage.clear();
                window.location.reload();
              }
            })
            .catch((error) => {
              dispatch(reloading(true));
              // swal("Vui lòng kiểm tra internet");
            });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          swal(res.data.message);
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  };

  return (
    <>
      <div className="card">
        <div className="card-header">Hướng dẫn thanh toán</div>
        <div className="card-body">
          <Row>
            <Col md={1}></Col>
            <Col md={10}>
              <Form.Group>
                <Form.Label>Nội dung</Form.Label>
                  <Editor
                        editorState={editorState}
                        toolbarClassName="toolbarClassName"
                        wrapperClassName="wrapperClassName"
                        editorClassName="editorClassName"
                        onEditorStateChange={onEditorStateChange}
                        style={{ minHeight: "200px", border: "3px solid" }}
                      />
              </Form.Group>
            </Col>
            <Col md={1}></Col>
            <Col md={12}>
              <div
                style={{
                  textAlign: "center",
                  marginBottom: "50px",
                  marginTop: "50px",
                }}
              >
                <Button
                  variant="primary"
                  className="ButtonSave"
                  style={{
                    background: "rgb(0 71 128)",
                    borderColor: "rgb(0 71 128)",
                  }}
                  onClick={() => UpdateAccount()}
                >
                  Cập nhật
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};
export default Event;
