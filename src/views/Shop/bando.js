// import React, { Component } from "react";
// import { set_lat, set_lng } from "../../Redux/action/index";// import { connect } from "react-redux";
// import { Map, Marker, GoogleApiWrapper } from "google-maps-react";

// export class MapContainer extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       center: { lat: this.props.lat, lng: this.props.lng },
//       maps: null,
//       map: null,
//       markers: [],
//       zoom: 17,
//       showingInfoWindow: false,
//       activeMarker: {},
//       selectedPlace: {},
//     };
//   }
//   onMapClicked = (mapProps, map, clickEvent) => {
//     let toadonew = {
//       lat: clickEvent.latLng.lat(),
//       lng: clickEvent.latLng.lng(),
//     };
//     this.props.Islat(clickEvent.latLng.lat());
//     this.props.Islng(clickEvent.latLng.lng());
//     this.setState({ center: toadonew });
//   };
//   componentDidUpdate(prevProps, prevState) {
//     if (this.props.lng !== this.state.center.lng) {
//      let centerUpdate = {
//       lat: this.state.center.lat, lng: this.props.lng 
//      }
//      this.setState({center:centerUpdate})

//     }
//     if (this.props.lat !== this.state.center.lat) {
//       let centerUpdate = {
//        lat: this.props.lat, lng: this.state.center.lng 
//       }
//       this.setState({center:centerUpdate})
 
//      }
//   }
 
   
//   render() {
   
//     return (
//       <Map
//         google={this.props.google}
//         style={{ width: "95%", height: "100%", position: "relative" }}
//         className={"map"}
//         zoom={this.state.zoom}
//         draggable={true}
//         containerElement={<div style={{height:`500px`,width:`50%`,display:'flex',flexDirection:'column-reverse' }} />}
//         initialCenter={this.state.center}
//         onClick={this.onMapClicked}
//       >
          
//         <Marker
//           // title={'The marker`s title will appear as a tooltip.'}
//           // name={'SOMA'}
//           position={this.state.center}
//         />
//       </Map>
//     );
//   }
// }
// const mapStateToProps = (state) => {
//   return {
//     lng: state.lng,
//     lat: state.lat,
//   };
// };
// const mapDispatchToProps = (dispatch) => {
//   return {
//     Islat: (lat) => dispatch(set_lat(lat)),
//     Islng: (lng) => dispatch(set_lng(lng)),
//   };
// };
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(
//   GoogleApiWrapper({
//     apiKey: "AIzaSyAgxaD412H4b24diNaw4rmum-qQBnfDxhU",
//   })(MapContainer)
// );
import { set_lat, set_lng } from "../../Redux/action/index";
import { connect } from "react-redux";
const _ = require("lodash");

const { compose, withProps, lifecycle } = require("recompose");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} = require("react-google-maps");
const { SearchBox } = require("react-google-maps/lib/components/places/SearchBox");
const MapWithASearchBox = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAgxaD412H4b24diNaw4rmum-qQBnfDxhU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  lifecycle({
    componentWillMount() {
      const refs = {}
      this.setState({
        bounds: null,
        center: {
          lat: 21.00644388014599, lng:105.79748248407326
        },
        markers: [],
        onMapMounted: ref => {
          refs.map = ref;
        },
        onBoundsChanged: () => {
          this.setState({
            bounds: refs.map.getBounds(),
            center: refs.map.getCenter(),
          })
        },
        onSearchBoxMounted: ref => {
          refs.searchBox = ref;
         
        },
      
        onPlacesChanged: () => {
          const places = refs.searchBox.getPlaces();
          // eslint-disable-next-line no-undef
          const bounds = new google.maps.LatLngBounds();

          places.forEach(place => {
            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport)
            } else {
              bounds.extend(place.geometry.location)
            }
          });
          const nextMarkers = places.map(place => ({
            position: place.geometry.location,
          }));
         
          const nextCenter = _.get(nextMarkers, '0.position', this.state.center);
           this.props.Islat(nextCenter.lat());
            this.props.Islng(nextCenter.lng());
          this.setState({
            center: nextCenter,
            markers: nextMarkers,
          });
          // refs.map.fitBounds(bounds);
        },
      })
    },
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={15}
    center={props.center}
    onBoundsChanged={props.onBoundsChanged}
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      
      // eslint-disable-next-line no-undef
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
    >
      <input
        type="text"
        placeholder="Tìm địa chỉ..."
        style={{
          boxSizing: `border-box`,
          border: `1px solid transparent`,
          width: `50%`,
          height: `32px`,
          marginTop: `12px`,
          padding: `18px 12px`,
          borderRadius: `3px`,
          boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
          fontSize: `14px`,
          outline: `none`,
          textOverflow: `ellipses`,
        }}
      />
    </SearchBox>
    {props.markers.map((marker, index) =>
      <Marker key={index} position={marker.position} />
    )}
  </GoogleMap>
);
<MapWithASearchBox />
const mapStateToProps = (state) => {
  return {
    lng: state.lng,
    lat: state.lat,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    Islat: (lat) => dispatch(set_lat(lat)),
    Islng: (lng) => dispatch(set_lng(lng)),
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(MapWithASearchBox)