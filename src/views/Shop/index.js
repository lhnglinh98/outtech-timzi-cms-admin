import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { CPagination } from "@coreui/react";
// import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Visibility from "@material-ui/icons/Visibility";
import Search from "@material-ui/icons/Search";
import ToggerOn from "@material-ui/icons/ToggleOn";
import ToggerOff from "@material-ui/icons/ToggleOff";
import swal from "sweetalert";
import "./index.css"
const Event = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [page, setPage] = useState(1);
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [name, setName] = useState("");
  const [user_id, setUser_id] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [listCategory, setListCategory] = useState([]);
  const [province_id, setProvince_id] = useState([]);
  const [user, setUser] = useState([]);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/store/list-store?page=${currentPage}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data.data);
          setPage(res.data.data.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

    CallApi(`list-province`, "GET", null, localStorage.getItem("token"))
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setListCategory(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
    CallApi(
      `admin/account/list-chain-owner`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));

        if (res.data.status === 1) {
          setUser(res.data.data)
        } else if (
          res.data.message === "Token không hợp lệ" ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, currentPage]);

  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Tìm");

    setTitleModal("Tìm kiếm");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    CallApi(
      `admin/store/list-store?name=${name}&user_id=${user_id}&province_id=${province_id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setShowHide(!showHide)
          setList(res.data.data.data);
          setPage(res.data.data.last_page);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

  };
  const lockAccount = (item) => {
    dispatch(reloading(true));
    CallApi(
      `admin/store/lock-store/${item.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    ).then((res) => {
      dispatch(reloading(false));
      swal(res.data.message);
      CallApi(
        `admin/store/list-store?name=${name}&user_id=${user_id}&province_id=${province_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));

          if (res.data.status === 1) {
            setList(res.data.data.data);
            setPage(res.data.data.last_page);
          } else if (
            res.data.message === "Token không hợp lệ" ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          // swal("Vui lòng kiểm tra internet");
        });
    });
  };
  const UnlockAccount = (item) => {
    dispatch(reloading(true));
    CallApi(
      `admin/store/unlock-store/${item.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    ).then((res) => {
      dispatch(reloading(false));
      swal(res.data.message);
      CallApi(
        `admin/store/list-store?name=${name}&user_id=${user_id}&province_id=${province_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));

          if (res.data.status === 1) {
            setList(res.data.data.data);
            setPage(res.data.data.last_page);
          } else if (
            res.data.message === "Token không hợp lệ" ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          // swal("Vui lòng kiểm tra internet");
        });
    });
  };
  // const DeleteItem = (item) => {
  //   swal({
  //     title: "Bạn chắc chắn muốn xóa?",
  //     icon: "warning",
  //     buttons: true,
  //     dangerMode: true,
  //   }).then((willDelete) => {
  //     if (willDelete) {
  //       dispatch(reloading(true));
  //       CallApi(
  //         `main/event/${item._id}/deleteEvent`,
  //         "DELETE",
  //         null,
  //         localStorage.getItem("token")
  //       )
  //         .then((res) => {
  //           dispatch(reloading(false));
  //           if (res.data.status === 1) {
  //             swal("Thành công!", "Bạn đã xóa thành công!", "success");
  //             setList([]);
  //             CallApi(
  //               `admin/store/list-store?page=${currentPage}`,
  //               "GET",
  //               null,
  //               localStorage.getItem("token")
  //             )
  //               .then((res) => {
  //                 dispatch(reloading(false));
  //                 if (res.data.status === 1) {
  //                   setList(res.data.data.data);
  //                   setPage(res.data.data.last_page);
  //                 } else if (
  //                   res.data.message === "Không tìm thấy tài khoản." ||
  //                   res.data.message === "Token đã hết hạn"
  //                 ) {
  //                   localStorage.clear();
  //                   window.location.reload();
  //                 }
  //               })
  //               .catch((error) => {
  //                 // swal("Vui lòng kiểm tra internet");
  //               });
  //           } else {
  //             swal(res.data.message);
  //           }
  //         })
  //         .catch((error) => {});
  //     }
  //   });
  // };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên cửa hàng</Form.Label>
                  <Form.Control
                    type="text"
                    name="name"
                    defaultValue={name}
                    placeholder="Tên cửa hàng..."
                    onChange={(e) => setName(e.target.value)}
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Chủ chuỗi</Form.Label>
                  <Form.Control
                    as="select"
                    value={user_id}
                    onChange={(e) => setUser_id(e.target.value)}
                  >
                    <option value="">Chọn chủ chuỗi</option>
                    {user.map((item, index) => {
                      return <option value={item.id}>{item.name}</option>;
                    })}
                  </Form.Control>

                </Form.Group>

                <Form.Group>
                  <Form.Label>Tỉnh thành</Form.Label>
                  <Form.Control
                    as="select"
                    value={province_id}
                    onChange={(e) => setProvince_id(e.target.value)}
                  >
                    <option value="">Chọn tỉnh thành</option>
                    {listCategory.map((item, index) => {
                      return <option value={item.id} key={index}>{item.name}</option>;
                    })}
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách cửa hàng
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Search style={{ fontSize: "20px" }} />
              </Button>
            </small>
            <small
              className="text-muted"
              onClick={() =>
                history.push({
                  pathname: `Quan-ly-cua-hang/Them-cua-hang`,
                })
              }
              style={{ marginLeft: "7px" }}
            >
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Hình ảnh
                </th>
                <th
                  style={{
                    width: "16%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên cửa hàng
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Giờ mở
                </th>

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Giờ đóng
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  hotline
                </th>
                <th
                  style={{
                    width: "14%",
                    textAlign: "center",
                  }}
                >
                  Điểm đáng giá
                </th>
                <th
                  style={{
                    width: "13%",
                    textAlign: "center",
                  }}
                >
                  Giá trung bình
                </th>
                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Địa chỉ
                </th>
                <th style={{
                  width: "13%",
                  textAlign: "center"
                }}>
                  Trạng thái
                </th>
                <th
                  style={{
                    width: "13%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                    // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>
                        <img
                          src={item.image}
                          alt="linkAvatar"
                          style={{
                            width: "60px",
                            maxWidth: "70%",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                      </td>
                      <td>{item.name}</td>
                      <td>{item.open_hours}</td>
                      <td>{item.close_hours}</td>
                      <td>{item.hotline}</td>
                      <td>{item.star}</td>
                      <td>
                        {Number(item.average_price)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>

                      <td>{item.address}</td>
                      <td>
                        {item.status === 2 ? (
                          <ToggerOn
                            onClick={() => UnlockAccount(item)}
                            style={{
                              border: "0px",
                              color: "gray",
                              fontWeight: "bold",
                              fontSize: "40px",
                            }}
                          />
                        ) : null}
                        {item.status === 1 ? (
                          <ToggerOff
                            onClick={() => lockAccount(item)}
                            style={{
                              border: "0px",
                              color: "#1cbc49",
                              fontWeight: "bold",
                              fontSize: "40px",
                            }}
                          />
                        ) : null}
                        {item.status === 0 ? "Chưa kích hoạt" : null}
                      </td>
                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => history.push(`/Quan-ly-cua-hang/cap-nhat/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          {/* <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button> */}
                          <Button
                            onClick={() => history.push(`/Quan-ly-cua-hang/chi-tiet-cua-hang/${item.id}`)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Visibility style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
          <br></br>
          <CPagination
            align="center"
            addListClass="some-class"
            activePage={currentPage}
            pages={page}
            onActivePageChange={setCurrentPage}
          />
        </div>
      </div>
    </>
  );
};
export default Event;
