import React from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Row, Col, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import jQuery from "jquery";
import Group3 from "../../assets/Group 14847 1.png";
import { reloading, set_lat, set_lng } from "../../Redux/action/index";
import MultiSelect from "react-multi-select-component";
import Remove from "@material-ui/icons/RemoveCircle";
import { connect } from "react-redux";
import BanDo from "./bando"
class Shopadd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      longtidue: this.props.lng,
      latitude: this.props.lat,
      image: "",
      hinhanh: "",
      percent_discount_revenue:"",
      address: "",
      listCategory: [],
      selected: [],
      average_price: "",
      open_hours: "",
      close_hours: "",
      hotline: "",
      active: false,
      category: [],
      bussiness: [""],
      province_id: "",
      district_id: "",
      ward_id: "",
      user_id: "",
      proviceList: [],
      districList: [],
      wardList: [],
      User: [],
    };
  }

  componentDidMount() {
    this.props.IsLoading(true);
    CallApi(
      `admin/category/list-category`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        this.props.IsLoading(false);
        if (res.data.status === 1) {
          let option = [];
          for (let i = 0; i < res.data.data.length; i++) {
            let obj = {
              label: res.data.data[i].name,
              value: res.data.data[i].id,
            };
            option.push(obj);
          }

          this.setState({ listCategory: option });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        this.props.IsLoading(true);
        // swal("Vui lòng kiểm tra internet");
      });
    CallApi(
      `admin/account/list-chain-owner`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        this.props.IsLoading(false);

        if (res.data.status === 1) {
          this.setState({ User: res.data.data });
        } else if (
          res.data.message === "Token không hợp lệ" ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
    this.props.IsLoading(true);
    CallApi(`list-province`, "GET", null, localStorage.getItem("token"))
      .then((res) => {
        this.props.IsLoading(false);
        if (res.data.status === 1) {
          this.setState({ proviceList: res.data.data });
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        this.props.IsLoading(true);
        // swal("Vui lòng kiểm tra internet");
      });
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.province_id !== this.state.province_id) {
      this.props.IsLoading(true);
      CallApi(
        `list-district?province_id=${this.state.province_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          this.props.IsLoading(false);
          if (res.data.status === 1) {
            this.setState({ districList: res.data.data });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          this.props.IsLoading(true);
          // swal("Vui lòng kiểm tra internet");
        });
      this.props.IsLoading(true);
      CallApi(
        `list-ward?province_id=${this.state.province_id}&district_id=${this.state.district_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          this.props.IsLoading(false);
          if (res.data.status === 1) {
            this.setState({ wardList: res.data.data });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          this.props.IsLoading(true);
          // swal("Vui lòng kiểm tra internet");
        });
    }
    if (prevState.district_id !== this.state.district_id) {
      this.props.IsLoading(true);
      CallApi(
        `list-ward?province_id=${this.state.province_id}&district_id=${this.state.district_id}`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          this.props.IsLoading(false);
          if (res.data.status === 1) {
            this.setState({ wardList: res.data.data });
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          this.props.IsLoading(true);
          // swal("Vui lòng kiểm tra internet");
        });
    }
    if(this.props.lng!==this.state.longtidue && this.state.longtidue!==""){
      document.getElementById("longtidue").value=this.props.lng
      this.setState({longtidue: this.props.lng})
    }
    if(this.props.lat!==this.state.latitude && this.state.latitude!==""){
    
      document.getElementById("latitude").value=this.props.lat
      this.setState({latitude: this.props.lat})
    }
  }
  UploadImage3 = () => this.fileInput.click();
  OnchangeImage3 = (e) => {
    var file = window.URL.createObjectURL(e.target.files[0]);
    this.setState({
      image: e.target.files[0],
      hinhanh: file,
    });
  };
  SubmitValue = () => {
    if (!this.state.image || this.state.image === "") {
      swal("Vui lòng thêm hình ảnh");
    }
    else if(!Number(this.state.percent_discount_revenue) || Number(this.state.percent_discount_revenue)>100 ||Number(this.state.percent_discount_revenue)<0){
        swal("Chiết khấu phải là số và nhỏ hơn 100")
    }
     else {
      this.props.IsLoading(true);
      let file = new FormData();
      file.append("name", this.state.name);
      file.append("address", this.state.address);
      file.append("latitude", this.state.latitude);
      file.append("longtidue", this.state.longtidue);
      file.append("image", this.state.image);
      file.append("average_price", this.state.average_price.replace(/,/gi, ""));
      file.append("open_hours", this.state.open_hours);
      file.append("percent_discount_revenue", this.state.percent_discount_revenue);
      file.append("close_hours", this.state.close_hours);
      file.append("hotline", this.state.hotline);
      file.append("user_id", this.state.user_id);
      file.append("province_id", this.state.province_id);
      file.append("district_id", this.state.district_id);
      file.append("ward_id", this.state.ward_id);
      let select = this.state.selected;
      jQuery.each(select, (key, item) => {
        file.append("category_id[" + key + "]", item.value);
      });
      let bussini = this.state.bussiness;
      jQuery.each(bussini, (key, item) => {
        file.append("category_business[" + key + "]", item);
      });
      CallApi(
        `admin/store/create-store`,
        "POST",
        file,
        localStorage.getItem("token")
      )
        .then((res) => {
          this.props.IsLoading(false);
          if (res.data.status === 1) {
            swal("Thành công!", "Bạn đã thêm thành công!", "success").then(
              (success) => {
                window.location.reload();
              }
            );
          } else {
            swal(res.data.message);
          }
        })
        .catch((error) => {
          swal("Có gì đó không ổn!!!");
        });
    }
  };
  onchangeBusiness = (e, index) => {
    for (let i = 0; i < this.state.bussiness.length; i++) {
      if (Number(i) === Number(index)) {
        this.state.bussiness.splice(index, 1, e.target.value);
        this.setState({
          bussiness: this.state.bussiness,
        });
      }
    }
  };
  OnclickAddItem = () => {
    this.state.bussiness.push("");
    this.setState({ bussiness: this.state.bussiness });
  };

  OnchangeInputText(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  }
  OnchangeSlect = (e) => {
    this.setState({ selected: e });
  };
  RemoteItem = (index) => {
    this.state.bussiness.splice(index, 1);

    this.setState({
      bussiness: this.state.bussiness,
    });
  };
  
  OnchangeInputTextlatitude = (e)=>{
    if(Number(e.target.value)){
      this.props.Islat(e.target.value)
      this.setState({latitude:e.target.value})
    }
    else{
      swal("Vĩ độ phải là số")
    }
  }
  OnchangeInputTextlongtidue =(e)=>{
    if(Number(e.target.value)){
        this.props.Islng(e.target.value)
        this.setState({longtidue:e.target.value})
    }
    else{
      swal("Kinh độ phải là số")
    }
  }
  render() {
    return (
      <div className="card">
        <div className="card-header">Thêm cửa hàng</div>
        <div className="card-body">
          <Row>
            <Col md={6}>
              <Form.Group>
                <Form.Label>Tên cửa hàng</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  defaultValue={this.state.name}
                  placeholder="Tên cửa hàng..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group>
             <Form.Group>
                <Form.Label>Chọn danh mục</Form.Label>
                <MultiSelect
                  options={this.state.listCategory}
                  value={this.state.selected}
                  onChange={(e) => this.OnchangeSlect(e)}
                  labelledBy="Select"
                />
              </Form.Group> <Form.Group>
                <Form.Label>Chủ chuỗi</Form.Label>
                <Form.Control
                  as="select"
                  name="user_id"
                  value={this.state.user_id}
                  onChange={(e) => this.OnchangeInputText(e)}
                >
                  <option value="">Chọn Chủ chuỗi</option>
                  {this.state.User.map((item, index) => {
                    return (
                      <option value={item.id} key={index}>
                        {item.name}
                      </option>
                    );
                  })}
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Giờ mở cửa</Form.Label>
                <Form.Control
                  type="time"
                  name="open_hours"
                  defaultValue={this.state.open_hours}
                  placeholder="Giờ mở cửa..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group><Form.Group>
                <Form.Label>Giờ đóng cửa</Form.Label>
                <Form.Control
                  type="time"
                  name="close_hours"
                  defaultValue={this.state.close_hours}
                  placeholder="Giờ đóng cửa..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Vĩ độ</Form.Label>
                <Form.Control
                  type="text"
                  id="latitude"
                  defaultValue={this.state.latitude}
                  placeholder="Vĩ độ..."
                  onChange={(e) => this.OnchangeInputTextlatitude(e)}
                /> 
              </Form.Group> <Form.Group>
                <Form.Label>Kinh độ</Form.Label>
                <Form.Control
                  type="text"
                  id="longtidue"
                  defaultValue={this.state.longtidue}
                  placeholder="Kinh độ..."
                  onChange={(e) => this.OnchangeInputTextlongtidue(e)}
                />
              </Form.Group>
            </Col>
            <Col md={6}>
               <Form.Group>
                <Form.Label>Giá trung bình</Form.Label>
                <Form.Control
                  type="text"
                  name="average_price"
                  value={Number(
                    this.state.average_price.toString().replace(/,/gi, "")
                  )
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  placeholder="Giá trung bình..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Chiết khấu (%)</Form.Label>
                <Form.Control
                  type="text"
                  name="percent_discount_revenue"
                  defaultValue={this.state.percent_discount_revenue}
                  placeholder="Chiết khấu (%)..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Hotline</Form.Label>
                <Form.Control
                  type="text"
                  name="hotline"
                  defaultValue={this.state.hotline}
                  placeholder="Hotline..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group>{" "}<Form.Group>
                <Form.Label>Địa chỉ</Form.Label>
                <Form.Control
                  type="text"
                  name="address"
                  defaultValue={this.state.address}
                  placeholder="Địa chỉ..."
                  onChange={(e) => this.OnchangeInputText(e)}
                />
              </Form.Group>
               <Form.Group>
                <Form.Label>Tỉnh thành</Form.Label>
                <Form.Control
                  as="select"
                  name="province_id"
                  value={this.state.province_id}
                  onChange={(e) => this.OnchangeInputText(e)}
                >
                  <option value="">Chọn tỉnh thành</option>
                  {this.state.proviceList.map((item, index) => {
                    return (
                      <option value={item.id} key={index}>
                        {item.name}
                      </option>
                    );
                  })}
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Quận huyện</Form.Label>
                <Form.Control
                  as="select"
                  name="district_id"
                  value={this.state.district_id}
                  onChange={(e) => this.OnchangeInputText(e)}
                >
                  <option value="">Chọn quận huyện</option>
                  {this.state.districList.map((item, index) => {
                    return (
                      <option value={item.id} key={index}>
                        {item.name}
                      </option>
                    );
                  })}
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Xã</Form.Label>
                <Form.Control
                  as="select"
                  name="ward_id"
                  value={this.state.ward_id}
                  onChange={(e) => this.OnchangeInputText(e)}
                >
                  <option value="">Chọn xã</option>
                  {this.state.wardList.map((item, index) => {
                    return (
                      <option value={item.id} key={index}>
                        {item.name}
                      </option>
                    );
                  })}
                </Form.Control>
              </Form.Group>
            
            </Col>
            <Col md={12}><Form.Group>
                <Form.Label>Danh mục kinh doanh</Form.Label>
                {this.state.bussiness?.map((item, index) => {
                  return (
                    <span key={index} style={{ display: "flex" }}>
                      <Form.Control
                        type="text"
                        style={{ margin: "5px 0px" }}
                        value={item}
                        placeholder="Danh mục kinh doanh..."
                        onChange={(e) => this.onchangeBusiness(e, index)}
                      />
                      <span style={{ maxWidth: "25px" }}>
                        <Add
                          onClick={() => this.OnclickAddItem()}
                          style={{
                            fontSize: "20px",
                            marginLeft: "5px",
                            color: "rgb(0 71 128)",

                            cursor: "pointer",
                          }}
                        />

                        {index > 0 ? (
                          <Remove
                            onClick={() => this.RemoteItem(index)}
                            style={{
                              marginLeft: "5px",
                              fontSize: "20px",
                              color: "rgb(0 71 128)",
                              cursor: "pointer",
                            }}
                          />
                        ) : null}
                      </span>
                    </span>
                  );
                })}
              </Form.Group></Col>
            <Col md={4}>
               <Form.Group>
                <Form.Label>Hình ảnh</Form.Label>
                <input
                  ref={(fileInput) => (this.fileInput = fileInput)}
                  type="file"
                  name="image"
                  accept="image/*"
                  capture
                  multiple
                  onChange={(e) => this.OnchangeImage3(e)}
                  style={{ display: "none" }}
                />
                <div
                  style={{
                    background: "white",
                    width: "100%",
                    margin: "10px 0px",
                    textAlign: "left",
                  }}
                >
                  {this.state.hinhanh === "" ? (
                    <img
                      src={Group3}
                      alt="imageas"
                      id="upfile3"
                      onClick={() => this.UploadImage3()}
                      style={{
                        cursor: "pointer",
                        maxWidth: "100%",
                        height: "auto",
                        maxHeight: "180px",
                      }}
                    />
                  ) : (
                    <img
                      src={this.state.hinhanh}
                      alt="imageas"
                      id="upfile3"
                      onClick={() => this.UploadImage3()}
                      style={{
                        cursor: "pointer",
                        maxWidth: "100%",
                      }}
                    />
                  )}
                </div>
              </Form.Group>
            </Col>
            <Col md={8}>
            <Form.Label>Bản đồ</Form.Label>
              <BanDo/>
            </Col>
          </Row>
        </div>
        <div
          style={{
            textAlign: "center",
            marginBottom: "50px",
            marginTop: "50px",
          }}
        >
          <Button
            variant="primary"
            className="ButtonSave"
            style={{
              background: "rgb(0 71 128)",
              borderColor: "rgb(0 71 128)",
            }}
            onClick={() => this.SubmitValue()}
          >
            Thêm cửa hàng
          </Button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    lng: state.lng,
    lat: state.lat,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    IsLoading: (loading) => dispatch(reloading(loading)),
    Islat: (lat) => dispatch(set_lat(lat)),
    Islng: (lng) => dispatch(set_lng(lng)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Shopadd);
