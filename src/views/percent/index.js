import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
const Event = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [shipper, setShipper] = useState([]);
  const [withdrawMoney, setWithdrawMoney] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [id, setId] = useState("");
  const [percent, setPercent] = useState("");
  const [money, setMoney] = useState("");
  const [type, setType] = useState(1);
  const [showHide, setShowHide] = useState(false);
  const [showHide2, setShowHide2] = useState(false);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/percent/percent-coin-detail`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });

      CallApi(
        `admin/percent/percent-fee-shipper-detail`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setShipper(res.data.data);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
      CallApi(
        `admin/setup-withdraw-money/detail`,
        "GET",
        null,
        localStorage.getItem("token")
      )
        .then((res) => {
          dispatch(reloading(false));
          if (res.data.status === 1) {
            setWithdrawMoney(res.data.data);
          } else if (
            res.data.message === "Không tìm thấy tài khoản." ||
            res.data.message === "Token đã hết hạn"
          ) {
            localStorage.clear();
            window.location.reload();
          }
        })
        .catch((error) => {
          dispatch(reloading(true));
          // swal("Vui lòng kiểm tra internet");
        });
  }, [dispatch]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setId(item.id);
    setPercent(item.percent)
    setType(1);
    setTitleModal("Cập nhật phần trăm chiết khấu xu");
  };
  const toggleModal3 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setId(item.id);
    setPercent(item.percent)
    setType(2);
    setTitleModal("Cập nhật phần trăm chiết khấu shipper");
  };
  const toggleModal4 = (item) => {
    setShowHide2(!showHide2);
    setSubmitModal("Cập nhật");
    setId(item.id);
    setMoney(item.money);
    setType(3);
    setTitleModal("Cập nhật số tiền tối thiểu shipper có thể rút.");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    if (type == 1) {
      if (Number(percent) || Number(percent) < 100) {
        CallApi(
          `admin/percent-coin/update-percent-coin/${id}`,
          "PUT",
          {
            percent,
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

              CallApi(
                `admin/percent/percent-coin-detail`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Phần trăm chiết khấu phải là số nhỏ hơn 100");

        dispatch(reloading(false));
      }
    } else if (type == 2) {
      if (Number(percent) || Number(percent) < 100) {
        CallApi(
          `admin/percent/update-percent-fee-shipper/${id}`,
          "PUT",
          {
            percent,
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

              CallApi(
                `admin/percent/percent-fee-shipper-detail`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setShipper(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Phần trăm chiết khấu phải là số nhỏ hơn 100");
        dispatch(reloading(false));
      }
    } else {
      if (Number(money) && Number(money) > 0) {
        CallApi(
          `admin/setup-withdraw-money/update/${id}`,
          "PUT",
          {
            money,
          },
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide2(!showHide2);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

              CallApi(
                `admin/setup-withdraw-money/detail`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setWithdrawMoney(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Số tiền phải lớn hơn 0.");
        dispatch(reloading(false));
      }
    }
  };
  
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Chiết khấu</Form.Label>
                  <Form.Control
                    type="text"
                    name="percent"
                    value={percent}
                    placeholder="Chiết khấu..."
                    onChange={(e) => setPercent(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      {showHide2 ? (
        <Modal show={showHide2} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Số tiền tối thiểu</Form.Label>
                  <Form.Control
                    type="text"
                    name="money"
                    value={money}
                    placeholder="Số tiền tối thiểu..."
                    onChange={(e) => setMoney(e.target.value)}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Phần trăm chiết khấu xu
          
        </div>
        <div className="card-body">
          <Table bordered hover responsive style={{maxWidth:"400px", margin:"0px auto"}}>
            <thead>
              <tr>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Chiết khấu (%)
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td style={{textAlign:"center"}}>{list.percent}</td>
                <td>
                  {" "}
                  <span
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      // flexWrap: "wrap",
                    }}
                  >
                    <Button
                      onClick={() => toggleModal2(list)}
                      className="label theme-bg text-white f-12"
                      style={{
                        borderRadius: "15px",
                        border: "0px",
                        boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                        float: "right",
                        fontSize: "12px",
                        backgroundImage:
                          "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                        margin: "5px",
                        padding: "3px 5px",
                        cursor: "pointer",
                      }}
                    >
                      <Edit style={{ fontSize: "20px" }} />
                    </Button>
                  </span>
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>

      <div className="card">
        <div className="card-header">
          Phần trăm chiết khấu shipper
          
        </div>
        <div className="card-body">
          <Table bordered hover responsive style={{maxWidth:"400px", margin:"0px auto"}}>
            <thead>
              <tr>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Chiết khấu (%)
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td style={{textAlign:"center"}}>{shipper.percent}</td>
                <td>
                  {" "}
                  <span
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      // flexWrap: "wrap",
                    }}
                  >
                    <Button
                      onClick={() => toggleModal3(shipper)}
                      className="label theme-bg text-white f-12"
                      style={{
                        borderRadius: "15px",
                        border: "0px",
                        boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                        float: "right",
                        fontSize: "12px",
                        backgroundImage:
                          "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                        margin: "5px",
                        padding: "3px 5px",
                        cursor: "pointer",
                      }}
                    >
                      <Edit style={{ fontSize: "20px" }} />
                    </Button>
                  </span>
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
      <div className="card">
        <div className="card-header">
          Số tiền tối thiểu shipper có thể rút
        </div>
        <div className="card-body">
          <Table bordered hover responsive style={{maxWidth:"400px", margin:"0px auto"}}>
            <thead>
              <tr>
                <th
                  style={{
                    width: "15%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Số tiền (VNĐ)
                </th>

                <th
                  style={{
                    width: "15%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td style={{textAlign:"center"}}>{Number(withdrawMoney.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}</td>
                <td>
                  {" "}
                  <span
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      // flexWrap: "wrap",
                    }}
                  >
                    <Button
                      onClick={() => toggleModal4(withdrawMoney)}
                      className="label theme-bg text-white f-12"
                      style={{
                        borderRadius: "15px",
                        border: "0px",
                        boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                        float: "right",
                        fontSize: "12px",
                        backgroundImage:
                          "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                        margin: "5px",
                        padding: "3px 5px",
                        cursor: "pointer",
                      }}
                    >
                      <Edit style={{ fontSize: "20px" }} />
                    </Button>
                  </span>
                </td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    </>
  );
};
export default Event;
