import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import Add from "@material-ui/icons/AddCircle";
import { Modal, Row, Col, Table, Button, Form } from "react-bootstrap";
import CallApi from "../../Middleware/api";
import { useDispatch } from "react-redux";
import { reloading } from "../../Redux/action/index";
import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
const Event = () => {
  const dispatch = useDispatch();
  const [list, setList] = useState([]);
  const [titleModal, setTitleModal] = useState("");
  const [submitModal, setSubmitModal] = useState("");
  const [title, setTitle] = useState("");
  const [id, setId] = useState("");
  const [code, setCode] = useState("");
  const [description, setDescription] = useState("");
  const [proviso, setProviso] = useState(0);
  const [type, setType] = useState("");
  const [time_open, setTime_open] = useState("");
  const [time_close, setTime_close] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [money, setMoney] = useState("");
  const [quantity, setQuantity] = useState("");
  const [quantity_use_with_user_new, setQuantityUseWithUserNew] = useState("");
  const [type_percent_or_money, setType_percent_or_money] = useState("1");
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/voucher-delivery/list-voucher`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
    dispatch(reloading(true));
  }, [dispatch]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setSubmitModal("Cập nhật");
    setTitle(item.title);
    setCode(item.code);
    setDescription(item.description);
    setType(item.type);
    setProviso(item.proviso);
    setId(item.id);
    setType_percent_or_money(item.type_percent_or_money)
    setMoney(item.type_percent_or_money===1 ? item.percent : item.money);
    setQuantity(item.quantity)
    setQuantityUseWithUserNew(item.quantity_use_with_user_new)
    setTime_close(item.time_close);
    setTime_open(item.time_open);
    setTitleModal("Cập nhật voucher");
  };
  const toggleModal3 = () => {
    setShowHide(!showHide);
    setSubmitModal("Thêm");
    setTitle("");
    setCode("");
    setDescription("");
    setType("");
    setProviso(0);
    setQuantity("")
    setQuantityUseWithUserNew("")
    setType_percent_or_money("1");
    setId("");
    setMoney("");
    setTime_close("");
    setTime_open("");
    setTitleModal("Thêm voucher");
  };
  const UpdateAccount = () => {
    dispatch(reloading(true));
    const fileMoney = {
      title,
      code,
      description,
      type,
      quantity,
      quantity_use_with_user_new,
      proviso,
      time_open:time_open,
        // time_open.slice(12, 16) +
        // " " +
        // time_open.slice(8, 10) +
        // "-" +
        // time_open.slice(5, 7) +
        // "-" +
        // time_open.slice(0, 4),
      time_close:time_close,
        // time_close.slice(12, 16) +
        // " " +
        // time_close.slice(8, 10) +
        // "-" +
        // time_close.slice(5, 7) +
        // "-" +
        // time_close.slice(0, 4),

      money: money?.toString().replace(/,/gi, ""),
    };
    const filePersion = {
      title,
      code,
      description,
      type,
      quantity,
      quantity_use_with_user_new,
      proviso,
      time_open:time_open,
        // time_open.slice(12, 16) +
        // " " +
        // time_open.slice(8, 10) +
        // "-" +
        // time_open.slice(5, 7) +
        // "-" +
        // time_open.slice(0, 4),
      time_close:time_close,
        // time_close.slice(12, 16) +
        // " " +
        // time_close.slice(8, 10) +
        // "-" +
        // time_close.slice(5, 7) +
        // "-" +
        // time_close.slice(0, 4),

        percent: money,
    };
    if (id === "") {
      console.log(proviso);
      if (proviso === 0 || (proviso !== 0 && Number(proviso))) {
        CallApi(
          `admin/voucher-delivery/create-voucher`,
          "POST",
          Number(type_percent_or_money) === 2 ? fileMoney : filePersion,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã thêm thành công!", "success");

              CallApi(
                `admin/voucher-delivery/list-voucher`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Điều kiện phải là số !");
        dispatch(reloading(false));
      }
    } else {
      if (proviso === 0 || (proviso !== 0 && Number(proviso))) {
        CallApi(
          `admin/voucher-delivery/update-voucher/${id}`,
          "PUT",
          Number(type_percent_or_money)===2 ? fileMoney:filePersion,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setShowHide(!showHide);
              swal("Thành công!", "Bạn đã cập nhật thành công!", "success");

              CallApi(
                `admin/voucher-delivery/list-voucher`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      } else {
        swal("Điều kiện phải là số !");
        dispatch(reloading(false));
      }
    }
  };
  const DeleteItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn kết thúc?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/voucher-delivery/delete-voucher/${item.id}`,
          "DELETE",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã kết thúc voucher thành công!", "success");
              setList([]);
              CallApi(
                `admin/voucher-delivery/list-voucher`,
                "GET",
                null,
                localStorage.getItem("token")
              )
                .then((res) => {
                  dispatch(reloading(false));
                  if (res.data.status === 1) {
                    setList(res.data.data);
                  } else if (
                    res.data.message === "Không tìm thấy tài khoản." ||
                    res.data.message === "Token đã hết hạn"
                  ) {
                    localStorage.clear();
                    window.location.reload();
                  }
                })
                .catch((error) => {
                  dispatch(reloading(true));
                  // swal("Vui lòng kiểm tra internet");
                });
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
          });
      }
    });
  };
  const ExportItem = (item) => {
    swal({
      title: "Bạn chắc chắn muốn xuất file báo cáo?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        window.location.href = `http://api.timzi.vn/api/export-voucher-order/${item.id}`;
        swal("Thành công!", "Bạn đã xuất file thành công!", "success");
        setList([]);
        CallApi(
           `admin/voucher-delivery/list-voucher`,
            "GET",
            null,
            localStorage.getItem("token")
          )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              setList(res.data.data);
            } else if (
              res.data.message === "Không tìm thấy tài khoản." ||
              res.data.message === "Token đã hết hạn"
            ) {
              localStorage.clear();
              window.location.reload();
            }
          })
          .catch((error) => {
            dispatch(reloading(true));
            // swal("Vui lòng kiểm tra internet");
          });
      }
    });
  };
  return (
    <>
      {showHide ? (
        <Modal show={showHide} onHide={() => CloseModal()}>
          <Modal.Header closeButton onClick={() => CloseModal()}>
            <Modal.Title>{titleModal}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Tên voucher</Form.Label>
                  <Form.Control
                    type="text"
                    name="title"
                    value={title}
                    placeholder="Tên voucher..."
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Form.Group>
              </Col>
              
              <Col md={12}>
                <Form.Group>
                  <Form.Label>Code</Form.Label>
                  <Form.Control
                    type="text"
                    name="code"
                    value={code}
                    placeholder="Code..."
                    onChange={(e) => setCode(e.target.value)}
                  />
                </Form.Group>
                
                <Form.Group>
                  <Form.Label>Mô tả</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows="4"
                    name="description"
                    value={description}
                    placeholder="Mô tả..."
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Loại voucher</Form.Label>
                  <Form.Control
                    as="select"
                    name="type"
                    value={type}
                    onChange={(e) => setType(e.target.value)}
                  >
                    <option value="">Chọn Loại voucher</option>
                    <option value="1">Khách hàng thường</option>
                    <option value="2">Khách mới</option>
                  </Form.Control>
                </Form.Group>

                <Form.Group>
                  <Form.Label>Kiểu áp dụng</Form.Label>
                  <Form.Control
                    as="select"
                    name="type_percent_or_money"
                    value={type_percent_or_money}
                    onChange={(e) => setType_percent_or_money(e.target.value)}
                  >
                    <option value="2">Tiền</option>
                    <option value="1">Phần trăm</option>
                  </Form.Control>
                </Form.Group>
                {Number(type_percent_or_money) === 2 ? (
                  <Form.Group>
                    <Form.Label>Giá tiền</Form.Label>
                    <Form.Control
                      type="text"
                      name="money"
                      value={
                        Number(money.toString().replace(/,/gi, ""))
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          ? Number(money.toString().replace(/,/gi, ""))
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          : 0
                      }
                      placeholder="Giá tiền..."
                      onChange={(e) => setMoney(e.target.value)}
                    />
                  </Form.Group>
                ) : (
                  <Form.Group>
                    <Form.Label>Phần trăm (%)</Form.Label>
                    <Form.Control
                      type="text"
                      name="money"
                      value={money}
                      placeholder="Phần trăm..."
                      onChange={(e) => setMoney(e.target.value)}
                    />
                  </Form.Group>
                )}
                {Number(type) === 2 ? (
                  <Form.Group>
                  <Form.Label>Số lượng dành cho khách mới</Form.Label>
                  <Form.Control
                    type="text"
                    name="quantity_use_with_user_new"
                    value={quantity_use_with_user_new}
                    placeholder="Số lượng..."
                    onChange={(e) => setQuantityUseWithUserNew(e.target.value)}
                  />
                </Form.Group>
                ) : (
                  <Form.Group>
                    <Form.Label>Số lượng</Form.Label>
                    <Form.Control
                      type="text"
                      name="quantity"
                      value={quantity}
                      placeholder="Số lượng..."
                      onChange={(e) => setQuantity(e.target.value)}
                    />
                  </Form.Group>
                )}
                {Number(type) === 1 ? (
                  <Form.Group>
                    <Form.Label>Điều kiện(Phí ship tối thiểu)</Form.Label>
                    <Form.Control
                      type="text"
                      name="proviso"
                      value={proviso}
                      placeholder="Điều kiện(Phí ship tối thiểu)..."
                      onChange={(e) => setProviso(e.target.value)}
                    />
                  </Form.Group>
                ) : (
                  <Form.Group>
                  </Form.Group>
                )}
              </Col>
                {Number(type) === 1 ? (
                  <Col md={6}>
                    <Form.Group>
                      <Form.Label>Bắt đầu</Form.Label>
                      <Form.Control
                        type="date"
                        name="time_open"
                        value={time_open}
                        onChange={(e) => setTime_open(e.target.value)}
                      />
                    </Form.Group>
                  </Col>
                ) : (
                  <Form.Group>
                  </Form.Group>
                )}
                {Number(type) === 1 ? (
                  <Col md={6}>
                    <Form.Group>
                      <Form.Label>Kết thúc</Form.Label>
                      <Form.Control
                        type="date"
                        name="time_close"
                        value={time_close}
                        onChange={(e) => setTime_close(e.target.value)}
                      />
                    </Form.Group>
                  </Col>
                ) : (
                  <Form.Group>
                  </Form.Group>
                )}
              
            </Row>
          </Modal.Body>

          <Modal.Footer style={{ flexWrap: "nowrap" }}>
            <Button
              onClick={() => UpdateAccount()}
              className="unread label theme-bg2 text-white f-12 float-right"
              style={{
                borderRadius: "15px",
                border: "0px",
                boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                float: "right",
                fontWeight: "bold",
                fontSize: "14px",
                padding: "5px 10px",
                cursor: "pointer",
              }}
            >
              {submitModal}
            </Button>
          </Modal.Footer>
        </Modal>
      ) : null}
      <div className="card">
        <div className="card-header">
          Danh sách voucher đơn giao hộ
          <div className="card-header-actions">
            <small className="text-muted" onClick={() => toggleModal3()}>
              <Button
                className="label theme-bg text-white f-12"
                style={{
                  borderRadius: "15px",
                  border: "0px",
                  boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                  fontSize: "12px",
                  backgroundImage:
                    "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                  padding: "3px 10px",
                  cursor: "pointer",
                }}
              >
                <Add style={{ fontSize: "20px" }} />
              </Button>
            </small>
          </div>
        </div>
        <div className="card-body">
          <Table bordered hover responsive id="TableRespon1">
            <thead>
              <tr>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Code
                </th>
                <th
                  style={{
                    width: "10%",
                    wordBreak: "break-word",
                    textAlign: "center",
                  }}
                >
                  Tên voucher
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Số lượng 
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Số lượng đã sd
                </th>

                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Số tiền
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Phần trăm
                </th>
                <th
                  style={{
                    width: "13%",
                    textAlign: "center",
                  }}
                >
                  Loại giảm giá
                </th>
                <th
                  style={{
                    width: "8%",
                    textAlign: "center",
                  }}
                >
                  Điều kiện
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Bắt đầu
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Kết thúc
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Trạng thái
                </th>
                <th
                  style={{
                    width: "10%",
                    textAlign: "center",
                  }}
                >
                  Tùy chọn
                </th>
              </tr>
            </thead>
            {list.length > 0 ? (
              <tbody>
                {list.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                      }}
                      // onClick={() => history.push(`/users/${item._id}`)}
                    >
                      <td>{item.code}</td>
                      <td>{item.title}</td>
                      <td>{item.type === 1 ? item.quantity : item.quantity_use_with_user_new}</td>
                      <td>{item.quantity_used}</td>

                      <td>
                        {" "}
                        {Number(item?.money)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>{item.percent} %</td>
                      <td>
                        {item.type === 1 ? "Voucher khách hàng thường" : "Voucher khách hàng mới"}{" "}
                      </td>
                      <td>
                        {Number(item?.proviso)
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "đ"}
                      </td>
                      <td>
                        {item?.time_open?.slice(8, 10)}-
                        {item?.time_open?.slice(5, 7)}-
                        {item?.time_open?.slice(0, 4)}
                      </td>
                      <td>
                        {item?.time_close?.slice(8, 10)}-
                        {item?.time_close?.slice(5, 7)}-
                        {item?.time_close?.slice(0, 4)}
                      </td>
                      <td>
                        {item.status === 1 ? "Chưa hoạt động" : null}
                        {item.status === 2 ? "Đang hoạt động" : null}
                        {item.status === 3 ? "Đã kết thúc" : null}
                      </td>

                      <td>
                        <span
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            // flexWrap: "wrap",
                          }}
                        >
                          <Button
                            onClick={() => toggleModal2(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "12px",
                              backgroundImage:
                                "linear-gradient(rgb(0 71 128), rgb(29 88 140))",
                              margin: "5px",
                              padding: "3px 5px",
                              cursor: "pointer",
                            }}
                          >
                            <Edit style={{ fontSize: "20px" }} />
                          </Button>
                          {/* <Button
                            onClick={() => ExportItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              marginRight: "0px",
                              backgroundImage:
                                " linear-gradient(#3776a9, #004780)",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <AccountBalanceIcon style={{ fontSize: "20px" }} />
                          </Button> */}
                          <Button
                            onClick={() => DeleteItem(item)}
                            className="label theme-bg text-white f-12"
                            style={{
                              borderRadius: "15px",
                              border: "0px",
                              background: "red",
                              boxShadow: "0 5px 10px 0 rgba(0, 0, 0, 0.2)",
                              float: "right",
                              fontSize: "10px",
                              padding: "3px 5px",
                              margin: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <Delete style={{ fontSize: "20px" }} />
                          </Button>
                        </span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            ) : (
              <tbody>
                <tr>
                  <td
                    colSpan="9"
                    style={{
                      textAlign: "center",
                      padding: "10px",
                    }}
                  >
                    Không có dữ liệu
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    </>
  );
};
export default Event;
