import React, { useState, useEffect } from "react";
import { CCard, CCardBody, CCol, CRow, CCardHeader } from "@coreui/react";
import { Button, Modal, Row, Col, Table } from "react-bootstrap";
import { useDispatch } from "react-redux";
import CallApi from "../../Middleware/api";
import { reloading } from "../../Redux/action/index";
import Check from "@material-ui/icons/Check";
import Delete from "@material-ui/icons/Delete";
import swal from "sweetalert";
import QRCode from "qrcode.react";
import "./index.css";
const DetailEvent = ({ match }) => {
  const dispatch = useDispatch();
  const [titleModal, setTitleModal] = useState("");
  const [listCombo, setListCombo] = useState([]);
  const [list, setList] = useState("");
  const [showHide, setShowHide] = useState(false);
  const [combo, setCombo] = useState(false);
  useEffect(() => {
    dispatch(reloading(true));
    CallApi(
      `admin/order-food/order-detail/${match.params.id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
        console.log(res.data.data)

          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          //   changeLocation(`/#/users`);
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });

    CallApi(
      `store/store-detail-with-book-table/${match.params.id}`,
      "GET",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          setList(res.data.data);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        } else {
          //   changeLocation(`/#/users`);
        }
      })
      .catch((error) => {
        // swal("Vui lòng kiểm tra internet");
      });
  }, [dispatch, match.params.id]);
  const CloseModal = () => {
    setShowHide(!showHide);
  };
  const changeLocation = (link) => {
    window.location = link;
  };
  const toggleModal3 = (item) => {
    setShowHide(!showHide);
    setListCombo(item?.food);
    setCombo(true);
    setTitleModal("Danh sách món ăn trong combo");
  };
  const toggleModal2 = (item) => {
    setShowHide(!showHide);
    setListCombo(item?.food);
    setCombo(false);
    setTitleModal("Danh sách món ăn");
  };
  const CheckOk = () => {
    dispatch(reloading(true));
    CallApi(
      `admin/store/confirm-store/${list.store.id}`,
      "PUT",
      null,
      localStorage.getItem("token")
    )
      .then((res) => {
        dispatch(reloading(false));
        if (res.data.status === 1) {
          swal("Thành công!", "Bạn đã duyệt thành công!", "success");
          changeLocation(`/#/Quan-ly-cua-hang`);
        } else if (
          res.data.message === "Không tìm thấy tài khoản." ||
          res.data.message === "Token đã hết hạn"
        ) {
          localStorage.clear();
          window.location.reload();
        }
      })
      .catch((error) => {
        dispatch(reloading(true));
        // swal("Vui lòng kiểm tra internet");
      });
  };
  const DeleteItem = () => {
    swal({
      title: "Bạn chắc chắn muốn xóa?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        dispatch(reloading(true));
        CallApi(
          `admin/store/cancel-store/${list.store.id}`,
          "PUT",
          null,
          localStorage.getItem("token")
        )
          .then((res) => {
            dispatch(reloading(false));
            if (res.data.status === 1) {
              swal("Thành công!", "Bạn đã xóa thành công!", "success");
              changeLocation(`/#/Quan-ly-cua-hang`);
            } else {
              swal(res.data.message);
            }
          })
          .catch((error) => {});
      }
    });
  };
  return (
    <CRow>
      <CCol lg={12}>
        <CCard>
          <CCardHeader>Thông tin tin đơn hàng- mã đơn hàng: {list.code} </CCardHeader>
          <CCardBody>
            <table className="table table-striped table-hover">
              <tbody>
                <tr>
                  <td>Tên khách hàng</td>
                  <td>
                   {list.user_name}
                  </td>
                </tr>
                <tr>
                  <td>Số điện thoại</td>
                  <td>
                    <strong>{list.phone}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Địa chỉ</td>
                  <td>
                    <strong>{list.address}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Thời gian đặt hàng</td>
                  <td>
                  {list.date_time?.slice(11, 16)} {list.date_time?.slice(8, 10)}-{list.date_time?.slice(5, 7)}-{list.date_time?.slice(0, 4)}
                    </td>
                </tr>
                <tr>
                  <td>Thời gian cập nhật cuối cùng</td>
                  <td>
                    {list.updated_at?.slice(11, 16)} {list.updated_at?.slice(8, 10)}-{list.updated_at?.slice(5, 7)}-{list.updated_at?.slice(0, 4)}
                    </td>
                </tr>
                <tr>
                  <td>Loại giảm giá</td>
                  <td>
                    {list.type_voucher==1?"Tiền trong đơn hàng":null}
                    {list.type_voucher==2?"Miễn phí ship":null}
                    {list.type_voucher==0?"Không có":null}
                  </td>
                </tr>
                <tr>
                  <td>Giảm giá</td>
                  <td>
                  {Number(list.discount)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                  </td>
                </tr>
                <tr>
                  <td>Phí vận chuyển</td>
                  <td>
                    <strong>
                    {Number(list.fee_ship)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>
                <tr>
                  <td>Giá trị món ăn</td>
                  <td>
                    <strong>
                      {Number(list.total_money_food)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>
                <tr>
                  <td>Tổng giá trị đơn hàng</td>
                  <td>
                  <strong>
                      {Number(list.total_money)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                    </strong>
                  </td>
                </tr>

               
                <tr>
                  <td>Phương thức thanh toán</td>
                  <td>
                    {list.payment_method_id == 1?"VNPAY":null}
                    {list.payment_method_id == 2?"Tiền mặt":null}
                  </td>
                </tr>
                <tr>
                  <td>Trạng thái đơn hàng</td>
                  <td>
                        {list.status === 0 ? "Hẹn đặt" : null}
                        {list.status === 1 ? "Chờ cửa hàng xác nhận" : null}
                        {list.status === 2 ? "Chờ shipper nhận đơn" : null}
                        {list.status === 3 ? "Shipper đã nhận đơn" : null}
                        {list.status === 4 ? "Đơn đã giao" : null}
                        {list.status === 5 ? "Cửa hàng hủy đơn" : null}
                        {list.status === 6 ? "Khách hàng hủy đơn" : null}
                        {list.status === 7 ? "Hết hạn xác nhận" : null}
                        {list.status === 8 ? "Shipper xác nhận, nhận đơn" : null}
                        {list.status === 9 ? "Shipper đã đến cửa hàng" : null}
                        {list.status === 10 ? "Shipper hủy đơn" : null}
                        {list.status === 11 ? "Shipper đã đến điểm giao" : null}
                        {list.status === 12 ? "Hết thời gian shipper nhận đơn" : null}
                        {list.status === 13 ? "Đơn hàng quá 24h" : null}
                        {list.status === 14 && list.staff_timzi === null ? "Tìm Zì hủy" : null}
                        {list.status === 14 && list.staff_timzi != null ? list.staff_timzi.name + " hủy" : null}
                  </td>
                </tr>
                <tr>
                  <td>Thông tin shipper</td>
                  <td>
                    {list.shipper?.name } - {list.shipper?.phone }
                    
                  </td>
                </tr>
              
              
                
              </tbody>
            </table>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol lg={12}>
        <CCard>
          <CCardHeader>Danh sách món ăn</CCardHeader>
          <CCardBody>
            <Table bordered hover responsive id="TableRespon1">
              <thead>
                <tr>
                  <th
                    style={{
                      maxWidth: "5%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    ID
                  </th>
                  <th
                    style={{
                      width: "15%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Hình ảnh
                  </th>
                  <th
                    style={{
                      width: "40%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Tên món ăn
                  </th>
                  <th
                    style={{
                      width: "20%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Giá
                  </th>
                  <th
                    style={{
                      width: "20%",
                      wordBreak: "break-word",
                      textAlign: "center",
                    }}
                  >
                    Số lượng
                  </th>
                </tr>
              </thead>
              {list?.order_food_detail?.length > 0 ? (
                <tbody>
                  {list?.order_food_detail?.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td style={{ textAlign: "center" }}>{item.id}</td>
                        <td style={{ textAlign: "center" }}>
                        <img
                          src={item.food?.image}
                          alt="linkAvatar"
                          style={{
                            width: "60%",
                            maxWidth: "250px",
                            textAlign: "center",
                            // borderRadius: "100%",
                          }}
                        />
                        </td>
                        <td style={{ textAlign: "center" }}>{item.food.name}</td>
                        <td style={{ textAlign: "center" }}>
                        {Number(item.price)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                        </td>
                        <td style={{ textAlign: "center" }}>{item.quantity}</td>

                      </tr>
                    );
                  })}
                </tbody>
              ) : (
                <tbody>
                  <tr>
                    <td
                      colSpan="9"
                      style={{
                        textAlign: "center",
                        padding: "10px",
                      }}
                    >
                      Không có dữ liệu
                    </td>
                  </tr>
                </tbody>
              )}
            </Table>
          </CCardBody>
        </CCard>
      </CCol>

      <CCol lg={12}>
        <CCard>
          <CCardHeader>Thông tin cửa hàng</CCardHeader>
          <CCardBody>
            <table className="table table-striped table-hover">
              <tbody>
                <tr>
                  <td>Hình ảnh</td>
                  <td>
                    <img
                      src={list.store?.image}
                      alt="linkAvatar"
                      style={{
                        width: "60%",
                        maxWidth: "250px",
                        textAlign: "center",
                        // borderRadius: "100%",
                      }}
                    />
                  </td>
                </tr>
                <tr>
                  <td>Tên cửa hàng</td>
                  <td>
                    <strong>{list.store?.name}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Địa chỉ</td>
                  <td>
                    <strong>{list.store?.address}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Hotline</td>
                  <td>{list.store?.hotline}</td>
                </tr>
                <tr>
                  <td>Giá trung bình</td>
                  <td>
                    {Number(list.store?.average_price)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " đ"}
                  </td>
                </tr>
                <tr>
                  <td>Giờ mở cửa</td>
                  <td>
                    <strong>{list.store?.open_hours}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Giờ đóng cửa</td>
                  <td>{list?.store?.close_hours}</td>
                </tr>

                <tr>
                  <td>Tọa độ</td>
                  <td>
                    <strong>{list.store?.latitude}</strong>
                  </td>
                </tr>
                <tr>
                  <td>Kinh độ</td>
                  <td>{list?.store?.longtidue}</td>
                </tr>
                <tr>
                  <td>Điểm đánh giá</td>
                  <td>{list?.store?.star}</td>
                </tr>
               
              
               
              </tbody>
            </table>
          </CCardBody>
        </CCard>
      </CCol>

    </CRow>
  );
};

export default DetailEvent;
