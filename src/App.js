import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import "./scss/style.scss";
// window.OneSignal = window.OneSignal || [];
// const OneSignal = window.OneSignal;
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

// Containers
const TheLayout = React.lazy(() => import("./containers/TheLayout"));

// Pages
const Login = React.lazy(() => import("./views/pages/login/Login"));
// const Register = React.lazy(() => import("./views/pages/register/Register"));
const Page404 = React.lazy(() => import("./views/pages/page404/Page404"));
const Page500 = React.lazy(() => import("./views/pages/page500/Page500"));

class App extends Component {

  constructor(props){
    super(props)
    this.state={}
  }
  // componentDidMount(){
  //   OneSignal.push(()=> {
  //     OneSignal.init(
  //       {
  //         appId: "1a9bbed2-9294-4192-a736-01b461cb389a", //STEP 9
  //         promptOptions: {
  //           slidedown: {
  //             enabled: true,
  //             actionMessage: "We'd like to show you notifications for the latest news and updates about the following categories.",
  //             acceptButtonText: "OMG YEEEEESS!",
  //             cancelButtonText: "NAHHH",
  //             categories: {
  //                 tags: [
  //                     {
  //                         tag: "react",
  //                         label: "ReactJS",
  //                     },
  //                     {
  //                       tag: "angular",
  //                       label: "Angular",
  //                     },
  //                     {
  //                       tag: "vue",
  //                       label: "VueJS",
  //                     },
  //                     {
  //                       tag: "js",
  //                       label: "JavaScript",
  //                     }
  //                 ]
  //             }     
  //         } 
  //       },
  //       welcomeNotification: {
  //         "title": "One Signal",
  //         "message": "Thanks for subscribing!",
  //       } 
  //     },
  //       //Automatically subscribe to the new_app_version tag
  //       OneSignal.sendTag("new_app_version", "new_app_version", tagsSent => {
  //         // Callback called when tag has finished sending
  //         // console.log('new_app_version TAG SENT', tagsSent);
  //       })
  //     );
  //   });
  // }
  render() {
    return (
      <HashRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            {/* <Route
              exact
              path="/login"
              name="Login Page"
              render={(props) => <Login {...props} />}
            /> */}
            {/* <Route
              exact
              path="/register"
              name="Register Page"
              render={(props) => <Register {...props} />}
            /> */}
            <Route
              exact
              path="/404"
              name="Page 404"
              render={(props) => <Page404 {...props} />}
            />
            <Route
              exact
              path="/500"
              name="Page 500"
              render={(props) => <Page500 {...props} />}
            />
            {localStorage.getItem("token") !== null ? (
              <Route
                path="/"
                name="Trang chủ"
                render={(props) => <TheLayout {...props} />}
              />
            ) : (
              <Route
                path="/"
                name="Trang chủ"
                render={(props) => <Login {...props} />}
              />
            )}
          </Switch>
        </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
