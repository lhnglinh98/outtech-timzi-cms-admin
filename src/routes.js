import React from 'react';
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));

const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));
const Shop = React.lazy(()=> import("./views/Shop/index"));
const AddShop = React.lazy(()=>import("./views/Shop/Add"));
const UpdateShop = React.lazy(()=>import("./views/Shop/Update"));
const Category = React.lazy(()=>import("./views/Category/index"))
const DetailShop = React.lazy(()=>import("./views/Shop/detail"))
const Voucher = React.lazy(()=>import("./views/voucher/index"))
const VoucherDelivery = React.lazy(()=>import("./views/voucher_delivery/index"))
const Ship = React.lazy(()=>import("./views/Ship/index"))
const ProductWeight = React.lazy(()=>import("./views/product_weight/index"))
const Banner = React.lazy(()=>import("./views/banner/index"))
const Percent = React.lazy (()=>import("./views/percent/index"))
const Programer = React.lazy(()=>import("./views/program/index"))
const ProgramStore = React.lazy(()=>import("./views/program_store/index"))
const CategoryFood = React.lazy(()=>import("./views/category_food/index"))
const ShopDoanhThu = React.lazy(()=>import("./views/revenue/shop"))
const ShipperDoanhThu = React.lazy(()=>import("./views/revenue/shipper"))
const DetailshopDoanhThu = React.lazy(()=>import("./views/revenue/detailShop"))
const Notification = React.lazy(()=>import("./views/notification/index"))
const NotificationTransfer = React.lazy(()=>import("./views/notification/transfer"))
const ListSaff = React.lazy(()=>import("./views/users/saff/index"))
const DetailSaff = React.lazy(()=>import("./views/users/saff/detail"))
const ListShipper = React.lazy(()=>import("./views/users/shipper/index"))
const DetailShipper = React.lazy(()=>import("./views/users/shipper/detail"))
const HistoryMoney = React.lazy(()=>import("./views/users/shipper/history"))
const AdminSaffList = React.lazy(()=>import("./views/users/adminSaff/index"))
const AdminSaffDetail = React.lazy(()=>import("./views/users/adminSaff/detail"))
const Homewebsite = React.lazy(()=>import("./views/website/home/index"))
const LinkYoutube = React.lazy(()=>import("./views/website/link_youtube/index"))
const ContentToward = React.lazy(()=>import("./views/website/content_toward/index"))
const featireWebsite = React.lazy(()=>import("./views/website/feature/index"))
const QuestionWS = React.lazy(()=>import("./views/website/question/index"))
const LeaderWS = React.lazy(()=>import("./views/website/leader/index"))
const ContactWS= React.lazy(()=>import("./views/website/contact/index"))
const DocumentWS = React.lazy(()=>import("./views/website/document/index"))

const PaymentGuide = React.lazy(()=>import("./views/policy/payment_guide/index"))
const PrivacyPolicy = React.lazy(()=>import("./views/policy/privacy_policy/index"))
const Regulation = React.lazy(()=>import("./views/policy/regulation/index"))
const TermOfUse = React.lazy(()=>import("./views/policy/term_of_use/index"))
const DisputeResolutionPolicy = React.lazy(()=>import("./views/policy/dispute_resolution_policy/index"))

const Suggestion = React.lazy(()=>import("./views/suggestion/index"))
const TypeFood = React.lazy(()=>import("./views/type_food/index"))
const Solution = React.lazy(()=>import("./views/solution/index"))
const CategoryPpermanent = React.lazy(()=>import("./views/category_permanent/index"))

const Order = React.lazy(()=>import("./views/Order/index"))
const DetailOrder = React.lazy(()=>import("./views/Order/detail"))
const OrderUserProduct = React.lazy(()=>import("./views/order_user_product/index"))
const DetailOrderUserProduct = React.lazy(()=>import("./views/order_user_product/detail"))
const TransferAccount = React.lazy(()=>import("./views/users/transfer_account/index"))

const OderBuyMe = React.lazy(()=>import("./views/OderBuyMe/OderBuyMe/index"))
const DetailOrderBuyMe = React.lazy(()=>import("./views/OderBuyMe/OderBuyMe/detail"))

const Distances = React.lazy(()=>import("./views/OderBuyMe/Distances/index"))
const CategoryServices = React.lazy(()=>import("./views/OderBuyMe/CateroryServices/index"))


const routes = [
  { path: '/', exact: true, name: 'Trang chủ' },
  { path: '/Quan-ly-tai-lieu-huong-dan', exact: true, name: 'Quản lý tài liệu hướng dẫn',component: DocumentWS  },
  { path: '/Quan-ly-dang-ki-lien-he-website', exact: true, name: 'Danh sách đăng ký liên hệ website',component: ContactWS  },
  { path: '/Quan-ly-doi-ngu-lanh-dao-website', exact: true, name: 'Quản lý đội ngũ lãnh đạo website',component: LeaderWS  },
  { path: '/Quan-ly-cau-hoi-website', exact: true, name: 'Quản lý câu hỏi website',component: QuestionWS  },
  { path: '/Quan-ly-tinh-nang-website', exact: true, name: 'Quản lý tính năng website',component: featireWebsite  },
  { path: '/Quan-ly-trang-chu-website', exact: true, name: 'Quản lý trang chủ website',component: Homewebsite  },
  { path: '/Quan-ly-link-youtube', exact: true, name: 'Quản lý link youtube',component: LinkYoutube  },
  { path: '/Quan-ly-noi-dung-huong-toi', exact: true, name: 'Quản lý nội dung hướng tới',component: ContentToward  },
  { path: '/thong-bao', exact: true, name: 'Thông báo',component: Notification  },
  { path: '/thong-bao-cms-chuyen-nhuong', exact: true, name: 'Thông báo CMS chuyển nhượng',component: NotificationTransfer  },
  { path: '/doanh-thu-shipper', exact: true, name: 'Doanh thu shipper',component: ShipperDoanhThu  },
  { path: '/doanh-thu-cua-hang/:id', exact: true, name: 'Chi tiết doanh thu cửa hàng',component: DetailshopDoanhThu  },
  { path: '/doanh-thu-cua-hang', exact: true, name: 'Doanh thu cửa hàng',component: ShopDoanhThu  },
  { path: '/quan-ly-danh-muc-mon-an', exact: true, name: 'Quản lý danh mục món ăn',component: CategoryFood  },
  { path: '/chuong-trinh-timzi', exact: true, name: 'Quản lý chương trình timzi',component: Programer  },
  { path: '/chuong-trinh-cua-hang', exact: true, name: 'Quản lý chương trình cửa hàng',component: ProgramStore  },
  { path: '/chiet-khau', exact: true, name: 'Quản lý chiết khấu',component: Percent  },
  { path: '/Banner', exact: true, name: 'Quản lý banner',component: Banner  },
  { path: '/quan-ly-phi-ship', exact: true, name: 'Quản lý phí ship',component: Ship  },
  { path: '/quan-ly-khoi-luong-hang-hoa', exact: true, name: 'Quản lý khối lượng hàng hóa',component: ProductWeight  },
  { path: '/quan-ly-voucher', exact: true, name: 'Quản lý voucher',component: Voucher  },
  { path: '/quan-ly-voucher-don-giao-ho', exact: true, name: 'Quản lý voucher đơn giao hộ',component: VoucherDelivery  },
  { path: '/quan-ly-danh-muc-cua-hang', exact: true, name: 'Quản lý danh mục cửa hàng',component: Category  },
  { path: '/Quan-ly-cua-hang/them-cua-hang', exact: true, name: 'Thêm cửa hàng',component: AddShop  },
  { path: '/Quan-ly-cua-hang/chi-tiet-cua-hang/:id', exact: true, name: 'Chi tiết cửa hàng',component: DetailShop  },
  { path: '/Quan-ly-cua-hang', exact: true, name: 'Quản lý cửa hàng',component: Shop  },
  { path: '/list-article', name: 'Dashboard', component: Dashboard },
  { path: '/listConversation', name: 'Cuộc hội thoại', }, 
  { path: '/Quan-ly-cua-hang/cap-nhat/:id', exact: true, name: 'Cập nhật cửa hàng',component: UpdateShop  },
  { path: '/users', exact: true,  name: 'Quản lý tài khoản khách hàng', component: Users },
  { path: '/users/:id', exact: true, name: 'Chi tiết tài khoản khách hàng', component: User },
  { path: '/tai-khoan-apps-shop', exact: true,  name: 'Quản lý tài khoản apps shop', component: ListSaff },
  { path: '/tai-khoan-apps-shop/:id', exact: true, name: 'Chi tiết tài khoản apps shop', component: DetailSaff },

  { path: '/tai-khoan-shipper', exact: true,  name: 'Quản lý tài khoản shipper', component: ListShipper },
  { path: '/tai-khoan-shipper/:id', exact: true, name: 'Chi tiết tài khoản shipper', component: DetailShipper },
  { path: '/lich-su-dong-tien/:id', exact: true, name: 'Lịch Sử Dòng Tiền', component: HistoryMoney },

  { path: '/quan-ly-tai-khoan-chuyen-nhuong', exact: true, name: 'Quản lý tài khoản chuyển nhượng',component: TransferAccount  },

  { path: '/Quan-ly-nhan-vien-quan-tri', exact: true,  name: 'Quản lý tài khoản nhân viên quản trị', component: AdminSaffList },
  { path: '/Quan-ly-nhan-vien-quan-tri/:id', exact: true, name: 'Chi tiết tài khoản nhân viên quản trị', component: AdminSaffDetail },
  { path: '/quan-ly-danh-muc-cua-hang/quan-ly-mon-an-theo-danh-muc/:id', exact: true, name: 'Gợi ý món ăn theo danh mục',component: Suggestion  },
  { path: '/quan-ly-su-dung-giai-phap', exact: true, name: 'Quản lý sử dụng giải pháp',component: Solution  },
  { path: '/quan-ly-danh-muc-cua-hang/menu-co-dinh/:id', exact: true, name: 'Menu cố định',component: CategoryPpermanent  },
  { path: '/quan-ly-danh-muc-kieu-mon-an', exact: true, name: 'Quản lý danh mục kiểu món ăn',component: TypeFood  },
  { path: '/huong-dan-thanh-toan', exact: true, name: 'Hướng dẫn thanh toán',component: PaymentGuide  },
  { path: '/chinh-sach-bao-mat', exact: true, name: 'Chính sách bảo mật',component: PrivacyPolicy  },
  { path: '/quy-che', exact: true, name: 'Quy chế',component: Regulation  },
  { path: '/dieu-khoan-su-dung', exact: true, name: 'Điều khoản sử dụng',component: TermOfUse  },
  { path: '/chinh-sach-giai-quyet-tranh-chap', exact: true, name: 'Chính sách giải quyết tranh chấp',component: DisputeResolutionPolicy  },

  { path: '/don-hang', exact: true, name: 'Đơn hàng',component: Order  },
  { path: '/chi-tiet-don-hang/:id', exact: true, name: 'Đơn hàng',component: DetailOrder  },
  { path: '/don-hang-giao-ho', exact: true, name: 'Đơn hàng giao hộ',component: OrderUserProduct  },
  { path: '/chi-tiet-don-hang-giao-ho/:id', exact: true, name: 'Chi tiết đơn hàng giao hộ',component: DetailOrderUserProduct  },

  { path: '/don-hang-mua-ho', exact: true, name: 'Danh sách đơn hàng mua hộ',component: OderBuyMe  },
  { path: '/chi-tiet-don-hang-mua-ho/:id', exact: true, name: 'Chi tiết đơn hàng giao hộ',component: DetailOrderBuyMe  },

  { path: '/quan-ly-category-services', exact: true, name: 'Cấu hình chi phí dịch vụ',component: CategoryServices },
  { path: '/quan-ly-distances', exact: true, name: 'Cấu hình chi phí khoảng cách',component: Distances },
];

export default routes;
