import { createStore } from "redux";

const initialState = {
  sidebarShow: "responsive",
  loading: false,
  profile:true,
  lat: 21.066790709203456, 
  lng: 105.76430964544943
};
// { type, ...rest }
const changeState = (state = initialState, action) => {
  switch (action.type) {
    case "SETREPONSIVE":
      return { ...state, sidebarShow: action.sidebarShow };
    case "RELOADING":
      return {
        ...state,
        loading: action.loading,
      };
      case "PROFILE":
      return {
        ...state,
        profile: action.profile,
      };
      case "LAT":
      return {
        ...state,
        lat: action.lat,
      };
      case "LNG":
      return {
        ...state,
        lng: action.lng,
      };
    default:
      return state;
  }
};

const store = createStore(changeState);
export default store;
