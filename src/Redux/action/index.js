import * as types from "../constants";
export const reloading = (loading) => {
  return {
    type: types.RELOADING,
    loading,
  };
};
export const set_profile = (profile) => {
  return {
    type: types.PROFILE,
    profile,
  };
};
export const Set_reponsive = (sidebarShow) => {
  return {
    type: types.SETREPONSIVE,
    sidebarShow,
  };
};

export const set_lat = (lat) => {
  return {
    type: types.LAT,
    lat,
  };
};
export const set_lng = (lng) => {
  return {
    type: types.LNG,
    lng,
  };
};
export default reloading;
